#!/bin/make -f
PYTHON_VERSION_STR="310-x86_64-linux-gnu"

rtrg_c.cpython-$(PYTHON_VERSION_STR).so: rtrg_c.c
	python setup.py build_ext --inplace
	echo "Check the last command and repeat it with a different linker if necessary"
