#!/usr/bin/env python

# Copyright 2022 Valentin Bruch <valentin.bruch@rwth-aachen.de>
# License: MIT
"""
Kondo FRTRG, plot data for pulsed voltage
"""
import os
import argparse
import numpy as np
import settings
import tables as tb
from data_management import DataManager, KondoImport
from scipy.optimize import leastsq
from gen_pulse_data import fourier_coef_gauss, fourier_coef_gauss_symmetric
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm


def plot_gammaL(dm, omega=0., pulse_duration=0.1, pulse_height=4., pulse_phase=None, baseline=0., pulse_shape=["gauss", "gauss_symmetric"], resolution=500, **parameters):
    x = np.linspace(-np.pi, np.pi, resolution)
    for shape in pulse_shape:
        match shape:
            case "gauss":
                vdc, fourier_coef = fourier_coef_gauss(10, omega, pulse_duration, pulse_height, pulse_phase, baseline)
            case "gauss_symmetric":
                vdc, fourier_coef = fourier_coef_gauss_symmetric(10, omega, pulse_duration, pulse_height, pulse_phase, baseline)
            case _:
                settings.logger.warn(f"unknown pulse shape: {shape}")
                continue
        for kondo in dm.load_all(vdc=vdc, omega=omega, has_fourier_coef=True, fourier_coef=fourier_coef, **parameters):
            settings.logger.info(f"loading hash={kondo.hash}")
            nmax = kondo.nmax
            gammaL = kondo.gammaL
            img = plt.imshow(np.abs(gammaL), norm=LogNorm(1e-6))
            plt.colorbar(img)
            plt.show()


def current_time_all(dm, pulse_duration=0.1, pulse_phase=0.5, pulse_height=None, baseline=0., resolution=500, **parameters):
    x = np.linspace(-np.pi, np.pi, resolution)
    fig, (ax_voltage, ax_current, ax_g) = plt.subplots(nrows=3, ncols=1, sharex=True)
    for (row, kondo) in load_all_pulses_full(dm, pulse_duration=pulse_duration, pulse_phase=pulse_phase, pulse_height=pulse_height):
            nmax = kondo.nmax
            gammaL = kondo.gammaL
            if gammaL.ndim == 2:
                current_coef = gammaL[:,nmax]
            elif gammaL.ndim == 1:
                current_coef = gammaL
            assert current_coef.size == 2*nmax+1
            deltaGammaL = kondo.deltaGammaL
            if deltaGammaL.ndim == 2:
                g_coef = np.pi*deltaGammaL[:,nmax]
            elif gammaL.ndim == 1:
                g_coef = np.pi*deltaGammaL
            assert g_coef.size == 2*nmax+1
            current = np.zeros(resolution, dtype=np.complex128)
            for n, c in enumerate(current_coef, -nmax):
                current += c*np.exp(-1j*n*x)
            #print(kondo.omega, (0.5*kondo.dc_current + integrate_ft(-0.1, 0.4, current_coef[nmax-1::-1]))/kondo.omega*2*np.pi, kondo.dc_current/kondo.omega*2*np.pi)
            ax_current.plot(x/kondo.omega, current.real)
            ax_current.plot(x/kondo.omega, current.imag)
            voltage = np.ones(resolution, dtype=np.float64) * kondo.vdc
            for n, c in enumerate(kondo.fourier_coef, 1):
                voltage += 2*(c*np.exp(1j*n*x)).real
            ax_voltage.plot(x/kondo.omega, voltage)
            g = np.zeros(resolution, dtype=np.complex128)
            for n, c in enumerate(g_coef, -nmax):
                g += c*np.exp(-1j*n*x)
            ax_g.plot(x/kondo.omega, g.real)
            ax_g.plot(x/kondo.omega, g.imag)
    plt.show()



def current_time(dm, omega=0., pulse_duration=0.1, pulse_phase=None, pulse_height=4., baseline=0., pulse_shape=["gauss", "gauss_symmetric"], resolution=500, **parameters):
    x = np.linspace(-np.pi, np.pi, resolution)
    for shape in pulse_shape:
        match shape:
            case "gauss":
                vdc, fourier_coef = fourier_coef_gauss(10, omega, pulse_duration, pulse_height, pulse_phase, baseline)
            case "gauss_symmetric":
                vdc, fourier_coef = fourier_coef_gauss_symmetric(10, omega, pulse_duration, pulse_height, pulse_phase, baseline)
            case _:
                settings.logger.warn(f"unknown pulse shape: {shape}")
                continue
        for kondo in dm.load_all(vdc=vdc, has_fourier_coef=True, fourier_coef=fourier_coef, **parameters):
            nmax = kondo.nmax
            gammaL = kondo.gammaL
            if gammaL.ndim == 2:
                current_coef = gammaL[:,nmax]
            elif gammaL.ndim == 1:
                current_coef = gammaL
            assert current_coef.size == 2*nmax+1
            current = np.zeros(resolution, dtype=np.complex128)
            for n, c in enumerate(current_coef, -nmax):
                current += c*np.exp(-1j*n*x)
            plt.plot(x/kondo.omega, current.real)
            plt.plot(x/kondo.omega, current.imag)
            voltage = np.ones(resolution, dtype=np.float64) * kondo.vdc
            for n, c in enumerate(kondo.fourier_coef, 1):
                voltage += 2*(c*np.exp(1j*n*x)).real
            plt.plot(x/kondo.omega, voltage)
    plt.show()


def integrate_ft(t0, t1, fourier_coef):
    """
    t1                             i2πnt
    ∫ f(t) dt  where f(t) =  Σ fn e
    t0                       n

    t0 and t1 should be given in units of T=2π/Ω.
    fourier_coef should be given in units of Ω and start with the first
    (not zeroth) coefficient.
    """
    fourier_coef = np.asarray(fourier_coef)
    phase_arr = 2*np.pi*np.arange(1, fourier_coef.size+1)
    return ((fourier_coef * (np.exp(1j*t1*phase_arr) - np.exp(1j*t0*phase_arr))).imag / phase_arr).sum() * 2


def plot_transported_charge(dm, omega=None, **parameters):
    if omega is not None and omega<=0:
        omega = None
    table = load_all_pulses(dm, omega=omega)
    table = table[table.pulse_type == "gauss"]
    fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1)
    ax1.set_xlabel("pulse duration t (1/Tk)")
    ax2.set_xlabel("pulse phase  φ/2π")
    ax1.set_ylabel("charge per pulse Q (e)")
    ax2.set_ylabel("charge per pulse Q (e)")

    phase_norm = plt.Normalize(0, 1.25)
    duration_norm = LogNorm(0.01, 0.2)
    ax1.scatter(table.pulse_duration, table.dc_current/table.omega*2*np.pi, c=table.pulse_phase, s=100/table.omega**2, marker="o", norm=phase_norm)
    ax2.scatter(table.pulse_phase, table.dc_current/table.omega*2*np.pi, c=table.pulse_duration, s=100/table.omega**2, marker="o", norm=duration_norm)

    sym_pulse_omega = []
    sym_pulse_duration = []
    sym_pulse_phase = []
    sym_pulse_charge = []
    _h5files = set()
    for (row, kondo) in load_all_pulses_full(dm, pulse_type="gauss_symmetric", omega=omega):
        ref_time = min(3*row.pulse_duration*row.omega/(2*np.pi), 0.15)
        nmax = kondo.nmax
        sym_pulse_charge.append(integrate_ft(-ref_time, 0.5-ref_time, kondo.gammaL[nmax-1::-1,nmax])/row.omega*2*np.pi)
        _h5files.add(kondo._h5file)
        sym_pulse_omega.append(row.omega)
        sym_pulse_duration.append(row.pulse_duration)
        sym_pulse_phase.append(row.pulse_phase)
    for file in _h5files:
        file.close()

    sym_pulse_duration = np.array(sym_pulse_duration)
    sorting = np.argsort(sym_pulse_duration)
    sym_pulse_duration = sym_pulse_duration[sorting]
    sym_pulse_omega = np.array(sym_pulse_omega)[sorting]
    sym_pulse_phase = np.array(sym_pulse_phase)[sorting]
    sym_pulse_charge = np.array(sym_pulse_charge)[sorting]

    ax1.scatter(sym_pulse_duration, sym_pulse_charge, c=sym_pulse_phase, marker="^", s=80/np.array(sym_pulse_omega)**2, norm=phase_norm)
    for phase in (0.25, 0.5, 0.75, 1):
        selection = np.abs(sym_pulse_phase - phase) < 1e-3
        ax1.plot(sym_pulse_duration[selection], sym_pulse_charge[selection], color="black")
    ax2.scatter(sym_pulse_phase, sym_pulse_charge, c=sym_pulse_duration, marker="^", s=80/np.array(sym_pulse_omega)**2, norm=duration_norm)
    selection = np.abs(sym_pulse_duration - 0.04) < 1e-4
    sorting = np.argsort(sym_pulse_phase[selection])
    ax2.plot(sym_pulse_phase[selection][sorting], sym_pulse_charge[selection][sorting], color="black")
    plt.show()


def fit_fourier_coef(fourier_coef, tol=1e-2):
    # gauss
    narr = np.arange(1, fourier_coef.size+1)
    fit_functions = dict(
            gauss = lambda param: (param[0]*np.exp(-param[1]*narr**2) - fourier_coef).view(dtype=np.float64),
            gauss_symmetric = lambda param: (param[0]*np.exp(-param[1]*((narr+1)/2)**2)*(narr%2) - fourier_coef).view(dtype=np.float64),
            )
    for name, func in fit_functions.items():
        fit, cov = leastsq(func, (0.2, 0.1))
        residuals = (func(fit)**2).sum()
        settings.logger.debug(f"fit_fourier_coef: fit {name} yields {fit}, residuals {residuals:.3g}")
        if residuals < tol:
            return dict(type=name, prefactor=fit[0], smoothen=fit[1])
    return dict(type="unknown")


def load_all_pulses(dm, **parameters):
    table = dm.list_fourier_coef(**parameters)
    table["pulse_type"] = "unknown"
    table["pulse_duration"] = 0.
    table["pulse_phase"] = 0.
    table["pulse_height"] = 0.
    fourier_coef = np.array([table[f"fcr{i}"]+1j*table[f"fci{i}"] for i in range(10)]).T
    for coef, idx in zip(fourier_coef, table.index):
        parameters = fit_fourier_coef(coef)
        match parameters["type"]:
            case "gauss":
                table.loc[idx, "pulse_type"] = "gauss"
                table.loc[idx, "pulse_duration"] = 4*np.sqrt(parameters["smoothen"]*np.log(2)) / table.omega[idx]
                table.loc[idx, "pulse_height"] = parameters["prefactor"] / np.sqrt(parameters["smoothen"]) * np.pi**0.5
                table.loc[idx, "pulse_phase"] = parameters["prefactor"] / table.omega[idx]
            case "gauss_symmetric":
                table.loc[idx, "pulse_type"] = "gauss_symmetric"
                table.loc[idx, "pulse_duration"] = 2*np.sqrt(parameters["smoothen"]*np.log(2)) / table.omega[idx]
                table.loc[idx, "pulse_height"] = parameters["prefactor"] / np.sqrt(parameters["smoothen"]) * np.pi**0.5
                table.loc[idx, "pulse_phase"] = parameters["prefactor"] / (2*table.omega[idx])
            case _:
                settings.logger.debug(f"unknown type: {parameters['type']}")
    return table


def load_all_pulses_full(dm, pulse_type="all", pulse_duration=None, pulse_phase=None, pulse_height=None, **parameters):
    table = load_all_pulses(dm, **parameters)
    if pulse_type == "all":
        sel = table.pulse_type != "unknown"
    else:
        sel = table.pulse_type == pulse_type
    if pulse_duration is not None:
        sel &= np.abs(table.pulse_duration - pulse_duration) < 1e-3 * pulse_phase
    if pulse_phase is not None:
        sel &= np.abs(table.pulse_phase - pulse_phase) < 1e-3
    if pulse_height is not None:
        sel &= np.abs(table.pulse_height - pulse_height) < 1e-3 * pulse_height
    for ((dirname, basename), subtable) in table[sel].groupby(["dirname", "basename"]):
        try:
            h5file = tb.open_file(os.path.join(dirname, basename))
        except:
            settings.logger.exception("Error while loading HDF5 file")
            continue
        #metadatatable = h5file.get_node('/metadata/mdtable')
        for index, row in subtable.iterrows():
            try:
                datanode = h5file.get_node("/data/" + row.hash)
                #metadatarow = metadatatable.where("hash == '%s'"%(row.hash))
                yield row, KondoImport(row, datanode, h5file, owns_h5file=subtable.shape[0]==1)
            except:
                settings.logger.exception("Error while loading data")


def parameter_overview(dm, **trash):
    table = load_all_pulses(dm)
    fig, ax_gauss = plt.subplots()

    sel = (table.pulse_type == "gauss") | (table.pulse_type == "gauss_symmetric")
    gauss_frequency = table.omega[sel] * (1 + (table.pulse_type[sel] == "gauss_symmetric"))
    gauss_duration = table.pulse_duration[sel]
    gauss_height = table.pulse_height[sel]
    gauss_phase = table.pulse_phase[sel]
    gauss_good = table.solver_flags[sel] & (DataManager.SOLVER_FLAGS["include_Ga"] | DataManager.SOLVER_FLAGS["second_order_rg_equations"]) == DataManager.SOLVER_FLAGS["include_Ga"]
    ax_gauss.plot(gauss_duration[gauss_good], gauss_phase[gauss_good], "x")
    ax_gauss.plot(gauss_duration[~gauss_good], gauss_phase[~gauss_good], "+")
    plt.show()


def main():
    from logging import _levelToName
    parser = argparse.ArgumentParser(description="Generate plots for pulsed voltage")
    parser.add_argument("function", type=str, default="current_time",
            help="name for preset parameters")
    parser.add_argument("--db_filename", metavar="file", type=str,
            help = "SQLite database file for saving metadata")
    parser.add_argument("--log_level", metavar="str", type=str,
            default = _levelToName.get(settings.logger.level, "INFO"),
            choices = ("INFO", "DEBUG", "WARNING", "ERROR"),
            help = "logging level")
    parser.add_argument("--omega", type=float,
            help="Frequency, units of Tk")
    parser.add_argument("--method", type=str, choices=("J", "mu"),
            help="method: J or mu")
    parser.add_argument("--nmax", metavar="int", type=int,
            help="Floquet matrix size")
    parser.add_argument("--padding", metavar="int", type=int,
            help="Floquet matrix ppadding")
    parser.add_argument("--voltage_branches", metavar="int", type=int,
            help="Voltage branches")
    parser.add_argument("--resonant_dc_shift", metavar="int", type=int,
            help="resonant DC shift")
    parser.add_argument("--pulse_phase", metavar="float", type=float,
            help="pulse phase/2π")
    parser.add_argument("--pulse_duration", metavar="float", type=float,
            help="pulse duration (FWHM), units of 1/Tkrg")
    parser.add_argument("--pulse_height", metavar="float", type=float,
            help="pulse height, units of Tkrg")
    parser.add_argument("--baseline", metavar="float", type=float, default=0.,
            help="Vdc, units of Tkrg")
    parser.add_argument("--integral_method", metavar="int", type=int,
            help="method for solving frequency integral (-1 for exact solution)")
    parser.add_argument("--include_Ga", metavar="bool", type=bool,
            help="include Ga in RG equations")
    parser.add_argument("--resolution", metavar="int", type=int, default=500,
            help="plot resolution")
    args = parser.parse_args()

    options = args.__dict__
    db_filename = options.pop("db_filename", None)
    if db_filename is not None:
        settings.defaults.DB_CONNECTION_STRING = "sqlite:///" + os.path.abspath(db_filename)
    settings.defaults.logger.setLevel(options.pop("log_level"))
    settings.defaults.update_globals()

    function = globals()[options.pop("function")]
    function(DataManager(), **options)


if __name__ == "__main__":
    main()
