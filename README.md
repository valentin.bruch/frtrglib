# FRTRG Kondo
Code implementing Floquet real-time renormalization group method for the Kondo
model as explained in <https://arxiv.org/abs/2206.06263>. The code in this
repository is more general than the code explained in the preprint cited above.

This includes software for dealing with matrices appearing in the Floquet Green's function approach. The modules have levels of specialization.

### Mitigation to truncation effects for infinite size matrices
`rtrg_c.c` provides helper functions to avoid truncation effects when dealing with infinite size square matrices. It relies on the following assumptions:
* All matrices are truncated infinite size matrices.
* The infinite size matrix can be (approximately) reconstructed by linearly continuing the matrix along it's diagonal.

An example of the extrapolation from the truncated matrix (box) to the estimated bigger matrix:
```
6 2 2 1 0 0 0 0 0 0 0 0
2 5 2_2_1_0_0_0_0_0 0 0
2 2|4 2 2 1 0 0 0 0|0 0
1 2|2 3 2 2 1 0 0 0|0 0
0 1|2 2 2 2 2 1 0 0|0 0
0 0|1 2 2 1 1 2 1 0|0 0
0 0|0 1 2 1 1 2 2 1|0 0
0 0|0 0 1 2 2 2 2 2|1 0
0 0|0 0 0 1 2 2 3 2|2 1
0 0|0_0_0_0_1_2_2_4|2 2
0 0 0 0 0 0 0 1 2 2 5 2
0 0 0 0 0 0 0 0 1 2 2 6
```

`rtrg_c` provides functions for matrix extrapolation, inversion, and multiplication for numpy arrays of type complex128.
In the multiplication the following symmetry can optionally be used to speed up the calculation: (in python syntax) `A[::-1,::-1] = ±A.conjugate()`.
This symmetry plays an important role in the Floquet Green's function formalism.

The functions of `rtrg_c` can be tested and benchmarked using `test.py` and `benchmark.py`.
For best performance some parameters in the code should be adapted as explained in the code and in `doc/main.tex`. The best values for these parameters depends mainly on the size of the Floquet matrices and the number of available OMP threads. Enabling more parallelization may slightly improve performance for large matrices but significantly worsen the performance for small matrices.
Note that `rtrg_c` works best with F-ordered numpy arrays, see `doc/main.tex` or (in python) `help(rtrg_c)`.

The highly experimental code for `rtrg_cublas` aims to provide the same functionality as `rtrg_c`, but uses CUBLAS (framework for nvidia GPUs) to speed up the matrix products.
Note that `rtrg_cublas` is outdated, less stable, and much slower than `rtrg_c` if not configured correctly.

### Floquet matrix classes
The class `RGfunction` in `rtrg.py` provide a high-level interface for calculations with Floquet matrices including replicas with shifted energy argument in python.

`reservoirmatrix.py` provides a matrix of `RGfunction` objects and some functions that speed up some calculations with these objects by using symmetries.

### FRTRG analysis of Kondo model
In `kondo.py` the framework of FRTRG is applied to the Kondo model.
`main.py` contains functions to generate, save, load and visualize FRTRG data for tke Kondo model.


## Performance
The option `compact` is used to improve performance for the special case `method==J` with V(-t)=-V(t), `voltage_branches==0` and large `nmax`.
The options `use_reference_implementation`, `ignore_symmetries`, `enforce_symmetric` are only for testing. They reduce the performance.


## Versions
version 14.16:
* improved initial condition for dΛL/dE from linear response

version 14.15:
* set Tk=1 in second order truncation

version 14.x:
* based on versions 8 and 13
* implements the RG for the system with and without unitary transformation (version 8 and 13)
* completely new user interface

baseversion 13:
* used to generate data for <https://arxiv.org/abs/2206.06263>

baseversion 12:
* handling of frequency shifts in initial conditions for Floquet matrices is a bit strange
* no noticeable difference in results

baseversion 11:
* small mistakes in initial conditions, but these are of higher order
* average current: very small difference, order 1e-8
* oscillating current (also for pulses): small difference, relative mistake of order 4e-4 or below

baseversion 10:
* significant mistake in initial conditions

baseversion 9:
* probably similar mistakes in initial conditions as in version 10
* attempt to implement RG equations starting from different (but equivalent) initial Hamiltonian with minimal changes to version 8

baseversion 8:
* final version using a Hamiltonian that was initially modified by a unitary transformation
* oscillating voltage only enters in initial conditions
* more efficient than version 13 when the average voltage vanishes
* less thoroughly tested than version 13

baseversion 7:
* old index order: uses (..., voltage shift index, floquet index, floquet index) instead of Fortran arrays with (..., floquet index, floquet index, voltage shift index)
* includes old code for pole and residue detection (removed in later version)
* includes old, divergent treatment of driving the coupling strength
* introduced `compact_rtrg` with `SymRGfunction`

baseversion 6:
* rewritten pole finder

baseversion 5:
* corrected a sign mistake (in the direction of time)
* introduced "compact" option

baseversion 4:
* no significant changes in calculations

baseversion 3:
* added variation of Tk

baseversion 2:
* calculation rewritten in large parts compared to baseversion 1
