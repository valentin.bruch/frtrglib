# build with:
# python3 setup.py build_ext --inplace

from setuptools import setup, Extension
import numpy as np
from os import environ

def main():
    compiler_args = ['-O3','-Wall','-Wextra','-std=c11']
    linker_args = []
    include_dirs = [np.get_include()]
    library_dirs = []
    name = 'rtrg_c'
    sources = ['rtrg_c.c']
    libraries = []

    if 'MKL' in environ:
        libraries += ['mkl_rt']
        include_dirs += ["/opt/intel/mkl/include"]
        library_dirs += ["/opt/intel/mkl/lib/intel64"]
        compiler_args += ['-DCBLAS']
        compiler_args += ['-DMKL']
    else:
        libraries += ['lapack']
        if 'CBLAS' in environ:
            compiler_args += ['-DCBLAS']
            libraries += ['cblas']
            #libraries += ['mkl_rt']
        else:
            libraries += ['blas']

        if 'LAPACK_C' in environ:
            compiler_args += ['-DLAPACK_C']

    parallel_modifiers = ('PARALLEL', 'PARALLEL_EXTRAPOLATION', 'PARALLEL_EXTRA_DIMS')

    need_omp = False
    for modifier in parallel_modifiers:
        if modifier in environ:
            compiler_args += ['-D' + modifier]
            need_omp = True

    if need_omp:
        compiler_args += ['-fopenmp']
        linker_args += ['-fopenmp']

    if 'DEBUG' in environ:
        compiler_args += ['-DDEBUG']

    if 'ANALYZE' in environ:
        compiler_args += ['-DANALYZE']

    module = Extension(
            name,
            sources = sources,
            include_dirs = include_dirs,
            library_dirs = library_dirs,
            libraries = libraries,
            extra_compile_args = compiler_args,
            extra_link_args = linker_args
            )
    setup(ext_modules = [module])


if __name__ == '__main__':
    main()
