#!/bin/env python
"""
Benchmarks for Kondo FRTRG solver
"""
import numpy as np
import cProfile
import pstats
try:
       from pstats import SortKey
except ImportError:
       SortKey = lambda:None
       SortKey.CUMULATIVE = None

import settings
from kondo import Kondo

try:
    # These functions are only available if preprocessor flag ANALYZE
    # was defined when compiling rtrg_c:
    from rtrg_c import get_statistics, reset_statistics
except ImportError:
    def get_statistics(*args): return -1
    def reset_statistics(): pass

def benchmark_kondo(parameters, solver_opts, save=False):
    kondo = Kondo(**parameters)
    pr = cProfile.Profile()
    pr.enable()
    try:
        kondo.run(**solver_opts)
    except KeyboardInterrupt as exception:
        settings.logger.exception("Benchmark interrupted!")
    pr.disable()
    ps = pstats.Stats(pr).sort_stats(SortKey.CUMULATIVE)
    return kondo, ps

def main(list_length=100):
    solver_opts = dict(rtol=1e-6, atol=1e-8)
    parameter_dicts = [
            #dict(nmax=12, voltage_branches=4, omega=10., d=1e9, vac= 5., vdc=4., unitary_transformation=False, padding= 0, compact=0, include_Ga=True, solve_integral_exactly=True, integral_method=-1),
            #dict(nmax=12, voltage_branches=4, omega=10., d=1e9, vac= 5., vdc=4., unitary_transformation=False, padding= 0, compact=0),
            #dict(nmax=12, voltage_branches=4, omega=10., d=1e9, vac= 5., vdc=4., unitary_transformation= True, padding= 0, compact=0),
            #dict(nmax=12, voltage_branches=4, omega=10., d=1e9, vac= 5., vdc=4., unitary_transformation= True, padding= 8, compact=0),
            #dict(nmax=24, voltage_branches=4, omega=10., d=1e9, vac=12., vdc=6., unitary_transformation=False, padding= 0, compact=0),
            #dict(nmax=24, voltage_branches=4, omega=10., d=1e9, vac=12., vdc=6., unitary_transformation= True, padding= 0, compact=0),
            #dict(nmax=24, voltage_branches=4, omega=10., d=1e9, vac=12., vdc=6., unitary_transformation= True, padding=16, compact=0),
            #dict(nmax=24, voltage_branches=0, omega=10., d=1e9, vac=12., vdc=0., unitary_transformation= True, padding=16, compact=0),
            #dict(nmax=32, voltage_branches=0, omega=10., d=1e9, vac=20., vdc=0., unitary_transformation= True, padding=24, compact=2),
            #dict(nmax=128, voltage_branches=0, omega=10., d=1e6, vac=150., vdc=0., unitary_transformation=True, padding=80, compact=2),
            dict(nmax=400, voltage_branches=0, omega=10., d=1e6, vac=250., vdc=0., unitary_transformation=True, padding=600, compact=2, include_Ga=True),
            ]
    for parameters in parameter_dicts:
        settings.logger.info("Starting:")
        settings.logger.info(parameters)
        reset_statistics()
        kondo, ps = benchmark_kondo(parameters, solver_opts)
        if get_statistics() > 0:
            print("Matrix multiplication statistics:")
            print("Total number of matrix products:", get_statistics())
            for flags in range(0x40):
                count = get_statistics(flags)
                if count > 0:
                    print(f"For flags={hex(flags):4}:{count:8}, per iteration:{count/kondo.total_iterations:8.3}")
        print()
        ps.print_stats(list_length)
        print()


if __name__ == "__main__":
    main()
