#!/usr/bin/env python3

import numpy as np
from inspect import signature
from reservoirmatrix import *

def rm_extended(shape, s=1):
    '''
    Generate random matrix of given shape with symmetry s.
    If s==1 or s==-1: matrix[::-1 on every axis] == s*matrix.conjugate()
    '''
    matrix = np.random.normal(size=shape) + 1j * np.random.normal(size=shape)
    return matrix + s*matrix[tuple(slice(None,None,-1) for s in shape)].conjugate()


def test_einsum(func, nmax=10, vb=3, padding=0, symmetric=True):
    gp = GlobalRGproperties(nmax=nmax, padding=padding, voltage_branches=vb, symmetric=symmetric)
    gp.energies = 10.*np.arange(-nmax, nmax+1, dtype=np.complex128)
    sig = signature(func)
    numargs = len(sig.parameters)
    return_type = sig.return_annotation
    args = []
    for symmetries in np.indices(3*np.ones(numargs, dtype=int)).T.reshape((3**numargs, numargs))-1:
        test_einsum_helper(func, RGfunction, gp, symmetries)
    if return_type == RGobj:
        for symmetries in np.indices(3*np.ones(numargs, dtype=int)).T.reshape((3**numargs, numargs))-1:
            test_einsum_helper(func, ReservoirMatrix, gp, symmetries)

def test_einsum_helper(func, default_type, gp, symmetries):
    sig = signature(func)
    rgobjs = []
    vb = gp.voltage_branches
    if vb is None:
        base_shape = (2*gp.nmax+1, 2*gp.nmax+1)
    else:
        base_shape = (2*vb+1, 2*gp.nmax+1, 2*gp.nmax+1)
    for (sym, parameter) in zip(symmetries, sig.parameters.values()):
        dtype = parameter.annotation
        if dtype == RGobj:
            dtype = default_type
        if dtype == RGfunction:
            rgobjs.append(RGfunction(gp, rm_extended(base_shape, sym), symmetry=sym))
        elif dtype == ReservoirMatrix:
            matrix = ReservoirMatrix(gp, sym)
            matrix[0,0] = RGfunction(gp, rm_extended(base_shape, sym), symmetry=sym)
            matrix[0,1] = RGfunction(gp, rm_extended(base_shape, 0), symmetry=0)
            if sym:
                matrix[1,0] = sym*matrix[0,1].floquetConjugate()
            else:
                matrix[1,0] = RGfunction(gp, rm_extended(base_shape, 0), symmetry=0)
            if gp.symmetric and sym:
                matrix[1,1] = matrix[0,0].copy()
            else:
                matrix[1,1] = RGfunction(gp, rm_extended(base_shape, sym), symmetry=sym)
            rgobjs.append(matrix)
        else:
            raise TypeError('Unexpected type')
    result = func(*rgobjs)
    gp.symmetric = False
    for obj in rgobjs:
        obj.symmetry = 0
    ref_result = func(*rgobjs)
    if isinstance(result, RGfunction):
        assert np.allclose(result.values, ref_result.values)
    elif isinstance(result, ReservoirMatrix):
        for i in range(2):
            for j in range(2):
                assert np.allclose(result[i,j].values, ref_result[i,j].values)
    elif isinstance(result, tuple):
        # Assume that it's a tuple of ReservoirMatrices since that's the only use case
        for k in range(len(result)):
            for i in range(2):
                for j in range(2):
                    assert np.allclose(result[k][i,j].values, ref_result[k][i,j].values)
    else:
        raise TypeError('Unexpected return type %s'%type(result))

def test_reservoirmatrix(*args, **kwargs):
    #test_einsum(einsum_32_13, *args, **kwargs)
    test_einsum(product_combinations, *args, **kwargs)
    test_einsum(einsum_34_12_43, *args, **kwargs)
    test_einsum(einsum_34_12_43_double, *args, **kwargs)


if __name__ == '__main__':
    test_reservoirmatrix()
