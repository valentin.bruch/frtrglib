#!/bin/env python
"""
Benchmark for matrix multiplication from rtrg_c.
"""

from test_rtrg_c import *
from timeit import timeit
from time import process_time

def benchmark(
            shots : 'number of repetitions in each run' = 5,
            runs : 'number of samples for statistics' = 10,
            n : 'rows of first matrix' = 1000,
            k : 'columns of first matrix' = 1000,
            m : 'columns of second matrix' = 1000,
            p : 'cutoff, size of matrix extrapolation' = 600,
            extra_dims : 'extra dimensions of matrices' = (),
            progress : 'show progress if nonzero' = 0,
        ):
    """
    Benchmark for general matrix multiplication
    """
    print('Benchmark: general matrix multiplication')
    if extra_dims:
        print('           shape (%d, %d, %d) + %s, cutoff %d'%(n, k, m, extra_dims, p))
    else:
        print('           shape (%d, %d, %d), cutoff %d'%(n, k, m, p))
    times_fu = []
    times_fp = []
    times_u = []
    times_p = []
    if not extra_dims:
        times_fs = []
        times_s = []
    for i in range(runs):
        if progress:
            print('%7.2f: Starting run%4d / %d'%(process_time(), i+1, runs), flush=True)
        a = frm(n, k, *extra_dims)
        b = frm(k, m, *extra_dims)
        times_fu.append(timeit('multiply(a, b, 0)', number=shots, globals=dict(a=a, b=b, multiply=rtrg_c.multiply_extended)))
        times_fp.append(timeit('multiply(a, b, p)', number=shots, globals=dict(a=a, b=b, multiply=rtrg_c.multiply_extended, p=p)))
        if not extra_dims:
            times_fs.append(timeit('a @ b', number=shots, globals=dict(a=a, b=b)))

        a = rm(n, k, *extra_dims)
        b = rm(k, m, *extra_dims)
        times_u.append(timeit('multiply(a, b, 0)', number=shots, globals=dict(a=a, b=b, multiply=rtrg_c.multiply_extended)))
        times_p.append(timeit('multiply(a, b, p)', number=shots, globals=dict(a=a, b=b, multiply=rtrg_c.multiply_extended, p=p)))
        if not extra_dims:
            times_s.append(timeit('a @ b', number=shots, globals=dict(a=a, b=b)))

    times_fu = np.array(times_fu)
    times_fp = np.array(times_fp)
    times_u = np.array(times_u)
    times_p = np.array(times_p)
    if not extra_dims:
        times_fs = np.array(times_fs)
        times_s = np.array(times_s)

    if (runs <= 10):
        print('Raw results:')
        print('Fortran unpadded: ', ','.join('%7.4f'%t for t in times_fu))
        print('Fortran padded:   ', ','.join('%7.4f'%t for t in times_fp))
        if not extra_dims:
            print('Fortran standard: ', ','.join('%7.4f'%t for t in times_fs))

        print('Standard unpadded:', ','.join('%7.4f'%t for t in times_u))
        print('Standard padded:  ', ','.join('%7.4f'%t for t in times_p))
        if not extra_dims:
            print('Standard standard:', ','.join('%7.4f'%t for t in times_s))

    print('\nTime (in s) for %d shots (%d samples):'%(shots, runs))
    print('                  minimum  median   mean    std')
    print('Fortran unpadded: %7.4f %7.4f %7.4f %7.4f'%(times_fu.min(), np.median(times_fu), times_fu.mean(), times_fu.std()))
    print('Fortran padded:   %7.4f %7.4f %7.4f %7.4f'%(times_fp.min(), np.median(times_fp), times_fp.mean(), times_fp.std()))
    if not extra_dims:
        print('Fortran standard: %7.4f %7.4f %7.4f %7.4f'%(times_fs.min(), np.median(times_fs), times_fs.mean(), times_fs.std()))
                                   
    print('Standard unpadded:%7.4f %7.4f %7.4f %7.4f'%(times_u.min(), np.median(times_u), times_u.mean(), times_u.std()))
    print('Standard padded:  %7.4f %7.4f %7.4f %7.4f'%(times_p.min(), np.median(times_p), times_p.mean(), times_p.std()))
    if not extra_dims:
        print('Standard standard:%7.4f %7.4f %7.4f %7.4f'%(times_s.min(), np.median(times_s), times_s.mean(), times_s.std()))
    print()


def benchmark_sym(
            shots : 'number of repetitions in each run' = 5,
            runs : 'number of samples for statistics' = 10,
            n : 'rows of first matrix' = 1000,
            k : 'columns of first matrix' = 1000,
            m : 'columns of second matrix' = 1000,
            p : 'cutoff, size of matrix extrapolation' = 600,
            s1 : 'symmetry of first matrix' = 1,
            s2 : 'symmetry of second matrix' = 1,
            extra_dims : 'extra dimensions of matrices' = (),
            progress : 'show progress if nonzero' = 0,
        ):
    """
    Benchmark for symmetry-assisted matrix multiplication
    """
    print('Benchmark: symmetric matrix multiplication')
    if extra_dims:
        print('           shape (%d, %d, %d) + %s, cutoff %d'%(n, k, m, extra_dims, p))
    else:
        print('           shape (%d, %d, %d), cutoff %d'%(n, k, m, p))
    times_fu = []
    times_fp = []
    times_u = []
    times_p = []
    if not extra_dims:
        times_fs = []
        times_s = []
    for i in range(runs):
        if progress:
            print('%7.2f: Starting run%4d / %d'%(process_time(), i+1, runs), flush=True)
        a = fsrm(n, k, *extra_dims, s=s1)
        b = fsrm(k, m, *extra_dims, s=s2)
        times_fu.append(timeit('multiply(a, b, 0, s)', number=shots, globals=dict(a=a, b=b, s=s1*s2, multiply=rtrg_c.multiply_extended)))
        times_fp.append(timeit('multiply(a, b, p, s)', number=shots, globals=dict(a=a, b=b, s=s1*s2, multiply=rtrg_c.multiply_extended, p=p)))
        if not extra_dims:
            times_fs.append(timeit('a @ b', number=shots, globals=dict(a=a, b=b)))

        a = srm(n, k, *extra_dims, s=s1)
        b = srm(k, m, *extra_dims, s=s2)
        times_u.append(timeit('multiply(a, b, 0, s)', number=shots, globals=dict(a=a, b=b, s=s1*s2, multiply=rtrg_c.multiply_extended)))
        times_p.append(timeit('multiply(a, b, p, s)', number=shots, globals=dict(a=a, b=b, s=s1*s2, multiply=rtrg_c.multiply_extended, p=p)))
        if not extra_dims:
            times_s.append(timeit('a @ b', number=shots, globals=dict(a=a, b=b)))

    times_fu = np.array(times_fu)
    times_fp = np.array(times_fp)
    times_u = np.array(times_u)
    times_p = np.array(times_p)
    if not extra_dims:
        times_fs = np.array(times_fs)
        times_s = np.array(times_s)

    if (runs <= 10):
        print('Raw results:')
        print('Fortran unpadded: ', ','.join('%7.4f'%t for t in times_fu))
        print('Fortran padded:   ', ','.join('%7.4f'%t for t in times_fp))
        if not extra_dims:
            print('Fortran standard: ', ','.join('%7.4f'%t for t in times_fs))

        print('Standard unpadded:', ','.join('%7.4f'%t for t in times_u))
        print('Standard padded:  ', ','.join('%7.4f'%t for t in times_p))
        if not extra_dims:
            print('Standard standard:', ','.join('%7.4f'%t for t in times_s))

    print('\nTime (in s) for %d shots (%d samples):'%(shots, runs))
    print('                  minimum  median   mean    std')
    print('Fortran unpadded: %7.4f %7.4f %7.4f %7.4f'%(times_fu.min(), np.median(times_fu), times_fu.mean(), times_fu.std()))
    print('Fortran padded:   %7.4f %7.4f %7.4f %7.4f'%(times_fp.min(), np.median(times_fp), times_fp.mean(), times_fp.std()))
    if not extra_dims:
        print('Fortran standard: %7.4f %7.4f %7.4f %7.4f'%(times_fs.min(), np.median(times_fs), times_fs.mean(), times_fs.std()))
                                   
    print('Standard unpadded:%7.4f %7.4f %7.4f %7.4f'%(times_u.min(), np.median(times_u), times_u.mean(), times_u.std()))
    print('Standard padded:  %7.4f %7.4f %7.4f %7.4f'%(times_p.min(), np.median(times_p), times_p.mean(), times_p.std()))
    if not extra_dims:
        print('Standard standard:%7.4f %7.4f %7.4f %7.4f'%(times_s.min(), np.median(times_s), times_s.mean(), times_s.std()))
    print()


if __name__ == '__main__':
    # For large matrices intel MKL is faster than openBLAS
    benchmark(    shots=5, runs=10, n=2000, k=2000, m=2000, p=1600)
    benchmark_sym(shots=5, runs=10, n=2000, k=2000, m=2000, p=1600)
    benchmark(    shots=10, runs=10, n=1000, k=1000, m=1000, p=600)
    benchmark_sym(shots=10, runs=10, n=1000, k=1000, m=1000, p=600)
    benchmark(    shots=10, runs=10, n=200, k=200, m=200, p=150, extra_dims=(15,))
    benchmark_sym(shots=10, runs=10, n=200, k=200, m=200, p=150, extra_dims=(15,))
    # For these parameters openBLAS can be faster than intel MKL
    benchmark(    shots=1000, runs=30, n=21, k=21, m=21, p=12, extra_dims=(7,))
    benchmark_sym(shots=1000, runs=30, n=21, k=21, m=21, p=12, extra_dims=(7,))
