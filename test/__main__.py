#!/usr/bin/env python3

import test_kondo
import test_reservoirmatrix
import test_rtrg_c

if __name__ == '__main__':
    test_rtrg_c.test()
    test_reservoirmatrix.test_reservoirmatrix()
    test_kondo.main()
