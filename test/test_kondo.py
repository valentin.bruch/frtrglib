#!/usr/bin/env python3
from kondo import Kondo
import numpy as np
import settings

def main():
    # TODO: This does not work as expected!
    kondo_params = dict(omega=10, nmax=12, voltage_branches=4, vdc=7, vac=4, include_Ga=True)
    run_params = dict(rtol=1e-9, atol=1e-11)
    settings.logger.info(f"Testing Kondo with parameters {kondo_params}")
    settings.logger.info("default implementation")
    kondo_j = Kondo(unitary_transformation=True, **kondo_params)
    solver_j = kondo_j.run(**run_params)
    kondo_mu = Kondo(unitary_transformation=False, **kondo_params)
    solver_mu = kondo_mu.run(**run_params)
    #if np.allclose(solver_j.y, solver_mu.y):
    nmax= kondo_params["nmax"]
    vb = kondo_params["voltage_branches"]
    if np.allclose(kondo_mu.deltaGammaL[nmax,nmax], kondo_j.deltaGammaL[nmax,nmax]) \
            and np.allclose(kondo_mu.gammaL[nmax,nmax], kondo_j.gammaL[nmax,nmax]):
        settings.logger.info("default implementation: J and mu agree")
    else:
        settings.logger.warning("default implementation: J and mu disagree!")

    settings.logger.info("ignore symmetries")
    settings.defaults["IGNORE_SYMMETRIES"] = 1
    settings.defaults.update_globals()
    kondo_j_ref1 = Kondo(unitary_transformation=True, **kondo_params)
    solver_j_ref1 = kondo_j_ref1.run(**run_params)
    if np.allclose(solver_j.y[:,0], solver_j_ref1.y[:solver_j.y.shape[0],0]):
        settings.logger.info("J: default implementation and IGNORE_SYMMETRIES agree")
    else:
        settings.logger.warning("J: default implementation and IGNORE_SYMMETRIES disagree!")
    kondo_mu_ref1 = Kondo(unitary_transformation=False, **kondo_params)
    solver_mu_ref1 = kondo_mu_ref1.run(**run_params)
    if np.allclose(solver_mu.y[:,0], solver_mu_ref1.y[:solver_mu.y.shape[0],0]):
        settings.logger.info("mu: default implementation and IGNORE_SYMMETRIES agree")
    else:
        settings.logger.warning("mu: default implementation and IGNORE_SYMMETRIES disagree!")

    settings.logger.info("reference implementation")
    settings.defaults["USE_REFERENCE_IMPLEMENTATION"] = 1
    settings.defaults.update_globals()
    kondo_j_ref2 = Kondo(unitary_transformation=True, **kondo_params)
    solver_j_ref2 = kondo_j_ref2.run(**run_params)
    if np.allclose(solver_j.y[:,0], solver_j_ref2.y[:solver_j.y.shape[0],0]):
        settings.logger.info("J: default and reference implementation agree")
    else:
        settings.logger.warning("J: default and reference implementation disagree!")
    kondo_mu_ref2 = Kondo(unitary_transformation=False, **kondo_params)
    solver_mu_ref2 = kondo_mu_ref2.run(**run_params)
    if np.allclose(solver_mu.y[:,0], solver_mu_ref2.y[:solver_mu.y.shape[0],0]):
        settings.logger.info("mu: default and reference implementation agree")
    else:
        settings.logger.warning("mu: default and reference implementation disagree!")

if __name__ == '__main__':
    main()
