#!/usr/bin/env python3

# Copyright 2022 Valentin Bruch <valentin.bruch@rwth-aachen.de>
# License: MIT
"""
Kondo FRTRG, generate plots based on save data
"""

import matplotlib.pyplot as plt
import matplotlib.colors as mplcolors
import argparse
import pandas as pd
import numpy as np
import os
from scipy.interpolate import bisplrep, bisplev, splrep, BSpline
from scipy.optimize import curve_fit
from frtrg_kondo import settings
from logging import _levelToName
from frtrg_kondo.data_management import DataManager, KondoImport

# reference_values maps (omega, vdc, vac) to reference values
REFERENCE_VALUES = {
        (10, 24, 22) : dict(
                idc=2.2563205,
                idc_err=2e-4,
                iac=0.7540501,
                iac_err=1e-4,
                gdc=0.07053006,
                gdc_err=2e-7,
                acphase=0.06650,
                acphase_err=2e-5,
            ),
        }

TK_VOLTAGE = 3.30743526735


def main():
    """
    Parse command line arguments and call other functions
    """
    parser = argparse.ArgumentParser(description=main.__doc__)
    valid_functions = {f.__name__:f for f in globals().values()
            if type(f) == type(main) \
                and getattr(f, '__module__', '') == '__main__' \
                and f.__name__[0] != '_'}
    parser.add_argument("functions", type=str, nargs='+', metavar="function",
            choices=valid_functions.keys(), help="functions to be called")
    parser.add_argument("--db_filename", metavar='file', type=str,
            help = "SQLite database file for saving metadata")
    parser.add_argument("--log_level", metavar="str", type=str,
            default = _levelToName.get(settings.logger.level, "INFO"),
            choices = ("INFO", "DEBUG", "WARNING", "ERROR"),
            help = "logging level")
    parser.add_argument("--omega", type=float,
            help="Frequency, units of Tk")
    parser.add_argument("--method", type=str, choices=('J', 'mu'),
            help="method: J or mu")
    parser.add_argument("--nmax", metavar='int', type=int,
            help="Floquet matrix size")
    parser.add_argument("--padding", metavar='int', type=int,
            help="Floquet matrix ppadding")
    parser.add_argument("--voltage_branches", metavar='int', type=int,
            help="Voltage branches")
    parser.add_argument("--resonant_dc_shift", metavar='int', type=int,
            help="resonant DC shift")
    parser.add_argument("--vdc", metavar='float', type=float,
            help="Vdc, units of Tkrg")
    fourier_coef_group = parser.add_mutually_exclusive_group()
    fourier_coef_group.add_argument("--vac", metavar='float', type=float,
            help="Vac, units of Tkrg")
    fourier_coef_group.add_argument("--fourier_coef", metavar='tuple', type=float, nargs='*',
            help="Voltage Fourier arguments, units of omega")
    parser.add_argument("--d", metavar='float', type=float,
            help="D (UV cutoff), units of Tkrg")
    parser.add_argument("--xL", metavar='float', type=float, nargs='+', default=0.5,
            help="Asymmetry, 0 < xL < 1")
    parser.add_argument("--compact", metavar='int', type=int,
            help="compact FRTRG implementation (0,1, or 2)")
    parser.add_argument("--solver_tol_rel", metavar="float", type=float,
            help="Solver relative tolerance")
    parser.add_argument("--solver_tol_abs", metavar="float", type=float,
            help="Solver absolute tolerance")
    args = parser.parse_args()

    options = args.__dict__
    db_filename = options.pop("db_filename", None)
    if db_filename is not None:
        settings.defaults.DB_CONNECTION_STRING = "sqlite:///" + os.path.abspath(db_filename)
    settings.defaults.logger.setLevel(options.pop("log_level"))
    settings.defaults.update_globals()

    dm = DataManager()
    for name in options.pop("functions"):
        valid_functions[name](dm=dm, **options)
    plt.show()

def plot(dm, **parameters):
    """
    Plot as function of that physical parameters (omega, vdc, or vac) that is
    not specified. This function required that two out of these three physical
    parameters are given.
    """
    if not 'omega' in parameters or parameters['omega'] is None:
        parameter = "omega"
    if not 'vdc' in parameters or parameters['vdc'] is None:
        parameter = "vdc"
    if not 'vac' in parameters or parameters['vac'] is None:
        parameter = "vac"
    table = dm.list(**parameters)
    table.sort_values(parameter, inplace=True)
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=False)
    ax1.set_ylabel("Idc")
    ax2.set_ylabel("Gdc")
    ax3.set_ylabel("Iac")
    ax4.set_ylabel("AC phase")
    ax3.set_xlabel("Vdc")
    ax4.set_xlabel("Vdc")
    ax1.plot(table[parameter], table.dc_current, '.-')
    ax2.plot(table[parameter], np.pi*table.dc_conductance, '.-')
    ax3.plot(table[parameter], table.ac_current_abs, '.-')
    ax4.plot(table[parameter], table.ac_current_phase, '.-')

def plot_vdc0(dm, **parameters):
    for name in ("omega", "vdc", "vac"):
        parameters.pop(name, None)
    results = dm.list(vdc=0, **parameters)
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=True)
    ax1.set_ylabel("Vac")
    ax3.set_ylabel("Vac")
    ax3.set_xlabel("Ω")
    ax4.set_xlabel("Ω")
    # DC conductance
    ax2.set_title('DC conductance')
    plot = ax2.scatter(
            results.omega,
            results.vac,
            c=np.pi*results.dc_conductance,
            marker='x',
            cmap=plt.cm.viridis)
    fig.colorbar(plot, ax=ax2)
    # AC current (abs)
    ax3.set_title('AC current')
    plot = ax3.scatter(
            results.omega,
            results.vac,
            c=results.ac_current_abs,
            marker='x',
            cmap=plt.cm.viridis)
    fig.colorbar(plot, ax=ax3)
    # AC current (phase)
    ax4.set_title('AC phase')
    phase_norm = plt.Normalize(-np.pi, np.pi)
    plot = ax4.scatter(
            results.omega,
            results.vac,
            c=results.ac_current_phase,
            marker='+',
            norm=phase_norm,
            cmap=plt.cm.hsv)
    fig.colorbar(plot, ax=ax4)

def plot_overview(dm, omega, **parameters):
    """
    Plot overview of dc and ac current and dc conductance for harmonic driving
    at fixed frequency as function of Vdc and Vac.
    """
    parameters.pop("method", None)
    results_J = dm.list(omega=omega, method='J', **parameters)
    results_mu = dm.list(omega=omega, method='mu', **parameters)

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=True)
    ax1.set_ylabel("Vac")
    ax3.set_ylabel("Vac")
    ax3.set_xlabel("Vdc")
    ax4.set_xlabel("Vdc")

    # DC current
    idc_min = min(
            results_J.size and results_J.dc_current.min(),
            results_mu.size and results_mu.dc_current.min())
    idc_max = max(
            results_J.size and results_J.dc_current.max(),
            results_mu.size and results_mu.dc_current.max())
    idc_norm = plt.Normalize(idc_min, idc_max)
    ax1.set_title('DC current')
    ax1.scatter(
            results_J.vdc,
            results_J.vac,
            c=results_J.dc_current,
            marker='x',
            norm=idc_norm,
            cmap=plt.cm.viridis)
    plot = ax1.scatter(
            results_mu.vdc,
            results_mu.vac,
            c=results_mu.dc_current,
            marker='+',
            norm=idc_norm,
            cmap=plt.cm.viridis)
    fig.colorbar(plot, ax=ax1)
    # DC conductance
    gmin = np.pi*min(
            results_J.dc_conductance.min() if results_J.size else 1,
            results_mu.dc_conductance.min() if results_mu.size else 1)
    gmax = np.pi*max(
            results_J.dc_conductance.max() if results_J.size else 0,
            results_mu.dc_conductance.max() if results_mu.size else 0)
    gnorm = plt.Normalize(gmin, gmax)
    ax2.set_title('DC conductance')
    ax2.scatter(
            results_J.vdc,
            results_J.vac,
            c=np.pi*results_J.dc_conductance,
            marker='x',
            norm=gnorm,
            cmap=plt.cm.viridis)
    plot = ax2.scatter(
            results_mu.vdc,
            results_mu.vac,
            c=np.pi*results_mu.dc_conductance,
            marker='+',
            norm=gnorm,
            cmap=plt.cm.viridis)
    fig.colorbar(plot, ax=ax2)
    # AC current (abs)
    ax3.set_title('AC current')
    iac_min = min(
            results_J.size and results_J.ac_current_abs.min(),
            results_mu.size and results_mu.ac_current_abs.min())
    iac_max = max(
            results_J.size and results_J.ac_current_abs.max(),
            results_mu.size and results_mu.ac_current_abs.max())
    iac_norm = plt.Normalize(iac_min, iac_max)
    ax3.scatter(
            results_J.vdc,
            results_J.vac,
            c=results_J.ac_current_abs,
            marker='x',
            norm=iac_norm,
            cmap=plt.cm.viridis)
    plot = ax3.scatter(
            results_mu.vdc,
            results_mu.vac,
            c=results_mu.ac_current_abs,
            marker='+',
            norm=iac_norm,
            cmap=plt.cm.viridis)
    fig.colorbar(plot, ax=ax3)
    # AC current (phase)
    ax4.set_title('AC phase')
    phase_norm = plt.Normalize(-np.pi, np.pi)
    ax4.scatter(
            results_J.vdc,
            results_J.vac,
            c=results_J.ac_current_phase,
            marker='x',
            norm=phase_norm,
            cmap=plt.cm.hsv)
    plot = ax4.scatter(
            results_mu.vdc,
            results_mu.vac,
            c=results_mu.ac_current_phase,
            marker='+',
            norm=phase_norm,
            cmap=plt.cm.hsv)
    fig.colorbar(plot, ax=ax4)

def plot_interpolate(
        dm,
        omega,
        dc_res=100,
        ac_res=100,
        vdc_min=0,
        vac_min=0,
        vdc_max=100,
        vac_max=50,
        **parameters):
    """
    Plot overview of dc and ac current and dc conductance for harmonic driving
    at fixed frequency as function of Vdc and Vac.
    """
    results_all = dm.list(omega=omega, **parameters)
    results = results_all.loc[(results_all["solver_flags"] & DataManager.SOLVER_FLAGS["simplified_initial_conditions"]) == 0]
    j = results.method == "J"
    mu = results.method == "mu"
    vac_max = min(vac_max, results.vac.max())
    vdc_max = min(vdc_max, results.vdc.max())

    vac_arr = np.linspace(vac_max/(2*ac_res), vac_max*(1 - 0.5/ac_res), ac_res)
    vdc_arr = np.linspace(vdc_max/(2*dc_res), vdc_max*(1 - 0.5/dc_res), dc_res)
    extent = (0, vdc_max, 0, vac_max)

    # Interpolate
    show_J = j.sum() > 10
    show_mu = mu.sum() > 10
    all_figs = []
    all_axes = []
    if show_J:
        gdc_J_tck = bisplrep(
                results.vac[j],
                results.vdc[j],
                results.dc_conductance[j],
                s=1e-5,
                kx=3,
                ky=3)
        idc_J_tck = bisplrep(
                results.vac[j],
                results.vdc[j],
                results.dc_current[j],
                s=1e-5,
                kx=3,
                ky=3)
        iac_J_tck = bisplrep(
                results.vac[j],
                results.vdc[j],
                results.ac_current_abs[j],
                s=1e-5,
                kx=3,
                ky=3)
        phase_J_tck = bisplrep(
                results.vac[j],
                results.vdc[j],
                results.ac_current_phase[j],
                s=1e-5,
                kx=3,
                ky=3)
        gdc_g_J_interp = bisplev(vac_arr, vdc_arr, gdc_J_tck)
        gdc_i_J_interp = bisplev(vac_arr, vdc_arr, idc_J_tck, dy=1)
        idc_J_interp   = bisplev(vac_arr, vdc_arr, idc_J_tck)
        iac_J_interp   = bisplev(vac_arr, vdc_arr, iac_J_tck)
        phase_J_interp = bisplev(vac_arr, vdc_arr, phase_J_tck)
        # create figure
        fig_j,  ((ax1_j, ax2_j), (ax3_j, ax4_j)) = plt.subplots(2, 2, sharex=True, sharey=True)
        ax1_j.set_ylabel("Vac")
        ax3_j.set_ylabel("Vac")
        ax3_j.set_xlabel("Vdc")
        ax4_j.set_xlabel("Vdc")
        all_figs.append(fig_j)
        all_axes.append((ax1_j, ax2_j, ax3_j, ax4_j))
    if show_mu:
        gdc_mu_tck = bisplrep(
                results.vac[mu],
                results.vdc[mu],
                results.dc_conductance[mu],
                s=5e-6,
                kx=3,
                ky=3)
        idc_mu_tck = bisplrep(
                results.vac[mu],
                results.vdc[mu],
                results.dc_current[mu],
                s=5e-6,
                kx=3,
                ky=3)
        iac_mu_tck = bisplrep(
                results.vac[mu],
                results.vdc[mu],
                results.ac_current_abs[mu],
                s=5e-6,
                kx=3,
                ky=3)
        phase_mu_tck = bisplrep(
                results.vac[mu],
                results.vdc[mu],
                results.ac_current_phase[mu],
                s=5e-6,
                kx=3,
                ky=3)
        gdc_g_mu_interp = bisplev(vac_arr, vdc_arr, gdc_mu_tck)
        gdc_i_mu_interp = bisplev(vac_arr, vdc_arr, idc_mu_tck, dy=1)
        idc_mu_interp   = bisplev(vac_arr, vdc_arr, idc_mu_tck)
        iac_mu_interp   = bisplev(vac_arr, vdc_arr, iac_mu_tck)
        phase_mu_interp = bisplev(vac_arr, vdc_arr, phase_mu_tck)
        # create figure
        fig_mu, ((ax1_mu, ax2_mu), (ax3_mu, ax4_mu)) = plt.subplots(2, 2, sharex=True, sharey=True)
        ax1_mu.set_ylabel("Vac")
        ax3_mu.set_ylabel("Vac")
        ax3_mu.set_xlabel("Vdc")
        ax4_mu.set_xlabel("Vdc")
        all_figs.append(fig_mu)
        all_axes.append((ax1_mu, ax2_mu, ax3_mu, ax4_mu))

    # DC current
    idc_norm = plt.Normalize(results.dc_current.min(), results.dc_current.max())
    for (fig, axes) in zip(all_figs, all_axes):
        ax = axes[0]
        ax.set_title('DC current')
        ax.scatter(results.vdc[j], results.vac[j], c=results.dc_current[j], marker='x', norm=idc_norm, cmap=plt.cm.viridis)
        plot = ax.scatter(results.vdc[mu], results.vac[mu], c=results.dc_current[mu], marker='+', norm=idc_norm, cmap=plt.cm.viridis)
        fig.colorbar(plot, ax=ax)
    if show_mu:
        ax1_mu.imshow(idc_mu_interp, extent=extent, aspect='auto', cmap=plt.cm.viridis, norm=idc_norm, origin='lower')
    if show_J:
        ax1_j.imshow(idc_J_interp, extent=extent, aspect='auto', cmap=plt.cm.viridis, norm=idc_norm, origin='lower')

    # DC conductance
    gnorm = plt.Normalize(np.pi*results.dc_conductance.min(), np.pi*results.dc_conductance.max())
    for (fig, axes) in zip(all_figs, all_axes):
        ax = axes[1]
        ax.set_title('DC conductance')
        ax.scatter(results.vdc[j], results.vac[j], c=np.pi*results.dc_conductance[j], marker='x', norm=gnorm, cmap=plt.cm.viridis)
        plot = ax.scatter(results.vdc[mu], results.vac[mu], c=np.pi*results.dc_conductance[mu], marker='+', norm=gnorm, cmap=plt.cm.viridis)
        fig.colorbar(plot, ax=ax)
    if show_mu:
        ax2_mu.imshow(np.pi*gdc_g_mu_interp, extent=extent, aspect='auto', cmap=plt.cm.viridis, norm=gnorm, origin='lower')
    if show_J:
        ax2_j.imshow(np.pi*gdc_g_J_interp, extent=extent, aspect='auto', cmap=plt.cm.viridis, norm=gnorm, origin='lower')

    # AC current (abs)
    iac_norm = plt.Normalize(results.ac_current_abs.min(), results.ac_current_abs.max())
    for (fig, axes) in zip(all_figs, all_axes):
        ax = axes[2]
        ax.set_title('AC current')
        ax.scatter(results.vdc[j], results.vac[j], c=results.ac_current_abs[j], marker='x', norm=iac_norm, cmap=plt.cm.viridis)
        plot = ax.scatter(results.vdc[mu], results.vac[mu], c=results.ac_current_abs[mu], marker='+', norm=iac_norm, cmap=plt.cm.viridis)
        fig.colorbar(plot, ax=ax)
    if show_mu:
        ax3_mu.imshow(iac_mu_interp, extent=extent, aspect='auto', cmap=plt.cm.viridis, norm=iac_norm, origin='lower')
    if show_J:
        ax3_j.imshow(iac_J_interp, extent=extent, aspect='auto', cmap=plt.cm.viridis, norm=iac_norm, origin='lower')

    # AC current (phase)
    phase_norm = plt.Normalize(-np.pi, np.pi)
    for (fig, axes) in zip(all_figs, all_axes):
        ax = axes[3]
        ax.set_title('AC phase')
        ax.scatter(results.vdc[j], results.vac[j], c=results.ac_current_phase[j], marker='x', norm=phase_norm, cmap=plt.cm.hsv)
        plot = ax.scatter(results.vdc[mu], results.vac[mu], c=results.ac_current_phase[mu], marker='+', norm=phase_norm, cmap=plt.cm.hsv)
        fig.colorbar(plot, ax=ax)
    if show_mu:
        ax4_mu.imshow(phase_mu_interp, extent=extent, aspect='auto', cmap=plt.cm.hsv, norm=phase_norm, origin='lower')
    if show_J:
        ax4_j.imshow(phase_J_interp, extent=extent, aspect='auto', cmap=plt.cm.hsv, norm=phase_norm, origin='lower')

    # Create figure
    fig, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(2, 3, sharex=True, sharey=True)
    ax1.set_ylabel("Vac")
    ax4.set_ylabel("Vac")
    ax4.set_xlabel("Vdc")
    ax5.set_xlabel("Vdc")
    ax6.set_xlabel("Vdc")
    ax1.set_title("DC current: mu vs. J")
    ax2.set_title("DC conductance: mu vs. J for G")
    ax3.set_title("DC conductance: G vs. I for mu")
    ax6.set_title("DC conductance: G vs. I for J")
    ax4.set_title("AC current: mu vs. J")
    ax5.set_title("AC phase: mu vs. J")
    check_norm = plt.Normalize(-0.03, 0.03)
    if show_J and show_mu:
        ax1.imshow(
                (idc_mu_interp - idc_J_interp)/idc_mu_interp,
                extent=extent,
                aspect='auto',
                cmap=plt.cm.seismic,
                norm=check_norm,
                origin='lower')
        ax2.imshow(
                (gdc_g_mu_interp - gdc_g_J_interp)/gdc_g_J_interp,
                extent=extent,
                aspect='auto',
                cmap=plt.cm.seismic,
                norm=check_norm,
                origin='lower')
        ax4.imshow(
                (iac_mu_interp - iac_J_interp)/iac_mu_interp,
                extent=extent,
                aspect='auto',
                cmap=plt.cm.seismic,
                norm=check_norm,
                origin='lower')
        ax5.imshow(
                phase_mu_interp - phase_J_interp,
                extent=extent,
                aspect='auto',
                cmap=plt.cm.seismic,
                norm=check_norm,
                origin='lower')
    if show_mu:
        img = ax3.imshow(
                (gdc_g_mu_interp - gdc_i_mu_interp)/gdc_g_mu_interp,
                extent=extent,
                aspect='auto',
                cmap=plt.cm.seismic,
                norm=check_norm,
                origin='lower')
    if show_J:
        img = ax6.imshow(
                (gdc_g_J_interp - gdc_i_J_interp)/gdc_g_J_interp,
                extent=extent,
                aspect='auto',
                cmap=plt.cm.seismic,
                norm=check_norm,
                origin='lower')
    fig.colorbar(img, ax=[ax1,ax2,ax3,ax4,ax5,ax6])

def plot_comparison(dm, omega, d=1e9, **parameters):
    """
    Compare current and dc conductance computed from both methods (J and mu) at
    fixed frequency as function of Vdc and Vac.
    Only data points which exist for both methods with equal Vdc, Vac, D and
    frequency are considered.
    """
    for name in ("omega", "method"):
        parameters.pop(name, None)
    results_J = dm.list(omega=omega, method='J', d=d, **parameters).sort_values(["vdc","vac"])
    results_mu = dm.list(omega=omega, method='mu', d=d, **parameters).sort_values(["vdc","vac"])
    results_J.vac = results_J.vac.round(6)
    results_J.vdc = results_J.vdc.round(6)
    results_mu.vac = results_mu.vac.round(6)
    results_mu.vdc = results_mu.vdc.round(6)
    merged = pd.merge(
            results_J,
            results_mu,
            how="inner",
            on=["vdc","vac","d","omega"],
            suffixes=("_J", "_mu"))

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=True)
    ax1.set_ylabel("Vac")
    ax3.set_ylabel("Vac")
    ax3.set_xlabel("Vdc")
    ax4.set_xlabel("Vdc")

    # DC current
    idc_diff = merged.dc_current_J - merged.dc_current_mu
    idc_max = abs(idc_diff).max()
    idc_norm = plt.Normalize(-idc_max, idc_max)
    ax1.set_title('DC current')
    plot = ax1.scatter(merged.vdc, merged.vac, c=idc_diff, marker='o', norm=idc_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax1)
    # DC conductance
    gdc_diff = np.pi*(merged.dc_conductance_J - merged.dc_conductance_mu)
    gdc_max = abs(gdc_diff).max()
    gdc_norm = plt.Normalize(-gdc_max, gdc_max)
    ax2.set_title('DC conductance')
    plot = ax2.scatter(merged.vdc, merged.vac, c=gdc_diff, marker='o', norm=gdc_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax2)
    # AC current (abs)
    iac_diff = merged.ac_current_abs_J - merged.ac_current_abs_mu
    iac_max = abs(iac_diff).max()
    iac_norm = plt.Normalize(-iac_max, iac_max)
    ax3.set_title('AC current')
    plot = ax3.scatter(merged.vdc, merged.vac, c=iac_diff, marker='o', norm=iac_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax3)
    # AC current (phase)
    phase_diff = (merged.ac_current_phase_J - merged.ac_current_phase_mu + np.pi) % (2*np.pi) - np.pi
    phase_max = abs(phase_diff).max()
    phase_norm = plt.Normalize(-phase_max, phase_max)
    ax4.set_title('AC phase')
    plot = ax4.scatter(merged.vdc, merged.vac, c=phase_diff, marker='o', norm=phase_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax4)

def plot_comparison_relative(dm, omega, d=1e9, **parameters):
    """
    Compare current and dc conductance computed from both methods (J and mu) at
    fixed frequency as function of Vdc and Vac.
    Only data points which exist for both methods with equal Vdc, Vac, D and
    frequency are considered.
    """
    for name in ("omega", "method"):
        parameters.pop(name, None)
    results_J = dm.list(omega=omega, method='J', d=d, **parameters).sort_values(["vdc","vac"])
    results_mu = dm.list(omega=omega, method='mu', d=d, **parameters).sort_values(["vdc","vac"])
    results_J.vac = results_J.vac.round(6)
    results_J.vdc = results_J.vdc.round(6)
    results_mu.vac = results_mu.vac.round(6)
    results_mu.vdc = results_mu.vdc.round(6)
    merged = pd.merge(
            results_J,
            results_mu,
            how="inner",
            on=["vdc","vac","d","omega"],
            suffixes=("_J", "_mu"))

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=True)
    ax1.set_ylabel("Vac")
    ax3.set_ylabel("Vac")
    ax3.set_xlabel("Vdc")
    ax4.set_xlabel("Vdc")

    # DC current
    idc_diff = 2*(merged.dc_current_J - merged.dc_current_mu)/(merged.dc_current_J + merged.dc_current_mu)
    idc_diff[merged.vdc == 0] = 0
    idc_max = abs(idc_diff).max()
    idc_norm = plt.Normalize(-idc_max, idc_max)
    ax1.set_title('DC current')
    plot = ax1.scatter(merged.vdc, merged.vac, s=5+5e3*np.abs(idc_diff), c=idc_diff, marker='o', norm=idc_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax1)
    # DC conductance
    gdc_diff = 2*(merged.dc_conductance_J - merged.dc_conductance_mu)/(merged.dc_conductance_J + merged.dc_conductance_mu)
    gdc_max = abs(gdc_diff).max()
    gdc_norm = plt.Normalize(-gdc_max, gdc_max)
    ax2.set_title('DC conductance')
    plot = ax2.scatter(merged.vdc, merged.vac, s=5+5e3*np.abs(gdc_diff), c=gdc_diff, marker='o', norm=gdc_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax2)
    # AC current (abs)
    iac_diff = 2*(merged.ac_current_abs_J - merged.ac_current_abs_mu)/(merged.ac_current_abs_J + merged.ac_current_abs_mu)
    iac_max = abs(iac_diff).max()
    iac_norm = plt.Normalize(-iac_max, iac_max)
    ax3.set_title('AC current')
    plot = ax3.scatter(merged.vdc, merged.vac, s=5+1e4*np.abs(iac_diff), c=iac_diff, marker='o', norm=iac_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax3)
    # AC current (phase)
    phase_diff = (merged.ac_current_phase_J - merged.ac_current_phase_mu + np.pi) % (2*np.pi) - np.pi
    phase_max = abs(phase_diff).max()
    phase_norm = plt.Normalize(-phase_max, phase_max)
    ax4.set_title('AC phase')
    plot = ax4.scatter(merged.vdc, merged.vac, s=5+2e4*np.abs(phase_diff), c=phase_diff, marker='o', norm=phase_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax4)

def plot_floquet_matrices(kondo : KondoImport, norm_min=1e-6):
    fig, axes = plt.subplots(3, 3)
    axes = axes.flatten()
    gamma = kondo.gamma
    idx = kondo.voltage_branches if gamma.ndim == 3 else ...
    try:
        axes[0].set_title("Γ")
        img = axes[0].imshow(np.abs(gamma[idx]), norm=mplcolors.LogNorm(norm_min))
        fig.colorbar(img, ax=axes[0])
    except AttributeError:
        pass
    try:
        axes[1].set_title("Z")
        img = axes[1].imshow(np.abs(kondo.z[idx]), norm=mplcolors.LogNorm(norm_min))
        fig.colorbar(img, ax=axes[1])
    except AttributeError:
        pass
    try:
        axes[2].set_title("ΓL")
        img = axes[2].imshow(np.abs(kondo.gammaL), norm=mplcolors.LogNorm(norm_min))
        fig.colorbar(img, ax=axes[2])
    except AttributeError:
        pass
    try:
        axes[3].set_title("δΓL")
        img = axes[3].imshow(np.abs(kondo.deltaGammaL), norm=mplcolors.LogNorm(norm_min))
        fig.colorbar(img, ax=axes[3])
    except AttributeError:
        pass
    try:
        axes[4].set_title("δΓ")
        img = axes[4].imshow(np.abs(kondo.deltaGamma[1 if gamma.ndim == 3 else ...]), norm=mplcolors.LogNorm(norm_min))
        fig.colorbar(img, ax=axes[4])
    except AttributeError:
        pass
    try:
        axes[5].set_title("yL")
        img = axes[5].imshow(np.abs(kondo.yL), norm=mplcolors.LogNorm(norm_min))
        fig.colorbar(img, ax=axes[5])
    except AttributeError:
        pass
    try:
        axes[6].set_title("G2")
        img = axes[6].imshow(np.abs(kondo.g2[:,:,idx].transpose(0,2,1,3).reshape((4*kondo.nmax+2, 4*kondo.nmax+2))), norm=mplcolors.LogNorm(norm_min))
        fig.colorbar(img, ax=axes[6])
    except AttributeError:
        pass
    try:
        axes[7].set_title("G3")
        img = axes[7].imshow(np.abs(kondo.g3[:,:,idx].transpose(0,2,1,3).reshape((4*kondo.nmax+2, 4*kondo.nmax+2))), norm=mplcolors.LogNorm(norm_min))
        fig.colorbar(img, ax=axes[7])
    except AttributeError:
        pass
    try:
        axes[8].set_title("I")
        img = axes[8].imshow(np.abs(kondo.current.transpose(0,2,1,3).reshape((4*kondo.nmax+2, 4*kondo.nmax+2))), norm=mplcolors.LogNorm(norm_min))
        fig.colorbar(img, ax=axes[8])
    except AttributeError:
        pass

def check_results(dm, max_num=5, **parameters):
    table = dm.list(**parameters)
    counter = 0
    for index, row in table.iterrows():
        for kondo in KondoImport.read_from_h5(os.path.join(row.dirname, row.basename), row.hash):
            try:
                plot_floquet_matrices(kondo)
            except KeyboardInterrupt:
                kondo._h5file.close()
                return
            counter += 1
            if counter >= max_num:
                settings.logger.warning("Maximum number of files read, stopping here")
                return
        if not kondo._owns_h5file:
            kondo._h5file.close()

def check_convergence(dm, **parameters):
    if not ("omega" in parameters and "vdc" in parameters and "vac" in parameters):
        settings.logger.warning("check_convergence expects specification of all physical parameters")
    table = dm.list(**parameters)
    mod = (table.solver_flags & DataManager.SOLVER_FLAGS["simplified_initial_conditions"]) != 0
    j = (~mod) & (table.method == "J")
    mu = (~mod) & (table.method == "mu")
    d_j = table.d[j]
    d_mu = table.d[mu]
    d_mod = table.d[mod]
    x = 1/np.log(table.d)**3
    x_j = x[j]
    x_mu = x[mu]
    x_mod = x[mod]
    drtol = table.d*table.solver_tol_rel
    norm = mplcolors.LogNorm(max(drtol.min(), 0.05), min(drtol.max(), 500))
    c_j = drtol[j]
    c_mu = drtol[mu]
    c_mod = drtol[mod]
    s_j = 0.05 * table.nmax[j]**2
    s_mu = 0.08 * table.nmax[mu]**2
    s_mod = 0.08 * table.nmax[mod]**2
    lw_j = 0.3 * table.voltage_branches[j]
    lw_mu = 0.3 * table.voltage_branches[mu]
    lw_mod = 0.3 * table.voltage_branches[mod]

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=False)
    ax3.set_xlabel("D")
    ax4.set_xlabel("D")
    ax1.set_xscale("log")

    # Show reference values
    try:
        reference_values = REFERENCE_VALUES[(parameters['omega'], parameters['vdc'], parameters['vac'])]
    except KeyError:
        reference_values = None

    if reference_values is not None:
        x = (table.d.min(), table.d.max())
        ax1.plot(x, (reference_values['idc'],reference_values['idc']), 'k:')
        ax1.fill_between(
                x,
                (reference_values['idc']-reference_values['idc_err'], reference_values['idc']-reference_values['idc_err']),
                (reference_values['idc']+reference_values['idc_err'], reference_values['idc']+reference_values['idc_err']),
                color='#80808080')
        ax2.plot(x, (reference_values['gdc'],reference_values['gdc']), 'k:')
        ax2.fill_between(
                x,
                (reference_values['gdc']-reference_values['gdc_err'], reference_values['gdc']-reference_values['gdc_err']),
                (reference_values['gdc']+reference_values['gdc_err'], reference_values['gdc']+reference_values['gdc_err']),
                color='#80808080')
        ax3.plot(x, (reference_values['iac'],reference_values['iac']), 'k:')
        ax3.fill_between(
                x,
                (reference_values['iac']-reference_values['iac_err'], reference_values['iac']-reference_values['iac_err']),
                (reference_values['iac']+reference_values['iac_err'], reference_values['iac']+reference_values['iac_err']),
                color='#80808080')
        ax4.plot(x, (reference_values['acphase'],reference_values['acphase']), 'k:')
        ax4.fill_between(
                x,
                (reference_values['acphase']-reference_values['acphase_err'], reference_values['acphase']-reference_values['acphase_err']),
                (reference_values['acphase']+reference_values['acphase_err'], reference_values['acphase']+reference_values['acphase_err']),
                color='#80808080')

    ax1.set_title("DC current")
    ax1.scatter(d_j, table.dc_current[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax1.scatter(d_mu, table.dc_current[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax1.scatter(d_mod, table.dc_current[mod], marker='*', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)

    ax2.set_title("DC conductance")
    ax2.scatter(d_j, table.dc_conductance[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax2.scatter(d_mu, table.dc_conductance[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax2.scatter(d_mod, table.dc_conductance[mod], marker='*', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)

    ax3.set_title("AC current")
    ax3.scatter(d_j, table.ac_current_abs[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax3.scatter(d_mu, table.ac_current_abs[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax3.scatter(d_mod, table.ac_current_abs[mod], marker='*', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)

    ax4.set_title("AC phase")
    ax4.scatter(d_j, table.ac_current_phase[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax4.scatter(d_mu, table.ac_current_phase[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax4.scatter(d_mod, table.ac_current_phase[mod], marker='*', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)

def check_convergence_nmax(dm, **parameters):
    if not ("omega" in parameters and "vdc" in parameters and "vac" in parameters and "d"):
        settings.logger.warning("check_convergence_nmax expects specification of all physical parameters and D")
    table = dm.list(**parameters)
    mod = (table.solver_flags & DataManager.SOLVER_FLAGS["simplified_initial_conditions"]) != 0
    j = (~mod) & (table.method == "J")
    mu = (~mod) & (table.method == "mu")
    norm = mplcolors.LogNorm(table.voltage_branches.min(), table.voltage_branches.max())
    nmax_j = table.nmax[j]
    nmax_mu = table.nmax[mu]
    nmax_mod = table.nmax[mod]
    s_j = -3*np.log(table.solver_tol_rel[j])
    s_mu = -3*np.log(table.solver_tol_rel[mu])
    s_mod = -3*np.log(table.solver_tol_rel[mod])
    lw_j = -0.1*np.log(table.solver_tol_abs[j])
    lw_mu = -0.1*np.log(table.solver_tol_abs[mu])
    lw_mod = -0.1*np.log(table.solver_tol_abs[mod])
    c_j = table.voltage_branches[j]
    c_mu = table.voltage_branches[mu]
    c_mod = table.voltage_branches[mod]

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=False)
    ax3.set_xlabel("nmax")
    ax4.set_xlabel("nmax")

    # Show reference values
    try:
        reference_values = REFERENCE_VALUES[(parameters['omega'], parameters['vdc'], parameters['vac'])]
    except KeyError:
        reference_values = None

    if reference_values is not None:
        x = (table.nmax.min(), table.nmax.max())
        ax1.plot(x, (reference_values['idc'],reference_values['idc']), 'k:')
        ax1.fill_between(
                x,
                (reference_values['idc']-reference_values['idc_err'], reference_values['idc']-reference_values['idc_err']),
                (reference_values['idc']+reference_values['idc_err'], reference_values['idc']+reference_values['idc_err']),
                color='#80808080')
        ax2.plot(x, (reference_values['gdc'],reference_values['gdc']), 'k:')
        ax2.fill_between(
                x,
                (reference_values['gdc']-reference_values['gdc_err'], reference_values['gdc']-reference_values['gdc_err']),
                (reference_values['gdc']+reference_values['gdc_err'], reference_values['gdc']+reference_values['gdc_err']),
                color='#80808080')
        ax3.plot(x, (reference_values['iac'],reference_values['iac']), 'k:')
        ax3.fill_between(
                x,
                (reference_values['iac']-reference_values['iac_err'], reference_values['iac']-reference_values['iac_err']),
                (reference_values['iac']+reference_values['iac_err'], reference_values['iac']+reference_values['iac_err']),
                color='#80808080')
        ax4.plot(x, (reference_values['acphase'],reference_values['acphase']), 'k:')
        ax4.fill_between(
                x,
                (reference_values['acphase']-reference_values['acphase_err'], reference_values['acphase']-reference_values['acphase_err']),
                (reference_values['acphase']+reference_values['acphase_err'], reference_values['acphase']+reference_values['acphase_err']),
                color='#80808080')

    ax1.set_title("DC current")
    ax1.scatter(nmax_j, table.dc_current[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax1.scatter(nmax_mu, table.dc_current[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax1.scatter(nmax_mod, table.dc_current[mod], marker='_', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)

    ax2.set_title("DC conductance")
    ax2.scatter(nmax_j, table.dc_conductance[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax2.scatter(nmax_mu, table.dc_conductance[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax2.scatter(nmax_mod, table.dc_conductance[mod], marker='_', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)

    ax3.set_title("AC current")
    ax3.scatter(nmax_j, table.ac_current_abs[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax3.scatter(nmax_mu, table.ac_current_abs[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax3.scatter(nmax_mod, table.ac_current_abs[mod], marker='_', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)

    ax4.set_title("AC phase")
    ax4.scatter(nmax_j, table.ac_current_phase[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax4.scatter(nmax_mu, table.ac_current_phase[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax4.scatter(nmax_mod, table.ac_current_phase[mod], marker='_', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)


def check_convergence_fit(dm, **parameters):
    if not ("omega" in parameters and "vdc" in parameters and "vac" in parameters):
        settings.logger.warning(
                "check_convergence expects specification of all physical parameters")
    table = dm.list(**parameters)
    mod = (table.solver_flags & DataManager.SOLVER_FLAGS["simplified_initial_conditions"]) != 0
    d_fit_max = 9e11
    d_fit_max_j_nopadding = 9e11
    d_fit_max_j_shifted_nopadding = 2e9
    j = (~mod) & (table.method == "J")
    mu = (~mod) & (table.method == "mu")
    logd_inv_arr = np.linspace(0, 1/np.log10(table.d.min()), 200)
    logd_inv = 1/np.log10(table.d)
    x = 1/np.log10(table.d)
    x3 = 1/np.log10(table.d)**3
    x_j = x[j]
    x_mu = x[mu]
    x_mod = x[mod]
    drtol = table.d*table.solver_tol_rel
    norm = mplcolors.LogNorm(max(drtol.min(), 0.05), min(drtol.max(), 500))
    c_j = drtol[j]
    c_mu = drtol[mu]
    c_mod = drtol[mod]
    s_j = 0.05 * table.nmax[j]**2
    s_mu = 0.08 * table.nmax[mu]**2
    s_mod = 0.08 * table.nmax[mod]**2
    lw_j = 0.3 * table.voltage_branches[j]
    lw_mu = 0.3 * table.voltage_branches[mu]
    lw_mod = 0.3 * table.voltage_branches[mod]

    selections = {}
    for r in range(10):
        suffix = '_%d'%r if r else ''
        if r in table.resonant_dc_shift.values:
            mu_shift = (~mod) \
                    & (table.method == "mu") \
                    & (table.d <= d_fit_max) \
                    & (table.resonant_dc_shift == r)
            mu_mod_shift = mod \
                    & (table.method == "mu") \
                    & (table.d <= d_fit_max) \
                    & (table.resonant_dc_shift == r)
            j_shift = (~mod) \
                    & (table.method == "J") \
                    & (     ((table.padding > 0) & (table.d <= d_fit_max)) \
                            | (table.d <= (d_fit_max_j_nopadding if r == 0 else d_fit_max_j_shifted_nopadding)) ) \
                    & (table.resonant_dc_shift == r)
            j_mod_shift = mod \
                    & (table.method == "J") \
                    & (     ((table.padding > 0) & (table.d <= d_fit_max)) \
                            | (table.d <= (d_fit_max_j_nopadding if r == 0 else d_fit_max_j_shifted_nopadding)) ) \
                    & (table.resonant_dc_shift == r)
            if mu_shift.any():
                selections["mu" + suffix] = mu_shift
            if mu_mod_shift.any():
                selections["mu_mod" + suffix] = mu_mod_shift
            if j_shift.any():
                selections["J" + suffix] = j_shift
            if j_mod_shift.any():
                selections["J_mod" + suffix] = j_mod_shift

    fit_func = lambda logd_inv, a, b, c: a + b * logd_inv**c

    x_mean = x3[~mod].mean()
    x_max = x3[~mod].max()
    x_shifted = x3[~mod] - x_mean
    s = (~mod).sum()
    s_xx = (x_shifted**2).sum()

    xmod_mean = x3[mod].mean()
    xmod_max = x3[mod].max()
    xmod_shifted = x3[mod] - xmod_mean
    smod = mod.sum()
    smod_xx = (xmod_shifted**2).sum()

    # Create figure
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=False)
    ax3.set_xlabel("1/log10(D)")
    ax4.set_xlabel("1/log10(D)")

    # Show reference values
    try:
        reference_values = REFERENCE_VALUES[(
            parameters['omega'],
            parameters['vdc'],
            parameters['vac'],
            )]
    except KeyError:
        reference_values = None

    if reference_values is not None:
        ax1.plot(
                (0, logd_inv_arr[-1]),
                (reference_values['idc'], reference_values['idc']),
                'k:')
        ax1.fill_between(
                (0, logd_inv_arr[-1]),
                (reference_values['idc']-reference_values['idc_err'], reference_values['idc']-reference_values['idc_err']),
                (reference_values['idc']+reference_values['idc_err'], reference_values['idc']+reference_values['idc_err']),
                color='#80808080')
        ax2.plot(
                (0, logd_inv_arr[-1]),
                (reference_values['gdc'], reference_values['gdc']),
                'k:')
        ax2.fill_between(
                (0, logd_inv_arr[-1]),
                (reference_values['gdc']-reference_values['gdc_err'], reference_values['gdc']-reference_values['gdc_err']),
                (reference_values['gdc']+reference_values['gdc_err'], reference_values['gdc']+reference_values['gdc_err']),
                color='#80808080')
        ax3.plot(
                (0, logd_inv_arr[-1]),
                (reference_values['iac'], reference_values['iac']),
                'k:')
        ax3.fill_between(
                (0, logd_inv_arr[-1]),
                (reference_values['iac']-reference_values['iac_err'], reference_values['iac']-reference_values['iac_err']),
                (reference_values['iac']+reference_values['iac_err'], reference_values['iac']+reference_values['iac_err']),
                color='#80808080')
        ax4.plot(
                (0, logd_inv_arr[-1]),
                (reference_values['acphase'], reference_values['acphase']),
                'k:')
        ax4.fill_between(
                (0, logd_inv_arr[-1]),
                (reference_values['acphase']-reference_values['acphase_err'], reference_values['acphase']-reference_values['acphase_err']),
                (reference_values['acphase']+reference_values['acphase_err'], reference_values['acphase']+reference_values['acphase_err']),
                color='#80808080')

    ax1.set_title("DC current")
    ax1.scatter(
            x_j,
            table.dc_current[j],
            marker = 'x',
            s = s_j,
            c = c_j,
            linewidths = lw_j,
            norm = norm)
    ax1.scatter(
            x_mu,
            table.dc_current[mu],
            marker = '+',
            s = s_mu,
            c = c_mu,
            linewidths = lw_mu,
            norm = norm)
    ax1.scatter(
            x_mod,
            table.dc_current[mod],
            marker = '*',
            s = s_mod,
            c = c_mod,
            linewidths = lw_mod,
            norm = norm)

    bb = table.dc_current[~mod].mean()
    aa = (x_shifted*table.dc_current[~mod]).sum()/s_xx
    #ax1.plot(logd_inv_arr, aa*(logd_inv_arr**3 - x_mean) + bb, linewidth=0.5)

    bbmod = table.dc_current[mod].mean()
    aamod = (xmod_shifted*table.dc_current[mod]).sum()/smod_xx
    #ax1.plot(logd_inv_arr, aamod*(logd_inv_arr**3 - xmod_mean) + bbmod, linewidth=0.5)

    for name, selection in selections.items():
        try:
            (a, b, c), covar = curve_fit(
                    fit_func,
                    logd_inv[selection],
                    table.dc_current[selection],
                    (bb-aa*x_mean, aa, 3))
            aerr, berr, cerr = covar.diagonal()**0.5
            print(f"DC current ({name:9}): a={a:.9}±{aerr:.9}, b={b:.9}±{berr:.9}, c={c:.9}±{cerr:.9}")
            ax1.plot(logd_inv_arr, fit_func(logd_inv_arr, a, b, c), label=name)
            ax1.plot((0,logd_inv_arr[-1]), (a,a), ':', label=name)
        except:
            pass

    ax2.set_title("DC conductance")
    ax2.scatter(
            x_j,
            table.dc_conductance[j],
            marker = 'x',
            s = s_j,
            c = c_j,
            linewidths = lw_j,
            norm = norm)
    ax2.scatter(
            x_mu,
            table.dc_conductance[mu],
            marker = '+',
            s = s_mu,
            c = c_mu,
            linewidths = lw_mu,
            norm = norm)
    ax2.scatter(
            x_mod,
            table.dc_conductance[mod],
            marker = '*',
            s = s_mod,
            c = c_mod,
            linewidths = lw_mod,
            norm = norm)

    ax3.set_title("AC current")
    ax3.scatter(
            x_j,
            table.ac_current_abs[j],
            marker = 'x',
            s = s_j,
            c = c_j,
            linewidths = lw_j,
            norm = norm)
    ax3.scatter(
            x_mu,
            table.ac_current_abs[mu],
            marker = '+',
            s = s_mu,
            c = c_mu,
            linewidths = lw_mu,
            norm = norm)
    ax3.scatter(
            x_mod,
            table.ac_current_abs[mod],
            marker = '*',
            s = s_mod,
            c = c_mod,
            linewidths = lw_mod,
            norm = norm)

    b = table.ac_current_abs[~mod].mean()
    a = (x_shifted*table.ac_current_abs[~mod]).sum()/s_xx
    #ax3.plot(logd_inv_arr, a*(logd_inv_arr**3 - x_mean) + b, linewidth=0.5)

    bmod = table.ac_current_abs[mod].mean()
    amod = (xmod_shifted*table.ac_current_abs[mod]).sum()/smod_xx
    #ax3.plot(logd_inv_arr, amod*(logd_inv_arr**3 - xmod_mean) + bmod, linewidth=0.5)

    for name, selection in selections.items():
        try:
            (a, b, c), covar = curve_fit(
                    fit_func,
                    logd_inv[selection],
                    table.ac_current_abs[selection],
                    (bb-aa*x_mean, aa, 3))
            aerr, berr, cerr = covar.diagonal()**0.5
            print(f"AC current ({name:9}): a={a:.9}±{aerr:.9}, b={b:.9}±{berr:.9}, c={c:.9}±{cerr:.9}")
            ax3.plot(logd_inv_arr, fit_func(logd_inv_arr, a, b, c), label=name)
            ax3.plot((0,logd_inv_arr[-1]), (a,a), ':', label=name)
        except:
            pass

    ax4.set_title("AC phase")
    ax4.scatter(
            x_j,
            table.ac_current_phase[j],
            marker='x',
            s=s_j,
            c=c_j,
            linewidths=lw_j,
            norm=norm)
    ax4.scatter(
            x_mu,
            table.ac_current_phase[mu],
            marker='+',
            s=s_mu,
            c=c_mu,
            linewidths=lw_mu,
            norm=norm)
    ax4.scatter(
            x_mod,
            table.ac_current_phase[mod],
            marker='*',
            s=s_mod,
            c=c_mod,
            linewidths=lw_mod,
            norm=norm)

    b = table.ac_current_phase[~mod].mean()
    a = (x_shifted*table.ac_current_phase[~mod]).sum()/s_xx
    #ax4.plot(logd_inv_arr, a*(logd_inv_arr**3 - x_mean) + b, linewidth=0.5)

    bmod = table.ac_current_phase[mod].mean()
    amod = (xmod_shifted*table.ac_current_phase[mod]).sum()/smod_xx
    #ax4.plot(logd_inv_arr, amod*(logd_inv_arr**3 - xmod_mean) + bmod, linewidth=0.5)

    for name, selection in selections.items():
        try:
            (a, b, c), covar = curve_fit(
                    fit_func,
                    logd_inv[selection],
                    table.ac_current_phase[selection],
                    (bb-aa*x_mean, aa, 3))
            aerr, berr, cerr = covar.diagonal()**0.5
            print(f"AC phase ({name:9}): a={a:.9}±{aerr:.9}, b={b:.9}±{berr:.9}, c={c:.9}±{cerr:.9}")
            ax4.plot(logd_inv_arr, fit_func(logd_inv_arr, a, b, c), label=name)
            ax4.plot((0,logd_inv_arr[-1]), (a,a), ':', label=name)
        except:
            pass


if __name__ == '__main__':
    main()
