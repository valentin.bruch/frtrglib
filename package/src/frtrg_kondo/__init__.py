"""
FRTRG Kondo package:
Floquet real-time renormalization group for periodically driven Kondo model.

Modules defined here
* kondo: implements RG equations for the Kondo model
* rtrg: defines data types for Floquet matrices
* compact_rtrg: use special symmetries for more efficient handling of some
  data types of rtrg.py
* reservoirmatrix.py: data type for vertices in RG equations
* settings: global settings used in this package.
* data_management.py: data management module
* gen_data: generate data, should be called directly
* main: show plots from saved data, should be called directly
* rtrg_c: functions required by rtrg implemented in C using BLAS
* rtrg_cublas: same functions as in rtrg_c using CUBLAS
* drive_gate_voltage: very basic extension of kondo module to case of driven
  coupling (or gate voltage in a quantum dot setup)
"""
__version__ = "0.14.8"
