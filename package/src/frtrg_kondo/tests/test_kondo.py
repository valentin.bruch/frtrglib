#!/usr/bin/env python3
from kondo import Kondo
import numpy as np
import settings

def main():
    for vdc in (0, 5, 100):
        settings.logger.info(f"Testing Kondo: omega=10, vac=0, vdc={vdc}, nmax=0")
        kondo_j = Kondo(unitary_transformation=True, omega=10, nmax=0, voltage_branches=2, vdc=vdc, vac=0)
        kondo_mu = Kondo(unitary_transformation=False, omega=10, nmax=0, voltage_branches=2, vdc=vdc, vac=0)
        kondo_j.run()
        kondo_mu.run()
        assert kondo_j.hash() == kondo_mu.hash()
    settings.logger.info(f"Testing Kondo: omega=10, vac=0, vdc=0, nmax=6")
    kondo_j = Kondo(unitary_transformation=True, omega=10, nmax=6, voltage_branches=2, vdc=0, vac=0)
    kondo_mu = Kondo(unitary_transformation=False, omega=10, nmax=6, voltage_branches=2, vdc=0, vac=0)
    solver_j = kondo_j.run()
    solver_mu = kondo_mu.run()
    assert np.allclose(solver_j.y, solver_mu.y)

if __name__ == '__main__':
    main()
