#!/bin/env python
"""
Test functions for library rtrg_c.
"""

import numpy as np
from os import environ
if environ.get("CUBLAS") is None:
    print("Using rtrg_c (no GPU acceleration)", flush=True)
    from frtrg_kondo import rtrg_c
else:
    print("Using rtrg_cublas", flush=True)
    from frtrg_kondo import rtrg_cublas

def matrix_extension_reference(matrix, cutoff):
    """
    Provide a reference implementation for matrix extension.
    """
    assert matrix.ndim == 2
    assert matrix.shape[0] >= cutoff+3
    assert matrix.shape[1] >= cutoff+3
    extended = np.zeros((matrix.shape[0]+2*cutoff, matrix.shape[1]+2*cutoff), dtype=matrix.dtype)
    extended[cutoff:-cutoff, cutoff:-cutoff] = matrix
    for i in range(1, cutoff+1):
        extended[cutoff-i, cutoff-i:2*cutoff+1-i] = (1+0.75*i)*matrix[0,:cutoff+1] - 0.5*i*(matrix[1,1:cutoff+2] + 0.5*matrix[2,2:cutoff+3])
        extended[cutoff-i:2*cutoff+1-i, cutoff-i] = (1+0.75*i)*matrix[:cutoff+1,0] - 0.5*i*(matrix[1:cutoff+2,1] + 0.5*matrix[2:cutoff+3,2])
        bottom = matrix.shape[0] + cutoff - 1
        left = matrix.shape[1] - 1
        extended[bottom+i,left+i:left+cutoff+1+i] = (1+0.75*i)*matrix[-1,-cutoff-1:] - 0.5*i*(matrix[-2,-cutoff-2:-1] + 0.5*matrix[-3,-cutoff-3:-2])
        right = matrix.shape[1] + cutoff - 1
        top = matrix.shape[0] - 1
        extended[top+i:top+cutoff+1+i,right+i] = (1+0.75*i)*matrix[-cutoff-1:,-1] - 0.5*i*(matrix[-cutoff-2:-1,-2] + 0.5*matrix[-cutoff-3:-2,-3])
    return extended


def rm(*shape):
    "Create a C-contiguous complex random matrix of given shape"
    return np.random.normal(size=shape) + 1j * np.random.normal(size=shape)

def srm(*shape, s=1):
    "Create a C-contiguous (anti)symmetric complex random matrix of given shape"
    matrix = rm(*shape)
    return matrix + s*matrix[::-1, ::-1].conjugate()

def frm(*shape):
    "Create an F-contiguous complex random matrix of given shape"
    return rm(*shape[::-1]).T

def fsrm(*shape, s=1):
    "Create an F-contiguous (anti)symmetric complex random matrix of given shape"
    matrix = rm(*shape[::-1])
    return (matrix + s*matrix[...,::-1, ::-1].conjugate()).T


def test_extension(n, m, p, *genargs, gen=frm):
    print("Testing matrix extension", n, m, p, flush=True)
    assert n >= p+3
    assert m >= p+3
    matrix = gen(n, m, *genargs)
    matrix_copy = matrix.copy()
    extended = rtrg_c.extend_matrix(matrix, p)
    reference = matrix_extension_reference(matrix, p)
    if not np.allclose(extended, reference):
        print(np.array(np.abs(extended - reference) < 1e-9, dtype=int))
        raise RuntimeError
    assert np.allclose(matrix, matrix_copy)

def test_multiplication(n, k, m, p, *genargs, gen=frm):
    print("Testing matrix multiplication", n, k, m, p, flush=True)
    assert n >= p+3
    assert m >= p+3
    assert k >= p+3
    a = gen(n, k, *genargs)
    b = gen(k, m, *genargs)
    a_copy = a.copy()
    b_copy = b.copy()
    c1 = rtrg_c.multiply_extended(a, b, p)
    if p > 0:
        c2 = ( rtrg_c.extend_matrix(a, p) @ rtrg_c.extend_matrix(b, p) )[p:-p, p:-p]
        c3 = ( matrix_extension_reference(a, p) @ matrix_extension_reference(b, p) )[p:-p, p:-p]
    else:
        c3 = c2 = a @ b
    if not (np.allclose(c1, c2) and np.allclose(c1, c3)):
        print(np.array(np.abs(c1 - c2) < 1e-9, dtype=int))
        print(np.array(np.abs(c1 - a @ b) < 1e-9, dtype=int))
        raise RuntimeError
    assert np.allclose(a, a_copy)
    assert np.allclose(b, b_copy)

def test_multiplication_sym(n, k, m, p, s1, s2, gen=fsrm):
    print("Testing symmetric matrix multiplication", n, k, m, p, s1, s2, flush=True)
    assert n >= p+3
    assert m >= p+3
    assert k >= p+3
    a = gen(n, k, s=s1)
    b = gen(k, m, s=s2)
    a_copy = a.copy()
    b_copy = b.copy()
    c1 = rtrg_c.multiply_extended(a, b, p, s1*s2)
    c2 = rtrg_c.multiply_extended(a, b, p)
    if not np.allclose(c1, c2):
        for row in np.abs(c1 - c2) < 1e-10:
            print("".join("." if v else "@" for v in row))
        raise RuntimeError;
    if p > 0:
        c3 = ( rtrg_c.extend_matrix(a, p) @ rtrg_c.extend_matrix(b, p) )[p:-p, p:-p]
        c4 = ( matrix_extension_reference(a, p) @ matrix_extension_reference(b, p) )[p:-p, p:-p]
        assert np.allclose(c1, c4)
        assert np.allclose(c1, c3)
    else:
        c3 = a @ b
        assert np.allclose(c1, c3)
    assert np.allclose(a, a_copy)
    assert np.allclose(b, b_copy)

def test_inversion(n, p, l, *genargs, gen=frm):
    print("Testing matrix inversion", n, p, l, flush=True)
    a = gen(n, n, *genargs)
    a_copy = a.copy()
    inv = rtrg_c.invert_extended(a, p, l)
    extended = matrix_extension_reference(a, p)
    if l > 0:
        extended = extended[l:-l, l:-l]
    ref = np.linalg.inv(extended)
    if p > l:
        ref = ref[p-l:l-p, p-l:l-p]
    assert np.allclose(inv, ref)
    assert np.allclose(a, a_copy)

def test_extension_nd(shape, p, gen=frm):
    print("Testing matrix extension, shape %s, %d"%(shape, p))
    a = gen(*shape)
    a_copy = a.copy()
    e = rtrg_c.extend_matrix(a, p)
    assert np.allclose(a, a_copy)
    assert e.ndim == a.ndim
    assert e.shape[2:] == a.shape[2:]
    e = e.reshape((e.shape[0], e.shape[1], np.prod(e.shape[2:])))
    a = a.reshape((a.shape[0], a.shape[1], np.prod(a.shape[2:])))
    for i in range(e.shape[2]):
        assert np.allclose(e[...,i], rtrg_c.extend_matrix(a[...,i], p))

def test_inversion_nd(shape, p, l, gen=frm):
    print("Testing matrix extension, shape %s, %d, %d"%(shape, p, l))
    assert shape[0] == shape[1]
    a = gen(*shape)
    a_copy = a.copy()
    inv = rtrg_c.invert_extended(a, p, l)
    assert np.allclose(a, a_copy)
    assert inv.shape == a.shape
    inv = inv.reshape((inv.shape[0], inv.shape[1], np.prod(inv.shape[2:])))
    a = a.reshape((a.shape[0], a.shape[1], np.prod(a.shape[2:])))
    for i in range(inv.shape[2]):
        assert np.allclose(inv[...,i], rtrg_c.invert_extended(a[...,i], p, l))

def test_multiplication_nd(n, k, m, shape, p, gen=frm):
    print("Testing matrix extension nd, %d %d %d, %s, %d"%(n, k, m, shape, p))
    a = gen(n, k, *shape)
    b = gen(k, m, *shape)
    a_copy = a.copy()
    b_copy = b.copy()
    c = rtrg_c.multiply_extended(a, b, p)
    assert np.allclose(a, a_copy)
    assert np.allclose(b, b_copy)
    assert c.ndim == a.ndim
    assert c.shape[0] == a.shape[0]
    assert c.shape[1:] == b.shape[1:]
    a = a.reshape((a.shape[0], a.shape[1], np.prod(a.shape[2:])))
    b = b.reshape((b.shape[0], b.shape[1], np.prod(b.shape[2:])))
    c = c.reshape((c.shape[0], c.shape[1], np.prod(c.shape[2:])))
    for i in range(c.shape[2]):
        assert np.allclose(c[...,i], rtrg_c.multiply_extended(a[...,i], b[...,i], p))

def test_multiplication_nd_sym(n, k, m, shape, p, s1, s2, gen=fsrm):
    print("Testing symmetric matrix extension nd, %d %d %d, %s, %d, %d %d"%(n, k, m, shape, p, s1, s2))
    a = gen(n, k, *shape, s=s1)
    b = gen(k, m, *shape, s=s2)
    a_copy = a.copy()
    b_copy = b.copy()
    c = rtrg_c.multiply_extended(a, b, p, s1*s2)
    assert np.allclose(a, a_copy)
    assert np.allclose(b, b_copy)
    c2 = rtrg_c.multiply_extended(a, b, p)
    assert c.ndim == a.ndim
    assert c.shape[0] == a.shape[0]
    assert c.shape[1:] == b.shape[1:]
    a = a.reshape((a.shape[0], a.shape[1], np.prod(a.shape[2:])))
    b = b.reshape((b.shape[0], b.shape[1], np.prod(b.shape[2:])))
    c = c.reshape((c.shape[0], c.shape[1], np.prod(c.shape[2:])))
    c2 = c2.reshape((c2.shape[0], c2.shape[1], np.prod(c2.shape[2:])))
    for i in range(c.shape[2]):
        assert np.allclose(c[...,i], rtrg_c.multiply_extended(a[...,i], b[...,i], p, s1*s2))
        assert np.allclose(c[...,i], rtrg_c.multiply_extended(a[...,i], b[...,i], p))
    assert np.allclose(c, c2)

def test_multiplication_cleared_corners(n, k, m, p, c, gen=frm):
    print("Testing matrix multiplication cleared corners", n, k, m, p, c, flush=True)
    assert n >= p+3 and n >= p+c
    assert m >= p+3 and m >= p+c
    assert k >= p+3 and k >= p+c
    assert p > 0 and c > 0
    a = gen(n, k)
    b = gen(k, m)
    a_copy = a.copy()
    b_copy = b.copy()
    c1 = rtrg_c.multiply_extended(a, b, p, 0, c)
    c2 = rtrg_c.multiply_extended(a, b, p, 0, 0)
    for i in range(c):
        c2[i, m-c+i:] = 0.
        c2[n-c+i, :i+1] = 0.
    if not np.allclose(c1, c2):
        print(np.array(np.abs(c1 - c2) < 1e-9, dtype=int))
        print(np.array(np.abs(c1) < 1e-9, dtype=int))
        raise RuntimeError
    assert np.allclose(a, a_copy)
    assert np.allclose(b, b_copy)

def test_multiplication_cleared_corners_sym(n, k, m, p, c, s1=1, s2=1, gen=fsrm):
    print("Testing matrix multiplication cleared corners symmetric", n, k, m, p, c, flush=True)
    assert n >= p+3 and n >= p+c
    assert m >= p+3 and m >= p+c
    assert k >= p+3 and k >= p+c
    assert p > 0 and c > 0
    a = gen(n, k, s=s1)
    b = gen(k, m, s=s2)
    a_copy = a.copy()
    b_copy = b.copy()
    c1 = rtrg_c.multiply_extended(a, b, p, s1*s2, c)
    c2 = rtrg_c.multiply_extended(a, b, p, 0, 0)
    for i in range(c):
        c2[n-c+i, :i+1] = 0.
        c2[i, m-c+i:] = 0.
    if not np.allclose(c1, c2):
        print(np.array(np.abs(c1 - c2) < 1e-9, dtype=int))
        print(np.array(np.abs(c1) < 1e-9, dtype=int))
        raise RuntimeError
    assert np.allclose(a, a_copy)
    assert np.allclose(b, b_copy)

def main():
    """
    Perform some random tests.
    """
    test_extension(10, 10, 5, gen=frm)
    test_extension(10, 10, 6, gen=frm)
    test_extension(10, 10, 7, gen=rm)
    test_extension(10, 11, 7, gen=rm)
    test_extension(10, 15, 7, gen=rm)
    test_extension(10, 9, 5, gen=rm)
    test_extension_nd((10, 10, 7), 5, gen=frm)
    test_extension_nd((10, 10, 3, 4), 6, gen=frm)
    test_extension_nd((10, 14, 3, 2), 5, gen=rm)
    test_extension_nd((9, 10, 5), 5, gen=rm)
    test_inversion(10, 6, 3, gen=frm)
    test_inversion(10, 7, 0, gen=frm)
    test_inversion(10, 7, 4, gen=frm)
    test_inversion(10, 6, 3, gen=rm)
    test_inversion(10, 7, 0, gen=rm)
    test_inversion(10, 7, 4, gen=rm)
    test_inversion_nd((10, 10, 1), 7, 4, gen=frm)
    test_inversion_nd((10, 10, 3), 7, 4, gen=frm)
    test_inversion_nd((10, 10, 3, 2), 7, 5, gen=frm)
    test_inversion_nd((10, 10, 3), 6, 4, gen=rm)
    test_multiplication(10, 10, 10, 5, gen=frm)
    test_multiplication(10, 9, 10, 6, gen=frm)
    test_multiplication(10, 10, 9, 5, gen=rm)
    test_multiplication(11, 10, 10, 7, gen=rm)
    test_multiplication(14, 10, 12, 7, gen=rm)
    test_multiplication(15, 10, 8, 5, gen=rm)
    test_multiplication(9, 13, 10, 6, gen=rm)
    test_multiplication(40, 40, 40, 32, gen=rm)
    test_multiplication(500, 500, 500, 473, gen=frm)
    test_multiplication(500, 500, 501, 473, gen=rm)
    test_multiplication_cleared_corners(10, 10, 10, 5, 5, gen=frm)
    test_multiplication_cleared_corners_sym(10, 10, 10, 5, 5, 1, -1, gen=fsrm)
    test_multiplication_cleared_corners(20, 20, 20, 12, 7, gen=frm)
    test_multiplication_cleared_corners_sym(20, 20, 20, 12, 7, 1, -1, gen=fsrm)
    test_multiplication_cleared_corners(20, 19, 20, 12, 6, gen=frm)
    test_multiplication_cleared_corners_sym(20, 19, 20, 12, 6, 1, -1, gen=fsrm)
    test_multiplication_cleared_corners(19, 20, 20, 12, 6, gen=frm)
    test_multiplication_cleared_corners_sym(19, 20, 20, 12, 6, 1, -1, gen=fsrm)
    test_multiplication_cleared_corners(19, 20, 19, 12, 6, gen=frm)
    test_multiplication_cleared_corners_sym(19, 20, 19, 12, 6, 1, -1, gen=fsrm)
    test_multiplication_sym(10, 10, 10, 0, 1, 1, gen=fsrm)
    test_multiplication_sym(10, 10, 10, 5, 1, 1, gen=fsrm)
    test_multiplication_sym(11, 11, 11, 0, -1, 1, gen=srm)
    test_multiplication_sym(9, 10, 10, 6, 1, 1, gen=fsrm)
    test_multiplication_sym(10, 10, 9, 5, 1, -1, gen=fsrm)
    test_multiplication_sym(10, 9, 9, 6, 1, -1, gen=srm)
    test_multiplication_sym(10, 11, 10, 7, -1, 1, gen=fsrm)
    test_multiplication_sym(11, 11, 10, 7, -1, -1, gen=srm)
    test_multiplication_sym(500, 500, 500, 472, 1, -1, gen=fsrm)
    test_multiplication_sym(500, 500, 501, 472, 1, -1, gen=fsrm)
    test_multiplication_sym(500, 499, 500, 472, 1, -1, gen=fsrm)
    test_multiplication_sym(500, 501, 500, 472, 1, -1, gen=srm)
    test_multiplication_sym(512, 512, 512, 472, 1, -1, gen=srm)
    test_multiplication_sym(512, 513, 512, 472, 1, -1, gen=srm)
    test_multiplication_sym(512, 511, 512, 0, 1, 1, gen=fsrm)
    test_multiplication_sym(512, 513, 512, 0, 1, 1, gen=fsrm)
    test_multiplication_sym(511, 512, 511, 0, 1, 1, gen=fsrm)
    test_multiplication_sym(800, 801, 800, 0, 1, 1, gen=fsrm)
    test_multiplication_sym(1023, 1024, 1023, 0, 1, 1, gen=fsrm)
    test_multiplication_sym(1024, 1025, 1024, 0, 1, 1, gen=fsrm)
    test_multiplication_sym(511, 512, 511, 273, 1, 1, gen=fsrm)
    test_multiplication_nd(10, 10, 10, (2,), 5, gen=frm)
    test_multiplication_nd(10, 9, 10, (1, 2), 6, gen=frm)
    test_multiplication_nd(10, 10, 9, (3, 2), 5, gen=rm)
    test_multiplication_nd(11, 10, 10, (2, 1), 7, gen=rm)
    test_multiplication_nd(40, 40, 40, (4, 3), 32, gen=rm)
    test_multiplication_nd(500, 500, 500, (1,), 473, gen=frm)
    test_multiplication_nd(500, 500, 501, (2,), 473, gen=rm)
    test_multiplication_nd_sym(10, 10, 10, (1,), 5, 1, 1, gen=fsrm)
    test_multiplication_nd_sym(11, 11, 11, (5,), 0, -1, 1, gen=srm)
    test_multiplication_nd_sym(10, 10, 10, (2,), 5, 1, -1, gen=fsrm)
    test_multiplication_nd_sym(10, 9, 9, (2,1), 6, 1, -1, gen=srm)
    test_multiplication_nd_sym(10, 10, 11, (2,3), 5, 1, -1, gen=fsrm)
    test_multiplication_nd_sym(10, 11, 10, (1,2), 7, -1, 1, gen=fsrm)
    test_multiplication_nd_sym(10, 10, 9, (2,3), 5, 1, -1, gen=fsrm)
    test_multiplication_nd_sym(11, 11, 10, (3,5), 7, -1, -1, gen=srm)
    test_multiplication_nd_sym(500, 500, 500, (1,), 472, 1, -1, gen=fsrm)
    test_multiplication_nd_sym(500, 501, 500, (2,), 472, 1, -1, gen=srm)


if __name__ == "__main__":
    main()
