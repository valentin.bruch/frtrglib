#!/usr/bin/env python3

from frtrg_kondo import test_kondo, test_reservoirmatrix, test_rtrg_c

if __name__ == "__main__":
    test_rtrg_c.main()
    test_reservoirmatrix.main()
    test_kondo.main()
