# FRTRG Kondo
Package implementing Floquet real-time renormalization group method for the
Kondo model as discussed in <https://arxiv.org/abs/2206.06263>.

The following parts of this module can be used directly from the command line:
* gen\_data
* plot

Both of these functions need the argument `--db_filename` specifying the
database file in which metadata for all generated data points are stored.

Example usage:
```sh
# Plot overview of all data points in given database at frequency omega=10
python -m frtrg_kondo.plot plot_overview --db_filename=/path/to/db.sqlite --omega=10
# Generate a single data point
python -m frtrg_kondo.gen_data --db_filename=/path/to/db.sqlite --filename=/path/to/HDF5-file.h5 --nmax=14 --method=J --vdc=0 --vac=5 --omega=10
# Generate 21 data points with vdc=0,1,...,20 using 4 parallel processes
OMP_NUM_TREADS=1 python -m frtrg_kondo.gen_data --db_filename=/path/to/db.sqlite --filename=/path/to/HDF5-file.h5 --nmax=14 --voltage_branches=3 --method=mu --vdc 0 20 --steps=21 --vac=5 --omega=10 --threads=4
```

Build with:
```sh
python -m build -w
```
