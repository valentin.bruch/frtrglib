# build with:

from setuptools import setup, Extension
import numpy as np
from os import environ

def main():
    compiler_args = ['-O3','-Wall','-Wextra','-std=c11']
    linker_args = []
    include_dirs = [np.get_include()]
    libraries = ['lapack']

    if 'CBLAS' in environ:
        compiler_args += ['-DCBLAS']
        libraries += ['cblas']
        #libraries += ['mkl_rt']
    else:
        libraries += ['blas']

    if 'LAPACK_C' in environ:
        compiler_args += ['-DLAPACK_C']

    parallel_modifiers = ('PARALLEL', 'PARALLEL_EXTRAPOLATION', 'PARALLEL_EXTRA_DIMS')

    need_omp = False
    for modifier in parallel_modifiers:
        if modifier in environ:
            compiler_args += ['-D' + modifier]
            need_omp = True

    if need_omp:
        compiler_args += ['-fopenmp']
        linker_args += ['-fopenmp']

    if 'DEBUG' in environ:
        compiler_args += ['-DDEBUG']

    if 'ANALYZE' in environ:
        compiler_args += ['-DANALYZE']

    module = Extension(
            "frtrg_kondo.rtrg_c",
            sources = ['src/frtrg_kondo/rtrg_c.c'],
            include_dirs = include_dirs,
            libraries = libraries,
            extra_compile_args = compiler_args,
            extra_link_args = linker_args
            )
    setup(ext_modules = [module])


if __name__ == '__main__':
    main()
