#!/usr/bin/env python3

# Copyright 2022 Valentin Bruch <valentin.bruch@rwth-aachen.de>
# License: MIT
"""
Kondo FRTRG, generate plots based on save data
"""

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm, FuncNorm, LinearSegmentedColormap
import argparse
import pandas as pd
import numpy as np
import os
from numbers import Number
#from util import plot_contours
from final_plots import filter_grid_data, photon_assisted_tunneling, gen_photon_assisted_tunneling, interp_vac0
from scipy.interpolate import bisplrep, bisplev, splrep, BSpline, griddata
from scipy.optimize import curve_fit, leastsq
from PIL import Image
from imageio import imwrite
import settings
from logging import _levelToName
from data_management import DataManager, KondoImport
from kondo import solveTV0_scalar
from scipy.integrate import quad, quad_vec

# color maps with better sampling
viridis = LinearSegmentedColormap.from_list('viridis', plt.cm.viridis.colors, 0x10000)

# reference_values maps (omega, vdc, vac) to reference values
REFERENCE_VALUES = {
        (10, 24, 22) : dict(
                idc=2.2563205,
                idc_err=2e-4,
                iac=0.7540501,
                iac_err=1e-4,
                gdc=0.07053006,
                gdc_err=2e-7,
                acphase=0.06650,
                acphase_err=2e-5,
            ),
        }

#TK_VOLTAGE = 3.44249 # for rtol=1e-10, atol=1e-12, voltage_branches=10
TK_VOLTAGE = 3.4425351 # for rtol=1e-9, atol=1e-11, voltage_branches=10
#TK_VOLTAGE = 3.44334 # with correction
TK_VOLTAGE_O3P = 3.458524 # for rtol=1e-9, atol=1e-11, voltage_branches=10
#TK_VOLTAGE_O3P = 3.4593 # with correction
#TK_VOLTAGE_O3 = 3.44428 # for rtol=1e-8, atol=1e-10, voltage_branches=4
TK_VOLTAGE_ORDER2 = 10.1368086 # for rtol=1e-9, atol=1e-11, voltage_branches=10
#TK_VOLTAGE_ORDER2 = 10.13754 # with correction


TK_GLAZMAN = (3/8 / 0.2)**2 # from  G/G0 = 1 - 0.2*(V/Tkrg)**2  for small V


def main():
    """
    Parse command line arguments and call other functions
    """
    parser = argparse.ArgumentParser(description=main.__doc__)
    valid_functions = {f.__name__:f for f in globals().values()
            if type(f) == type(main) \
                and getattr(f, '__module__', '') == '__main__' \
                and f.__name__[0] != '_'}
    parser.add_argument("functions", type=str, nargs='+', metavar="function",
            choices=valid_functions.keys(), help="functions to be called")
    parser.add_argument("--db_filename", metavar='file', type=str,
            help = "SQLite database file for saving metadata")
    parser.add_argument("--log_level", metavar="str", type=str,
            default = _levelToName.get(settings.logger.level, "INFO"),
            choices = ("INFO", "DEBUG", "WARNING", "ERROR"),
            help = "logging level")
    parser.add_argument("--omega", type=float,
            help="Frequency, units of Tk")
    parser.add_argument("--method", type=str, choices=('J', 'mu'),
            help="method: J or mu")
    parser.add_argument("--nmax", metavar='int', type=int,
            help="Floquet matrix size")
    parser.add_argument("--padding", metavar='int', type=int,
            help="Floquet matrix ppadding")
    parser.add_argument("--voltage_branches", metavar='int', type=int,
            help="Voltage branches")
    parser.add_argument("--resonant_dc_shift", metavar='int', type=int,
            help="resonant DC shift")
    parser.add_argument("--vdc", metavar='float', type=float,
            help="Vdc, units of Tkrg")
    fourier_coef_group = parser.add_mutually_exclusive_group()
    fourier_coef_group.add_argument("--vac", metavar='float', type=float,
            help="Vac, units of Tkrg")
    fourier_coef_group.add_argument("--fourier_coef", metavar='tuple', type=float, nargs='*',
            help="Voltage Fourier arguments, units of omega")
    parser.add_argument("--d", metavar='float', type=float,
            help="D (UV cutoff), units of Tkrg")
    parser.add_argument("--xL", metavar='float', type=float, nargs='+', default=0.5,
            help="Asymmetry, 0 < xL < 1")
    parser.add_argument("--compact", metavar='int', type=int,
            help="compact FRTRG implementation (0,1, or 2)")
    parser.add_argument("--solver_tol_rel", metavar="float", type=float,
            help="Solver relative tolerance")
    parser.add_argument("--solver_tol_abs", metavar="float", type=float,
            help="Solver absolute tolerance")
    parser.add_argument("--truncation_order", metavar="int", type=int, choices=(2,3),
            help="Truncation order of RG equations")
    parser.add_argument("--integral_method", metavar="int", type=int, default=-15,
            help="method for solving frequency integral (-1 for exact solution)")
    parser.add_argument("--include_Ga", metavar="bool", type=int, default=1,
            help="include Ga in RG equations")
    parser.add_argument("--good_flags", metavar="int", type=int, default=0,
            help = "Require these flags")
    parser.add_argument("--bad_flags", metavar="int", type=int, default=0,
            help = "Avoid these flags")
    args = parser.parse_args()

    options = args.__dict__
    db_filename = options.pop("db_filename", None)
    if db_filename is not None:
        settings.defaults.DB_CONNECTION_STRING = "sqlite:///" + os.path.abspath(db_filename)
    settings.defaults.logger.setLevel(options.pop("log_level"))
    settings.defaults.update_globals()

    dm = DataManager()
    for name in options.pop("functions"):
        valid_functions[name](dm=dm, **options)
    plt.show()

def plot(dm, **parameters):
    """
    Plot as function of that physical parameters (omega, vdc, or vac) that is
    not specified. This function required that two out of these three physical
    parameters are given.
    """
    if not 'omega' in parameters or parameters['omega'] is None:
        parameter = "omega"
    if not 'vdc' in parameters or parameters['vdc'] is None:
        parameter = "vdc"
    if not 'vac' in parameters or parameters['vac'] is None:
        parameter = "vac"
    table = dm.list(**parameters)
    table.sort_values(parameter, inplace=True)
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=False)
    ax1.set_ylabel("Idc")
    ax2.set_ylabel("Gdc")
    ax3.set_ylabel("Iac")
    ax4.set_ylabel("AC phase")
    ax3.set_xlabel("Vdc")
    ax4.set_xlabel("Vdc")
    ax1.plot(table[parameter], table.dc_current, '.-')
    ax2.plot(table[parameter], np.pi*table.dc_conductance, '.-')
    ax3.plot(table[parameter], table.ac_current_abs, '.-')
    ax4.plot(table[parameter], table.ac_current_phase, '.-')


def plot_vdc0(dm, **parameters):
    for name in ("omega", "vdc", "vac"):
        parameters.pop(name, None)
    results = dm.list(vdc=0, **parameters)
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=True)
    ax1.set_ylabel("Vac")
    ax3.set_ylabel("Vac")
    ax3.set_xlabel("Ω")
    ax4.set_xlabel("Ω")
    # DC conductance
    ax2.set_title('DC conductance')
    plot = ax2.scatter(
            results.omega,
            results.vac,
            c=np.pi*results.dc_conductance,
            marker='x',
            cmap=plt.cm.viridis)
    fig.colorbar(plot, ax=ax2)
    # AC current (abs)
    ax3.set_title('AC current')
    plot = ax3.scatter(
            results.omega,
            results.vac,
            c=results.ac_current_abs,
            marker='x',
            cmap=plt.cm.viridis)
    fig.colorbar(plot, ax=ax3)
    # AC current (phase)
    ax4.set_title('AC phase')
    phase_norm = plt.Normalize(-np.pi, np.pi)
    plot = ax4.scatter(
            results.omega,
            results.vac,
            c=results.ac_current_phase,
            marker='+',
            norm=phase_norm,
            cmap=plt.cm.hsv)
    fig.colorbar(plot, ax=ax4)

def export_array(dm, filename="../rtrg/figdata/omega25_order2.npz", omega=25, truncation_order=2, d=1e9, solver_tol_rel=1e-8, solver_tol_abs=1e-10, method="mu", xL=0.5, **parameters):
    data = dm.list(omega=omega, truncation_order=truncation_order, d=d, solver_tol_rel=solver_tol_rel, solver_tol_abs=solver_tol_abs, method=method, xL=xL, **parameters)
    data = data.sort_values(["vac","vdc"])
    np.savez(filename, vdc=data.vdc, vac=data.vac, omega=data.omega, gdc=data.dc_conductance, idc=data.dc_current, iac=data.ac_current_abs)

def plot_overview(dm, omega, **parameters):
    """
    Plot overview of dc and ac current and dc conductance for harmonic driving
    at fixed frequency as function of Vdc and Vac.
    """
    parameters.pop("method", None)
    results_J = dm.list(omega=omega, method='J', **parameters)
    results_mu = dm.list(omega=omega, method='mu', **parameters)

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=True)
    ax1.set_ylabel("Vac")
    ax3.set_ylabel("Vac")
    ax3.set_xlabel("Vdc")
    ax4.set_xlabel("Vdc")

    # DC current
    idc_min = min(
            results_J.size and results_J.dc_current.min(),
            results_mu.size and results_mu.dc_current.min())
    idc_max = max(
            results_J.size and results_J.dc_current.max(),
            results_mu.size and results_mu.dc_current.max())
    idc_norm = plt.Normalize(idc_min, idc_max)
    ax1.set_title('DC current')
    ax1.scatter(
            results_J.vdc,
            results_J.vac,
            c=results_J.dc_current,
            marker='x',
            norm=idc_norm,
            cmap=plt.cm.viridis)
    plot = ax1.scatter(
            results_mu.vdc,
            results_mu.vac,
            c=results_mu.dc_current,
            marker='+',
            norm=idc_norm,
            cmap=plt.cm.viridis)
    fig.colorbar(plot, ax=ax1)
    # DC conductance
    gmin = np.pi*min(
            results_J.dc_conductance.min() if results_J.size else 1,
            results_mu.dc_conductance.min() if results_mu.size else 1)
    gmax = np.pi*max(
            results_J.dc_conductance.max() if results_J.size else 0,
            results_mu.dc_conductance.max() if results_mu.size else 0)
    gnorm = plt.Normalize(gmin, gmax)
    ax2.set_title('DC conductance')
    ax2.scatter(
            results_J.vdc,
            results_J.vac,
            c=np.pi*results_J.dc_conductance,
            marker='x',
            norm=gnorm,
            cmap=plt.cm.viridis)
    plot = ax2.scatter(
            results_mu.vdc,
            results_mu.vac,
            c=np.pi*results_mu.dc_conductance,
            marker='+',
            norm=gnorm,
            cmap=plt.cm.viridis)
    fig.colorbar(plot, ax=ax2)
    # AC current (abs)
    ax3.set_title('AC current')
    iac_min = min(
            results_J.size and results_J.ac_current_abs.min(),
            results_mu.size and results_mu.ac_current_abs.min())
    iac_max = max(
            results_J.size and results_J.ac_current_abs.max(),
            results_mu.size and results_mu.ac_current_abs.max())
    iac_norm = plt.Normalize(iac_min, iac_max)
    ax3.scatter(
            results_J.vdc,
            results_J.vac,
            c=results_J.ac_current_abs,
            marker='x',
            norm=iac_norm,
            cmap=plt.cm.viridis)
    plot = ax3.scatter(
            results_mu.vdc,
            results_mu.vac,
            c=results_mu.ac_current_abs,
            marker='+',
            norm=iac_norm,
            cmap=plt.cm.viridis)
    fig.colorbar(plot, ax=ax3)
    # AC current (phase)
    ax4.set_title('AC phase')
    phase_norm = plt.Normalize(-np.pi, np.pi)
    ax4.scatter(
            results_J.vdc,
            results_J.vac,
            c=results_J.ac_current_phase,
            marker='x',
            norm=phase_norm,
            cmap=plt.cm.hsv)
    plot = ax4.scatter(
            results_mu.vdc,
            results_mu.vac,
            c=results_mu.ac_current_phase,
            marker='+',
            norm=phase_norm,
            cmap=plt.cm.hsv)
    fig.colorbar(plot, ax=ax4)

def plot_interpolate(
        dm,
        omega,
        dc_res=100,
        ac_res=100,
        vdc_min=0,
        vac_min=0,
        vdc_max=100,
        vac_max=50,
        **parameters):
    """
    Plot overview of dc and ac current and dc conductance for harmonic driving
    at fixed frequency as function of Vdc and Vac.
    """
    results_all = dm.list(omega=omega, **parameters)
    results = results_all.loc[(results_all["solver_flags"] & DataManager.SOLVER_FLAGS["simplified_initial_conditions"]) == 0]
    j = results.method == "J"
    mu = results.method == "mu"
    vac_max = min(vac_max, results.vac.max())
    vdc_max = min(vdc_max, results.vdc.max())

    vac_arr = np.linspace(vac_max/(2*ac_res), vac_max*(1 - 0.5/ac_res), ac_res)
    vdc_arr = np.linspace(vdc_max/(2*dc_res), vdc_max*(1 - 0.5/dc_res), dc_res)
    extent = (0, vdc_max, 0, vac_max)

    # Interpolate
    show_J = j.sum() > 10
    show_mu = mu.sum() > 10
    all_figs = []
    all_axes = []
    if show_J:
        gdc_J_tck = bisplrep(
                results.vac[j],
                results.vdc[j],
                results.dc_conductance[j],
                s=1e-5,
                kx=3,
                ky=3)
        idc_J_tck = bisplrep(
                results.vac[j],
                results.vdc[j],
                results.dc_current[j],
                s=1e-5,
                kx=3,
                ky=3)
        iac_J_tck = bisplrep(
                results.vac[j],
                results.vdc[j],
                results.ac_current_abs[j],
                s=1e-5,
                kx=3,
                ky=3)
        phase_J_tck = bisplrep(
                results.vac[j],
                results.vdc[j],
                results.ac_current_phase[j],
                s=1e-5,
                kx=3,
                ky=3)
        gdc_g_J_interp = bisplev(vac_arr, vdc_arr, gdc_J_tck)
        gdc_i_J_interp = bisplev(vac_arr, vdc_arr, idc_J_tck, dy=1)
        idc_J_interp   = bisplev(vac_arr, vdc_arr, idc_J_tck)
        iac_J_interp   = bisplev(vac_arr, vdc_arr, iac_J_tck)
        phase_J_interp = bisplev(vac_arr, vdc_arr, phase_J_tck)
        # create figure
        fig_j,  ((ax1_j, ax2_j), (ax3_j, ax4_j)) = plt.subplots(2, 2, sharex=True, sharey=True)
        ax1_j.set_ylabel("Vac")
        ax3_j.set_ylabel("Vac")
        ax3_j.set_xlabel("Vdc")
        ax4_j.set_xlabel("Vdc")
        all_figs.append(fig_j)
        all_axes.append((ax1_j, ax2_j, ax3_j, ax4_j))
    if show_mu:
        gdc_mu_tck = bisplrep(
                results.vac[mu],
                results.vdc[mu],
                results.dc_conductance[mu],
                s=5e-6,
                kx=3,
                ky=3)
        idc_mu_tck = bisplrep(
                results.vac[mu],
                results.vdc[mu],
                results.dc_current[mu],
                s=5e-6,
                kx=3,
                ky=3)
        iac_mu_tck = bisplrep(
                results.vac[mu],
                results.vdc[mu],
                results.ac_current_abs[mu],
                s=5e-6,
                kx=3,
                ky=3)
        phase_mu_tck = bisplrep(
                results.vac[mu],
                results.vdc[mu],
                results.ac_current_phase[mu],
                s=5e-6,
                kx=3,
                ky=3)
        gdc_g_mu_interp = bisplev(vac_arr, vdc_arr, gdc_mu_tck)
        gdc_i_mu_interp = bisplev(vac_arr, vdc_arr, idc_mu_tck, dy=1)
        idc_mu_interp   = bisplev(vac_arr, vdc_arr, idc_mu_tck)
        iac_mu_interp   = bisplev(vac_arr, vdc_arr, iac_mu_tck)
        phase_mu_interp = bisplev(vac_arr, vdc_arr, phase_mu_tck)
        # create figure
        fig_mu, ((ax1_mu, ax2_mu), (ax3_mu, ax4_mu)) = plt.subplots(2, 2, sharex=True, sharey=True)
        ax1_mu.set_ylabel("Vac")
        ax3_mu.set_ylabel("Vac")
        ax3_mu.set_xlabel("Vdc")
        ax4_mu.set_xlabel("Vdc")
        all_figs.append(fig_mu)
        all_axes.append((ax1_mu, ax2_mu, ax3_mu, ax4_mu))

    # DC current
    idc_norm = plt.Normalize(results.dc_current.min(), results.dc_current.max())
    for (fig, axes) in zip(all_figs, all_axes):
        ax = axes[0]
        ax.set_title('DC current')
        ax.scatter(results.vdc[j], results.vac[j], c=results.dc_current[j], marker='x', norm=idc_norm, cmap=plt.cm.viridis)
        plot = ax.scatter(results.vdc[mu], results.vac[mu], c=results.dc_current[mu], marker='+', norm=idc_norm, cmap=plt.cm.viridis)
        fig.colorbar(plot, ax=ax)
    if show_mu:
        ax1_mu.imshow(idc_mu_interp, extent=extent, aspect='auto', cmap=plt.cm.viridis, norm=idc_norm, origin='lower')
    if show_J:
        ax1_j.imshow(idc_J_interp, extent=extent, aspect='auto', cmap=plt.cm.viridis, norm=idc_norm, origin='lower')

    # DC conductance
    gnorm = plt.Normalize(np.pi*results.dc_conductance.min(), np.pi*results.dc_conductance.max())
    for (fig, axes) in zip(all_figs, all_axes):
        ax = axes[1]
        ax.set_title('DC conductance')
        ax.scatter(results.vdc[j], results.vac[j], c=np.pi*results.dc_conductance[j], marker='x', norm=gnorm, cmap=plt.cm.viridis)
        plot = ax.scatter(results.vdc[mu], results.vac[mu], c=np.pi*results.dc_conductance[mu], marker='+', norm=gnorm, cmap=plt.cm.viridis)
        fig.colorbar(plot, ax=ax)
    if show_mu:
        ax2_mu.imshow(np.pi*gdc_g_mu_interp, extent=extent, aspect='auto', cmap=plt.cm.viridis, norm=gnorm, origin='lower')
    if show_J:
        ax2_j.imshow(np.pi*gdc_g_J_interp, extent=extent, aspect='auto', cmap=plt.cm.viridis, norm=gnorm, origin='lower')

    # AC current (abs)
    iac_norm = plt.Normalize(results.ac_current_abs.min(), results.ac_current_abs.max())
    for (fig, axes) in zip(all_figs, all_axes):
        ax = axes[2]
        ax.set_title('AC current')
        ax.scatter(results.vdc[j], results.vac[j], c=results.ac_current_abs[j], marker='x', norm=iac_norm, cmap=plt.cm.viridis)
        plot = ax.scatter(results.vdc[mu], results.vac[mu], c=results.ac_current_abs[mu], marker='+', norm=iac_norm, cmap=plt.cm.viridis)
        fig.colorbar(plot, ax=ax)
    if show_mu:
        ax3_mu.imshow(iac_mu_interp, extent=extent, aspect='auto', cmap=plt.cm.viridis, norm=iac_norm, origin='lower')
    if show_J:
        ax3_j.imshow(iac_J_interp, extent=extent, aspect='auto', cmap=plt.cm.viridis, norm=iac_norm, origin='lower')

    # AC current (phase)
    phase_norm = plt.Normalize(-np.pi, np.pi)
    for (fig, axes) in zip(all_figs, all_axes):
        ax = axes[3]
        ax.set_title('AC phase')
        ax.scatter(results.vdc[j], results.vac[j], c=results.ac_current_phase[j], marker='x', norm=phase_norm, cmap=plt.cm.hsv)
        plot = ax.scatter(results.vdc[mu], results.vac[mu], c=results.ac_current_phase[mu], marker='+', norm=phase_norm, cmap=plt.cm.hsv)
        fig.colorbar(plot, ax=ax)
    if show_mu:
        ax4_mu.imshow(phase_mu_interp, extent=extent, aspect='auto', cmap=plt.cm.hsv, norm=phase_norm, origin='lower')
    if show_J:
        ax4_j.imshow(phase_J_interp, extent=extent, aspect='auto', cmap=plt.cm.hsv, norm=phase_norm, origin='lower')

    # Create figure
    fig, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(2, 3, sharex=True, sharey=True)
    ax1.set_ylabel("Vac")
    ax4.set_ylabel("Vac")
    ax4.set_xlabel("Vdc")
    ax5.set_xlabel("Vdc")
    ax6.set_xlabel("Vdc")
    ax1.set_title("DC current: mu vs. J")
    ax2.set_title("DC conductance: mu vs. J for G")
    ax3.set_title("DC conductance: G vs. I for mu")
    ax6.set_title("DC conductance: G vs. I for J")
    ax4.set_title("AC current: mu vs. J")
    ax5.set_title("AC phase: mu vs. J")
    check_norm = plt.Normalize(-0.03, 0.03)
    if show_J and show_mu:
        ax1.imshow(
                (idc_mu_interp - idc_J_interp)/idc_mu_interp,
                extent=extent,
                aspect='auto',
                cmap=plt.cm.seismic,
                norm=check_norm,
                origin='lower')
        ax2.imshow(
                (gdc_g_mu_interp - gdc_g_J_interp)/gdc_g_J_interp,
                extent=extent,
                aspect='auto',
                cmap=plt.cm.seismic,
                norm=check_norm,
                origin='lower')
        ax4.imshow(
                (iac_mu_interp - iac_J_interp)/iac_mu_interp,
                extent=extent,
                aspect='auto',
                cmap=plt.cm.seismic,
                norm=check_norm,
                origin='lower')
        ax5.imshow(
                phase_mu_interp - phase_J_interp,
                extent=extent,
                aspect='auto',
                cmap=plt.cm.seismic,
                norm=check_norm,
                origin='lower')
    if show_mu:
        img = ax3.imshow(
                (gdc_g_mu_interp - gdc_i_mu_interp)/gdc_g_mu_interp,
                extent=extent,
                aspect='auto',
                cmap=plt.cm.seismic,
                norm=check_norm,
                origin='lower')
    if show_J:
        img = ax6.imshow(
                (gdc_g_J_interp - gdc_i_J_interp)/gdc_g_J_interp,
                extent=extent,
                aspect='auto',
                cmap=plt.cm.seismic,
                norm=check_norm,
                origin='lower')
    fig.colorbar(img, ax=[ax1,ax2,ax3,ax4,ax5,ax6])

def plot_comparison(dm, omega, d=1e9, **parameters):
    """
    Compare current and dc conductance computed from both methods (J and mu) at
    fixed frequency as function of Vdc and Vac.
    Only data points which exist for both methods with equal Vdc, Vac, D and
    frequency are considered.
    """
    #for name in ("omega", "method"):
    #    parameters.pop(name, None)
    #results_J = dm.list(omega=omega, method='J', d=d, **parameters).sort_values(["vdc","vac"])
    #results_mu = dm.list(omega=omega, method='mu', d=d, **parameters).sort_values(["vdc","vac"])
    ## For comparison with resonant dc voltage:
    for name in ("omega", "method", "voltage_branches"):
        parameters.pop(name, None)
    parameters["include_Ga"] = False
    parameters["integral_method"] = -15
    parameters["truncation_order"] = 3
    parameters["solver_tol_rel"] = 1e-8
    parameters["solver_tol_abs"] = 1e-10
    results_J = dm.list(omega=omega, method='mu', d=d, voltage_branches=4, **parameters).sort_values(["vdc","vac"])
    results_mu = dm.list(omega=omega, method='J', d=d, voltage_branches=0, **parameters).sort_values(["vdc","vac"])
    ## For comparison of different numbers of voltage branches:
    #for name in ("omega", "method", "voltage_branches"):
    #    parameters.pop(name, None)
    #parameters["include_Ga"] = True
    #parameters["integral_method"] = -15
    #parameters["truncation_order"] = 3
    #parameters["solver_tol_rel"] = 1e-8
    #parameters["solver_tol_abs"] = 1e-10
    #results_J = dm.list(omega=omega, method='mu', d=d, voltage_branches=4, **parameters).sort_values(["vdc","vac"])
    #results_mu = dm.list(omega=omega, method='mu', d=d, voltage_branches=7, **parameters).sort_values(["vdc","vac"])
    results_J.vac = results_J.vac.round(6)
    results_J.vdc = results_J.vdc.round(6)
    results_mu.vac = results_mu.vac.round(6)
    results_mu.vdc = results_mu.vdc.round(6)
    merged = pd.merge(
            results_J,
            results_mu,
            how="inner",
            on=["vdc","vac","d","omega"],
            suffixes=("_J", "_mu"))

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=True)
    ax1.set_ylabel("Vac")
    ax3.set_ylabel("Vac")
    ax3.set_xlabel("Vdc")
    ax4.set_xlabel("Vdc")

    # DC current
    idc_diff = merged.dc_current_J - merged.dc_current_mu
    idc_max = abs(idc_diff).max()
    idc_norm = plt.Normalize(-idc_max, idc_max)
    ax1.set_title('DC current')
    plot = ax1.scatter(merged.vdc, merged.vac, c=idc_diff, marker='o', norm=idc_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax1)
    # DC conductance
    gdc_diff = np.pi*(merged.dc_conductance_J - merged.dc_conductance_mu)
    gdc_max = abs(gdc_diff).max()
    gdc_norm = plt.Normalize(-gdc_max, gdc_max)
    ax2.set_title('DC conductance')
    plot = ax2.scatter(merged.vdc, merged.vac, c=gdc_diff, marker='o', norm=gdc_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax2)
    # AC current (abs)
    iac_diff = merged.ac_current_abs_J - merged.ac_current_abs_mu
    iac_max = abs(iac_diff).max()
    iac_norm = plt.Normalize(-iac_max, iac_max)
    ax3.set_title('AC current')
    plot = ax3.scatter(merged.vdc, merged.vac, c=iac_diff, marker='o', norm=iac_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax3)
    # AC current (phase)
    phase_diff = (merged.ac_current_phase_J - merged.ac_current_phase_mu + np.pi) % (2*np.pi) - np.pi
    phase_max = abs(phase_diff).max()
    phase_norm = plt.Normalize(-phase_max, phase_max)
    ax4.set_title('AC phase')
    plot = ax4.scatter(merged.vdc, merged.vac, c=phase_diff, marker='o', norm=phase_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax4)

def plot_comparison_relative(dm, omega, d=1e9, **parameters):
    """
    Compare current and dc conductance computed from both methods (J and mu) at
    fixed frequency as function of Vdc and Vac.
    Only data points which exist for both methods with equal Vdc, Vac, D and
    frequency are considered.
    """
    #for name in ("omega", "method"):
    #    parameters.pop(name, None)
    #results_J = dm.list(omega=omega, method='J', d=d, **parameters).sort_values(["vdc","vac"])
    #results_mu = dm.list(omega=omega, method='mu', d=d, **parameters).sort_values(["vdc","vac"])
    ## For comparison with resonant dc voltage:
    for name in ("omega", "method", "voltage_branches"):
        parameters.pop(name, None)
    parameters["include_Ga"] = False
    parameters["integral_method"] = -15
    parameters["truncation_order"] = 3
    parameters["solver_tol_rel"] = 1e-8
    parameters["solver_tol_abs"] = 1e-10
    results_J = dm.list(omega=omega, method='mu', d=d, voltage_branches=4, **parameters).sort_values(["vdc","vac"])
    results_mu = dm.list(omega=omega, method='J', d=d, voltage_branches=0, **parameters).sort_values(["vdc","vac"])

    results_J.vac = results_J.vac.round(6)
    results_J.vdc = results_J.vdc.round(6)
    results_mu.vac = results_mu.vac.round(6)
    results_mu.vdc = results_mu.vdc.round(6)
    merged = pd.merge(
            results_J,
            results_mu,
            how="inner",
            on=["vdc","vac","d","omega"],
            suffixes=("_J", "_mu"))

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=True)
    ax1.set_ylabel("Vac")
    ax3.set_ylabel("Vac")
    ax3.set_xlabel("Vdc")
    ax4.set_xlabel("Vdc")

    # DC current
    idc_diff = 2*(merged.dc_current_J - merged.dc_current_mu)/(merged.dc_current_J + merged.dc_current_mu)
    idc_diff[merged.vdc == 0] = 0
    idc_max = abs(idc_diff).max()
    idc_norm = plt.Normalize(-idc_max, idc_max)
    ax1.set_title('DC current')
    plot = ax1.scatter(merged.vdc, merged.vac, s=5+5e3*np.abs(idc_diff), c=idc_diff, marker='o', norm=idc_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax1)
    # DC conductance
    gdc_diff = 2*(merged.dc_conductance_J - merged.dc_conductance_mu)/(merged.dc_conductance_J + merged.dc_conductance_mu)
    gdc_max = abs(gdc_diff).max()
    gdc_norm = plt.Normalize(-gdc_max, gdc_max)
    ax2.set_title('DC conductance')
    plot = ax2.scatter(merged.vdc, merged.vac, s=5+5e3*np.abs(gdc_diff), c=gdc_diff, marker='o', norm=gdc_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax2)
    # AC current (abs)
    iac_diff = 2*(merged.ac_current_abs_J - merged.ac_current_abs_mu)/(merged.ac_current_abs_J + merged.ac_current_abs_mu)
    iac_max = abs(iac_diff).max()
    iac_norm = plt.Normalize(-iac_max, iac_max)
    ax3.set_title('AC current')
    plot = ax3.scatter(merged.vdc, merged.vac, s=5+1e4*np.abs(iac_diff), c=iac_diff, marker='o', norm=iac_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax3)
    # AC current (phase)
    phase_diff = (merged.ac_current_phase_J - merged.ac_current_phase_mu + np.pi) % (2*np.pi) - np.pi
    phase_max = abs(phase_diff).max()
    phase_norm = plt.Normalize(-phase_max, phase_max)
    ax4.set_title('AC phase')
    plot = ax4.scatter(merged.vdc, merged.vac, s=5+2e4*np.abs(phase_diff), c=phase_diff, marker='o', norm=phase_norm, cmap=plt.cm.seismic)
    fig.colorbar(plot, ax=ax4)

def plot_floquet_matrices(kondo : KondoImport, norm_min=1e-6):
    fig, axes = plt.subplots(3, 3)
    axes = axes.flatten()
    gamma = kondo.gamma
    idx = kondo.voltage_branches if gamma.ndim == 3 else ...
    try:
        axes[0].set_title("Γ")
        img = axes[0].imshow(np.abs(gamma[idx]), norm=LogNorm(norm_min))
        fig.colorbar(img, ax=axes[0])
    except AttributeError:
        pass
    try:
        axes[1].set_title("Z")
        img = axes[1].imshow(np.abs(kondo.z[idx]), norm=LogNorm(norm_min))
        fig.colorbar(img, ax=axes[1])
    except AttributeError:
        pass
    try:
        axes[2].set_title("ΓL")
        img = axes[2].imshow(np.abs(kondo.gammaL), norm=LogNorm(norm_min))
        fig.colorbar(img, ax=axes[2])
    except AttributeError:
        pass
    try:
        axes[3].set_title("δΓL")
        img = axes[3].imshow(np.abs(kondo.deltaGammaL), norm=LogNorm(norm_min))
        fig.colorbar(img, ax=axes[3])
    except AttributeError:
        pass
    try:
        axes[4].set_title("δΓ")
        img = axes[4].imshow(np.abs(kondo.deltaGamma[1 if gamma.ndim == 3 else ...]), norm=LogNorm(norm_min))
        fig.colorbar(img, ax=axes[4])
    except AttributeError:
        pass
    try:
        axes[5].set_title("yL")
        img = axes[5].imshow(np.abs(kondo.yL), norm=LogNorm(norm_min))
        fig.colorbar(img, ax=axes[5])
    except AttributeError:
        pass
    try:
        axes[6].set_title("G2")
        img = axes[6].imshow(np.abs(kondo.g2[:,:,idx].transpose(0,2,1,3).reshape((4*kondo.nmax+2, 4*kondo.nmax+2))), norm=LogNorm(norm_min))
        fig.colorbar(img, ax=axes[6])
    except AttributeError:
        pass
    try:
        axes[7].set_title("G3")
        img = axes[7].imshow(np.abs(kondo.g3[:,:,idx].transpose(0,2,1,3).reshape((4*kondo.nmax+2, 4*kondo.nmax+2))), norm=LogNorm(norm_min))
        fig.colorbar(img, ax=axes[7])
    except AttributeError:
        pass
    try:
        axes[8].set_title("I")
        img = axes[8].imshow(np.abs(kondo.current.transpose(0,2,1,3).reshape((4*kondo.nmax+2, 4*kondo.nmax+2))), norm=LogNorm(norm_min))
        fig.colorbar(img, ax=axes[8])
    except AttributeError:
        pass

def fixed_omega_dconvergence(dm, omega=16.5372, solver_tol_rel=1e-10, solver_tol_abs=1e-12, method="mu", **parameters):
    parameters.pop("d", None)
    data = dm.list(
            omega=omega,
            solver_tol_rel=solver_tol_rel,
            solver_tol_abs=solver_tol_abs,
            method=method,
            **parameters).sort_values(["vdc", "vac", "d"])
    #fig, (ax_g, ax_idc, ax_iac) = plt.subplots(nrows=3, sharex=True)
    #ax_g.set_xscale("log")
    #data_newidx = data.set_index(["vdc","vac"])
    #data_newidx.sort_index(inplace=True)
    #cmap = plt.cm.viridis_r
    #for index in data_newidx.index.unique():
    #    d = np.pi*data_newidx.d[index]
    #    g = np.pi*data_newidx.dc_conductance[index]
    #    idc = np.pi*data_newidx.dc_current[index]
    #    iac = np.pi*data_newidx.ac_current_abs[index]
    #    ax_g.plot(d, g, color=cmap(1e4*g.std()/g.mean()))
    #    ax_idc.plot(d, idc, color=cmap(5e2*idc.std()/(idc.mean() or np.nan)))
    #    ax_iac.plot(d, iac, color=cmap(5e2*iac.std()/(iac.mean() or np.nan)))

    group = data.groupby(by=["vdc","vac"])
    fit_func = lambda logd_inv, a, b, c: a + b * logd_inv**c
    def fit(df):
        g = df.dc_conductance.mean()
        g_err = df.dc_conductance.std()
        try:
            (idc, idc_pref, idc_exp), covar = curve_fit(fit_func, 1/np.log(df.d), df.dc_current, (df.dc_current.mean(), 0., 3.))
            idc_err, idc_pref_err, idc_exp_err = covar.diagonal()**0.5
        except (RuntimeError, TypeError, ValueError):
            idc = idc_pref = idc_exp = idc_err = idc_pref_err = idc_exp_err = np.nan
        try:
            (iac, iac_pref, iac_exp), covar = curve_fit(fit_func, 1/np.log(df.d), df.ac_current_abs, (df.ac_current_abs.mean(), 0., 3.))
            iac_err, iac_pref_err, iac_exp_err = covar.diagonal()**0.5
        except (RuntimeError, TypeError, ValueError):
            iac = iac_pref = iac_exp = iac_err = iac_pref_err = iac_exp_err = np.nan
        return pd.Series(dict(g=g, g_err=g_err, idc=idc, idc_pref=idc_pref, idc_exp=idc_exp, idc_err=idc_err, idc_pref_err=idc_pref_err, idc_exp_err=idc_exp_err, iac=iac, iac_pref=iac_pref, iac_exp=iac_exp, iac_err=iac_err, iac_pref_err=iac_pref_err, iac_exp_err=iac_exp_err))
    data_fitted = group.apply(fit)
    vac_vdc_frame = data_fitted.index.to_frame()
    fig, (ax_g, ax_idc, ax_iac) = plt.subplots(ncols=3)
    ax_g.errorbar(vac_vdc_frame["vac"], np.pi*data_fitted.g, fmt="o", yerr=10*np.pi*data_fitted.g_err, capsize=2, zorder=-1)
    ax_idc.errorbar(vac_vdc_frame["vac"], data_fitted.idc, fmt="o", yerr=10*data_fitted.idc_err, capsize=2, zorder=-1)
    ax_iac.errorbar(vac_vdc_frame["vdc"], 2*data_fitted.iac, fmt="o", yerr=20*data_fitted.iac_err, capsize=2, zorder=-1)
    ax_g.scatter(data.vac, np.pi*data.dc_conductance, c=np.log(data.d), marker="x")
    ax_idc.scatter(data.vac, data.dc_current, c=np.log(data.d), marker="x")
    ax_iac.scatter(data.vdc, 2*data.ac_current_abs, c=np.log(data.d), marker="x")
    #ax_g.scatter(vac_vdc_frame["vdc"], vac_vdc_frame["vac"], c=np.pi*data_fitted.g, cmap=plt.cm.viridis)
    #ax_idc.scatter(vac_vdc_frame["vdc"], vac_vdc_frame["vac"], c=data_fitted.idc, cmap=plt.cm.viridis)
    #ax_iac.scatter(vac_vdc_frame["vdc"], vac_vdc_frame["vac"], c=data_fitted.iac, cmap=plt.cm.viridis)

    fig, (ax_g, ax_idc, ax_iac) = plt.subplots(ncols=3, sharex=True, sharey=True)
    ax_g.errorbar(
            vac_vdc_frame["vdc"],
            vac_vdc_frame["vac"],
            fmt="o",
            yerr=1e4*data_fitted.g_err/data_fitted.g,
            capsize=2,
            zorder=-1)
    ax_idc.errorbar(
            vac_vdc_frame["vdc"],
            vac_vdc_frame["vac"],
            fmt="o",
            yerr=1e4*data_fitted.idc_err/data_fitted.idc,
            capsize=2,
            zorder=-1)
    ax_iac.errorbar(
            vac_vdc_frame["vdc"],
            vac_vdc_frame["vac"],
            fmt="o",
            yerr=1e4*data_fitted.iac_err/data_fitted.iac,
            capsize=2,
            zorder=-1)
    vdc_grid = []
    vac_grid = []
    g_diff_grid = []
    idc_enhanced_grid = []
    iac_enhanced_grid = []
    idc_diff_grid = []
    iac_diff_grid = []
    d_grid = []
    for vdc_vac, idx in group.indices.items():
        subtable = data.iloc[idx]
        vac_grid.append(subtable.vac)
        vdc_grid.append(subtable.vdc)
        d_grid.append(subtable.d)
        g_diff_grid.append((subtable.dc_conductance - data_fitted.g[vdc_vac])/(data_fitted.g[vdc_vac] or np.nan))
        idc_diff_grid.append((subtable.dc_current - data_fitted.idc[vdc_vac])/(data_fitted.idc[vdc_vac] or np.nan))
        iac_diff_grid.append((subtable.ac_current_abs - data_fitted.iac[vdc_vac])/(data_fitted.iac[vdc_vac] or np.nan))
        idc_enhanced_grid.append(7*subtable.dc_current - 6*data_fitted.idc[vdc_vac])
        iac_enhanced_grid.append(7*subtable.ac_current_abs - 6*data_fitted.iac[vdc_vac])
    vdc_grid = np.concatenate(vdc_grid)
    vac_grid = np.concatenate(vac_grid)
    d_grid = np.concatenate(d_grid)
    g_diff_grid = np.pi*np.concatenate(g_diff_grid)
    idc_diff_grid = np.concatenate(idc_diff_grid)
    iac_diff_grid = np.concatenate(iac_diff_grid)
    idc_enhanced_grid = np.concatenate(idc_enhanced_grid)
    iac_enhanced_grid = np.concatenate(iac_enhanced_grid)
    ax_g.scatter(vdc_grid, vac_grid + 1e4*g_diff_grid, c=np.log(d_grid), marker="x")
    ax_idc.scatter(vdc_grid, vac_grid + 1e3*idc_diff_grid, c=np.log(d_grid), marker="x")
    ax_iac.scatter(vdc_grid, vac_grid + 1e3*iac_diff_grid, c=np.log(d_grid), marker="x")

    fig, (ax_idc, ax_iac) = plt.subplots(ncols=2)
    ax_idc.set_ylabel("Idc, deviation ×7")
    ax_idc.set_xlabel("Vac")
    ax_iac.set_ylabel("Iac, deviation ×7")
    ax_iac.set_xlabel("Vdc")
    ax_idc.scatter(
            vac_grid,
            idc_enhanced_grid,
            c=d_grid,
            norm=LogNorm(),
            cmap=plt.cm.viridis_r,
            marker="x")
    plot = ax_iac.scatter(
            vdc_grid,
            2*iac_enhanced_grid,
            c=d_grid,
            norm=LogNorm(),
            cmap=plt.cm.viridis_r,
            marker="x")
    cb = fig.colorbar(plot, ax=[ax_idc, ax_iac])
    cb.set_label("D")
    ax_idc.errorbar(
            vac_vdc_frame["vac"],
            data_fitted.idc,
            fmt="o",
            yerr=70*data_fitted.idc_err,
            capsize=2,
            zorder=1)
    ax_iac.errorbar(
            vac_vdc_frame["vdc"],
            2*data_fitted.iac,
            fmt="o",
            yerr=140*data_fitted.iac_err,
            capsize=2,
            zorder=1)

    plt.show()

def check_results(dm, max_num=5, **parameters):
    table = dm.list(**parameters)
    counter = 0
    settings.logger.info(f"Found {table.shape[0]} candidates")
    settings.logger.debug(parameters)
    for index, row in table.iterrows():
        for kondo in KondoImport.read_from_h5(os.path.join(row.dirname, row.basename), row.hash):
            try:
                plot_floquet_matrices(kondo)
            except KeyboardInterrupt:
                kondo._h5file.close()
                return
            counter += 1
            if counter >= max_num:
                settings.logger.warning("Maximum number of files read, stopping here")
                return
        if not kondo._owns_h5file:
            kondo._h5file.close()

def check_convergence(dm, **parameters):
    if not ("omega" in parameters and "vdc" in parameters and "vac" in parameters):
        settings.logger.warning("check_convergence expects specification of all physical parameters")
    table = dm.list(**parameters)
    mod = (table.solver_flags & DataManager.SOLVER_FLAGS["simplified_initial_conditions"]) != 0
    j = (~mod) & (table.method == "J")
    mu = (~mod) & (table.method == "mu")
    d_j = table.d[j]
    d_mu = table.d[mu]
    d_mod = table.d[mod]
    x = 1/np.log(table.d)**3
    x_j = x[j]
    x_mu = x[mu]
    x_mod = x[mod]
    drtol = table.d*table.solver_tol_rel
    norm = LogNorm(max(drtol.min(), 0.05), min(drtol.max(), 500))
    c_j = drtol[j]
    c_mu = drtol[mu]
    c_mod = drtol[mod]
    s_j = 0.12 * table.nmax[j]**2
    s_mu = 0.15 * table.nmax[mu]**2
    s_mod = 0.15 * table.nmax[mod]**2
    lw_j = 0.3 * table.voltage_branches[j]
    lw_mu = 0.3 * table.voltage_branches[mu]
    lw_mod = 0.3 * table.voltage_branches[mod]

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=False)
    ax3.set_xlabel("D")
    ax4.set_xlabel("D")
    ax1.set_xscale("log")

    # Show reference values
    try:
        reference_values = REFERENCE_VALUES[(parameters['omega'], parameters['vdc'], parameters['vac'])]
    except KeyError:
        reference_values = None

    if reference_values is not None:
        x = (table.d.min(), table.d.max())
        ax1.plot(x, (reference_values['idc'],reference_values['idc']), 'k:')
        ax1.fill_between(
                x,
                (reference_values['idc']-reference_values['idc_err'], reference_values['idc']-reference_values['idc_err']),
                (reference_values['idc']+reference_values['idc_err'], reference_values['idc']+reference_values['idc_err']),
                color='#80808080')
        ax2.plot(x, (reference_values['gdc'],reference_values['gdc']), 'k:')
        ax2.fill_between(
                x,
                (reference_values['gdc']-reference_values['gdc_err'], reference_values['gdc']-reference_values['gdc_err']),
                (reference_values['gdc']+reference_values['gdc_err'], reference_values['gdc']+reference_values['gdc_err']),
                color='#80808080')
        ax3.plot(x, (reference_values['iac'],reference_values['iac']), 'k:')
        ax3.fill_between(
                x,
                (reference_values['iac']-reference_values['iac_err'], reference_values['iac']-reference_values['iac_err']),
                (reference_values['iac']+reference_values['iac_err'], reference_values['iac']+reference_values['iac_err']),
                color='#80808080')
        ax4.plot(x, (reference_values['acphase'],reference_values['acphase']), 'k:')
        ax4.fill_between(
                x,
                (reference_values['acphase']-reference_values['acphase_err'], reference_values['acphase']-reference_values['acphase_err']),
                (reference_values['acphase']+reference_values['acphase_err'], reference_values['acphase']+reference_values['acphase_err']),
                color='#80808080')

    ax1.set_title("DC current")
    ax1.scatter(d_j, table.dc_current[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax1.scatter(d_mu, table.dc_current[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax1.scatter(d_mod, table.dc_current[mod], marker='*', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)

    ax2.set_title("DC conductance")
    ax2.scatter(d_j, table.dc_conductance[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax2.scatter(d_mu, table.dc_conductance[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax2.scatter(d_mod, table.dc_conductance[mod], marker='*', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)

    ax3.set_title("AC current")
    ax3.scatter(d_j, table.ac_current_abs[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax3.scatter(d_mu, table.ac_current_abs[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax3.scatter(d_mod, table.ac_current_abs[mod], marker='*', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)

    ax4.set_title("AC phase")
    ax4.scatter(d_j, table.ac_current_phase[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax4.scatter(d_mu, table.ac_current_phase[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax4.scatter(d_mod, table.ac_current_phase[mod], marker='*', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)

def check_convergence_nmax(dm, **parameters):
    if not ("omega" in parameters and "vdc" in parameters and "vac" in parameters and "d"):
        settings.logger.warning("check_convergence_nmax expects specification of all physical parameters and D")
    table = dm.list(**parameters)
    mod = (table.solver_flags & DataManager.SOLVER_FLAGS["simplified_initial_conditions"]) != 0
    j = (~mod) & (table.method == "J")
    mu = (~mod) & (table.method == "mu")
    norm = LogNorm(table.voltage_branches.min(), table.voltage_branches.max())
    nmax_j = table.nmax[j]
    nmax_mu = table.nmax[mu]
    nmax_mod = table.nmax[mod]
    s_j = -3*np.log(table.solver_tol_rel[j])
    s_mu = -3*np.log(table.solver_tol_rel[mu])
    s_mod = -3*np.log(table.solver_tol_rel[mod])
    lw_j = -0.1*np.log(table.solver_tol_abs[j])
    lw_mu = -0.1*np.log(table.solver_tol_abs[mu])
    lw_mod = -0.1*np.log(table.solver_tol_abs[mod])
    c_j = table.voltage_branches[j]
    c_mu = table.voltage_branches[mu]
    c_mod = table.voltage_branches[mod]

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=False)
    ax3.set_xlabel("nmax")
    ax4.set_xlabel("nmax")

    # Show reference values
    try:
        reference_values = REFERENCE_VALUES[(parameters['omega'], parameters['vdc'], parameters['vac'])]
    except KeyError:
        reference_values = None

    if reference_values is not None:
        x = (table.nmax.min(), table.nmax.max())
        ax1.plot(x, (reference_values['idc'],reference_values['idc']), 'k:')
        ax1.fill_between(
                x,
                (reference_values['idc']-reference_values['idc_err'], reference_values['idc']-reference_values['idc_err']),
                (reference_values['idc']+reference_values['idc_err'], reference_values['idc']+reference_values['idc_err']),
                color='#80808080')
        ax2.plot(x, (reference_values['gdc'],reference_values['gdc']), 'k:')
        ax2.fill_between(
                x,
                (reference_values['gdc']-reference_values['gdc_err'], reference_values['gdc']-reference_values['gdc_err']),
                (reference_values['gdc']+reference_values['gdc_err'], reference_values['gdc']+reference_values['gdc_err']),
                color='#80808080')
        ax3.plot(x, (reference_values['iac'],reference_values['iac']), 'k:')
        ax3.fill_between(
                x,
                (reference_values['iac']-reference_values['iac_err'], reference_values['iac']-reference_values['iac_err']),
                (reference_values['iac']+reference_values['iac_err'], reference_values['iac']+reference_values['iac_err']),
                color='#80808080')
        ax4.plot(x, (reference_values['acphase'],reference_values['acphase']), 'k:')
        ax4.fill_between(
                x,
                (reference_values['acphase']-reference_values['acphase_err'], reference_values['acphase']-reference_values['acphase_err']),
                (reference_values['acphase']+reference_values['acphase_err'], reference_values['acphase']+reference_values['acphase_err']),
                color='#80808080')

    ax1.set_title("DC current")
    ax1.scatter(nmax_j, table.dc_current[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax1.scatter(nmax_mu, table.dc_current[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax1.scatter(nmax_mod, table.dc_current[mod], marker='_', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)

    ax2.set_title("DC conductance")
    ax2.scatter(nmax_j, table.dc_conductance[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax2.scatter(nmax_mu, table.dc_conductance[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax2.scatter(nmax_mod, table.dc_conductance[mod], marker='_', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)

    ax3.set_title("AC current")
    ax3.scatter(nmax_j, table.ac_current_abs[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax3.scatter(nmax_mu, table.ac_current_abs[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax3.scatter(nmax_mod, table.ac_current_abs[mod], marker='_', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)

    ax4.set_title("AC phase")
    ax4.scatter(nmax_j, table.ac_current_phase[j], marker='x', s=s_j, c=c_j, linewidths=lw_j, norm=norm)
    ax4.scatter(nmax_mu, table.ac_current_phase[mu], marker='+', s=s_mu, c=c_mu, linewidths=lw_mu, norm=norm)
    ax4.scatter(nmax_mod, table.ac_current_phase[mod], marker='_', s=s_mod, c=c_mod, linewidths=lw_mod, norm=norm)


def check_convergence_fit(dm, **parameters):
    if not ("omega" in parameters and "vdc" in parameters and "vac" in parameters):
        settings.logger.warning(
                "check_convergence expects specification of all physical parameters")
    table = dm.list(**parameters)
    flags = DataManager.SOLVER_FLAGS["simplified_initial_conditions"] | DataManager.SOLVER_FLAGS["improved_initial_conditions"]
    mod_bad = table.solver_flags & flags == DataManager.SOLVER_FLAGS["simplified_initial_conditions"]
    mod_old = table.solver_flags & flags == 0
    mod_good = table.solver_flags & flags == DataManager.SOLVER_FLAGS["improved_initial_conditions"]
    d_fit_max = 9e11
    d_fit_max_j_nopadding = 9e11
    d_fit_max_j_shifted_nopadding = 2e9
    j = mod_old & (table.method == "J")
    mu_good = mod_good & (table.method == "mu")
    mu_old = mod_old & (table.method == "mu")
    mu_bad = mod_bad & (table.method == "mu")
    logd_inv_arr = np.linspace(0, 1/np.log10(table.d.min()), 200)
    logd_inv = 1/np.log10(table.d)
    x = 1/np.log10(table.d)
    x_j = x[j]
    x_good = x[mu_good]
    x_old = x[mu_old]
    x_bad = x[mu_bad]
    drtol = table.d*table.solver_tol_rel
    norm = LogNorm(max(drtol.min(), 0.05), min(drtol.max(), 500))
    c_j = drtol[j]
    c_good = drtol[mu_good]
    c_old = drtol[mu_old]
    c_bad = drtol[mu_bad]
    s_j = 0.05 * table.nmax[j]**2
    s_good = 0.08 * table.nmax[mu_good]**2
    s_old = 0.08 * table.nmax[mu_old]**2
    s_bad = 0.08 * table.nmax[mu_bad]**2
    lw_j = 0.3 * table.voltage_branches[j]
    lw_good = 0.3 * table.voltage_branches[mu_good]
    lw_old = 0.3 * table.voltage_branches[mu_old]
    lw_bad = 0.3 * table.voltage_branches[mu_bad]

    selections = {}
    for r in range(10):
        suffix = '_%d'%r if r else ''
        if r in table.resonant_dc_shift.values:
            mu_good_shift = mod_good \
                    & (table.method == "mu") \
                    & (table.d <= d_fit_max) \
                    & (table.resonant_dc_shift == r)
            mu_old_shift = mod_old \
                    & (table.method == "mu") \
                    & (table.d <= d_fit_max) \
                    & (table.resonant_dc_shift == r)
            mu_bad_shift = mod_bad \
                    & (table.method == "mu") \
                    & (table.d <= d_fit_max) \
                    & (table.resonant_dc_shift == r)
            j_shift = mod_old \
                    & (table.method == "J") \
                    & (     ((table.padding > 0) & (table.d <= d_fit_max)) \
                            | (table.d <= (d_fit_max_j_nopadding if r == 0 else d_fit_max_j_shifted_nopadding)) ) \
                    & (table.resonant_dc_shift == r)
            j_mod_shift = mod_bad \
                    & (table.method == "J") \
                    & (     ((table.padding > 0) & (table.d <= d_fit_max)) \
                            | (table.d <= (d_fit_max_j_nopadding if r == 0 else d_fit_max_j_shifted_nopadding)) ) \
                    & (table.resonant_dc_shift == r)
            if mu_good_shift.any():
                selections["mu_good" + suffix] = mu_good_shift
            if mu_old_shift.any():
                selections["mu_old" + suffix] = mu_old_shift
            if mu_bad_shift.any():
                selections["mu_bad" + suffix] = mu_bad_shift
            if j_shift.any():
                selections["J" + suffix] = j_shift
            if j_mod_shift.any():
                selections["J_mod" + suffix] = j_mod_shift

    fit_func = lambda logd_inv, a, b, c: a + b * logd_inv**c

    x_bad_mean = (x[mod_bad]**2).mean()
    x_bad_max = (x[mod_bad]**2).max()
    x_bad_shifted = x[mod_bad]**2 - x_bad_mean
    s_bad = (mod_bad).sum()
    s_bad_xx = (x_bad_shifted**2).sum()

    x_old_mean = (x[mod_old]**3).mean()
    x_old_max = (x[mod_old]**3).max()
    x_old_shifted = x[mod_old]**3 - x_old_mean
    s_old = (mod_old).sum()
    s_old_xx = (x_old_shifted**2).sum()

    x_good_mean = (x[mod_good]**4).mean()
    x_good_max = (x[mod_good]**4).max()
    x_good_shifted = x[mod_good]**4 - x_good_mean
    s_good = (mod_good).sum()
    s_good_xx = (x_good_shifted**2).sum()

    # Create figure
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, sharex=True, sharey=False)
    ax3.set_xlabel("1/log10(D)")
    ax4.set_xlabel("1/log10(D)")

    # Show reference values
    try:
        reference_values = REFERENCE_VALUES[(
            parameters['omega'],
            parameters['vdc'],
            parameters['vac'],
            )]
    except KeyError:
        reference_values = None

    if reference_values is not None:
        ax1.plot(
                (0, logd_inv_arr[-1]),
                (reference_values['idc'], reference_values['idc']),
                'k:')
        ax1.fill_between(
                (0, logd_inv_arr[-1]),
                (reference_values['idc']-reference_values['idc_err'], reference_values['idc']-reference_values['idc_err']),
                (reference_values['idc']+reference_values['idc_err'], reference_values['idc']+reference_values['idc_err']),
                color='#80808080')
        ax2.plot(
                (0, logd_inv_arr[-1]),
                (reference_values['gdc'], reference_values['gdc']),
                'k:')
        ax2.fill_between(
                (0, logd_inv_arr[-1]),
                (reference_values['gdc']-reference_values['gdc_err'], reference_values['gdc']-reference_values['gdc_err']),
                (reference_values['gdc']+reference_values['gdc_err'], reference_values['gdc']+reference_values['gdc_err']),
                color='#80808080')
        ax3.plot(
                (0, logd_inv_arr[-1]),
                (reference_values['iac'], reference_values['iac']),
                'k:')
        ax3.fill_between(
                (0, logd_inv_arr[-1]),
                (reference_values['iac']-reference_values['iac_err'], reference_values['iac']-reference_values['iac_err']),
                (reference_values['iac']+reference_values['iac_err'], reference_values['iac']+reference_values['iac_err']),
                color='#80808080')
        ax4.plot(
                (0, logd_inv_arr[-1]),
                (reference_values['acphase'], reference_values['acphase']),
                'k:')
        ax4.fill_between(
                (0, logd_inv_arr[-1]),
                (reference_values['acphase']-reference_values['acphase_err'], reference_values['acphase']-reference_values['acphase_err']),
                (reference_values['acphase']+reference_values['acphase_err'], reference_values['acphase']+reference_values['acphase_err']),
                color='#80808080')

    ax1.set_title("DC current")
    ax1.scatter(
            x_j,
            table.dc_current[j],
            marker = 'o',
            s = s_j,
            c = c_j,
            linewidths = lw_j,
            norm = norm)
    ax1.scatter(
            x_good,
            table.dc_current[mu_good],
            marker = '*',
            s = s_good,
            c = c_good,
            linewidths = lw_good,
            norm = norm)
    ax1.scatter(
            x_bad,
            table.dc_current[mu_bad],
            marker = '+',
            s = s_bad,
            c = c_bad,
            linewidths = lw_bad,
            norm = norm)
    ax1.scatter(
            x_old,
            table.dc_current[mu_old],
            marker = 'x',
            s = s_old,
            c = c_old,
            linewidths = lw_old,
            norm = norm)

    bb_good = table.dc_current[mod_good].mean()
    aa_good = (x_good_shifted*table.dc_current[mod_good]).sum()/s_good_xx
    ax1.plot(logd_inv_arr, aa_good*(logd_inv_arr**4 - x_good_mean) + bb_good, linewidth=0.5)

    bb_old = table.dc_current[mod_old].mean()
    aa_old = (x_old_shifted*table.dc_current[mod_old]).sum()/s_old_xx
    ax1.plot(logd_inv_arr, aa_old*(logd_inv_arr**3 - x_old_mean) + bb_old, linewidth=0.5)

    bb_bad = table.dc_current[mod_bad].mean()
    aa_bad = (x_bad_shifted*table.dc_current[mod_bad]).sum()/s_bad_xx
    ax1.plot(logd_inv_arr, aa_bad*(logd_inv_arr**2 - x_bad_mean) + bb_bad, linewidth=0.5)

    #for name, selection in selections.items():
    #    try:
    #        (a, b, c), covar = curve_fit(
    #                fit_func,
    #                logd_inv[selection],
    #                table.dc_current[selection],
    #                (bb-aa*x_mean, aa, 3))
    #        aerr, berr, cerr = covar.diagonal()**0.5
    #        print(f"DC current ({name:9}): a={a:.9}±{aerr:.9}, b={b:.9}±{berr:.9}, c={c:.9}±{cerr:.9}")
    #        ax1.plot(logd_inv_arr, fit_func(logd_inv_arr, a, b, c), label=name)
    #        ax1.plot((0,logd_inv_arr[-1]), (a,a), ':', label=name)
    #    except:
    #        pass

    ax2.set_title("DC conductance")
    ax2.scatter(
            x_j,
            table.dc_conductance[j],
            marker = 'o',
            s = s_j,
            c = c_j,
            linewidths = lw_j,
            norm = norm)
    ax2.scatter(
            x_good,
            table.dc_conductance[mu_good],
            marker = '*',
            s = s_good,
            c = c_good,
            linewidths = lw_good,
            norm = norm)
    ax2.scatter(
            x_old,
            table.dc_conductance[mu_old],
            marker = '+',
            s = s_old,
            c = c_old,
            linewidths = lw_old,
            norm = norm)
    ax2.scatter(
            x_bad,
            table.dc_conductance[mu_bad],
            marker = 'x',
            s = s_bad,
            c = c_bad,
            linewidths = lw_bad,
            norm = norm)

    ax3.set_title("AC current")
    ax3.scatter(
            x_j,
            table.ac_current_abs[j],
            marker = 'o',
            s = s_j,
            c = c_j,
            linewidths = lw_j,
            norm = norm)
    ax3.scatter(
            x_good,
            table.ac_current_abs[mu_good],
            marker = '*',
            s = s_good,
            c = c_good,
            linewidths = lw_good,
            norm = norm)
    ax3.scatter(
            x_old,
            table.ac_current_abs[mu_old],
            marker = '+',
            s = s_old,
            c = c_old,
            linewidths = lw_old,
            norm = norm)
    ax3.scatter(
            x_bad,
            table.ac_current_abs[mu_bad],
            marker = 'x',
            s = s_bad,
            c = c_bad,
            linewidths = lw_bad,
            norm = norm)

    b = table.ac_current_abs[mu_good].mean()
    a = (x_good_shifted*table.ac_current_abs[mu_good]).sum()/s_good_xx
    ax3.plot(logd_inv_arr, a*(logd_inv_arr**4 - x_good_mean) + b, linewidth=0.5)

    b = table.ac_current_abs[mu_old].mean()
    a = (x_old_shifted*table.ac_current_abs[mu_old]).sum()/s_old_xx
    ax3.plot(logd_inv_arr, a*(logd_inv_arr**3 - x_old_mean) + b, linewidth=0.5)

    b = table.ac_current_abs[mu_bad].mean()
    a = (x_bad_shifted*table.ac_current_abs[mu_bad]).sum()/s_bad_xx
    ax3.plot(logd_inv_arr, a*(logd_inv_arr**2 - x_bad_mean) + b, linewidth=0.5)

    #for name, selection in selections.items():
    #    try:
    #        (a, b, c), covar = curve_fit(
    #                fit_func,
    #                logd_inv[selection],
    #                table.ac_current_abs[selection],
    #                (bb-aa*x_mean, aa, 3))
    #        aerr, berr, cerr = covar.diagonal()**0.5
    #        print(f"AC current ({name:9}): a={a:.9}±{aerr:.9}, b={b:.9}±{berr:.9}, c={c:.9}±{cerr:.9}")
    #        ax3.plot(logd_inv_arr, fit_func(logd_inv_arr, a, b, c), label=name)
    #        ax3.plot((0,logd_inv_arr[-1]), (a,a), ':', label=name)
    #    except:
    #        pass

    ax4.set_title("AC phase")
    ax4.scatter(
            x_j,
            table.ac_current_phase[j],
            marker='o',
            s=s_j,
            c=c_j,
            linewidths=lw_j,
            norm=norm)
    ax4.scatter(
            x_good,
            table.ac_current_phase[mu_good],
            marker='*',
            s=s_good,
            c=c_good,
            linewidths=lw_good,
            norm=norm)
    ax4.scatter(
            x_old,
            table.ac_current_phase[mu_old],
            marker='+',
            s=s_old,
            c=c_old,
            linewidths=lw_old,
            norm=norm)
    ax4.scatter(
            x_bad,
            table.ac_current_phase[mu_bad],
            marker='x',
            s=s_bad,
            c=c_bad,
            linewidths=lw_bad,
            norm=norm)

    b = table.ac_current_phase[mu_good].mean()
    a = (x_good_shifted*table.ac_current_phase[mu_good]).sum()/s_good_xx
    ax4.plot(logd_inv_arr, a*(logd_inv_arr**4 - x_good_mean) + b, linewidth=0.5)

    b = table.ac_current_phase[mu_old].mean()
    a = (x_old_shifted*table.ac_current_phase[mu_old]).sum()/s_old_xx
    ax4.plot(logd_inv_arr, a*(logd_inv_arr**3 - x_old_mean) + b, linewidth=0.5)

    b = table.ac_current_phase[mu_bad].mean()
    a = (x_bad_shifted*table.ac_current_phase[mu_bad]).sum()/s_bad_xx
    ax4.plot(logd_inv_arr, a*(logd_inv_arr**2 - x_bad_mean) + b, linewidth=0.5)

    #for name, selection in selections.items():
    #    try:
    #        (a, b, c), covar = curve_fit(
    #                fit_func,
    #                logd_inv[selection],
    #                table.ac_current_phase[selection],
    #                (bb-aa*x_mean, aa, 3))
    #        aerr, berr, cerr = covar.diagonal()**0.5
    #        print(f"AC phase ({name:9}): a={a:.9}±{aerr:.9}, b={b:.9}±{berr:.9}, c={c:.9}±{cerr:.9}")
    #        ax4.plot(logd_inv_arr, fit_func(logd_inv_arr, a, b, c), label=name)
    #        ax4.plot((0,logd_inv_arr[-1]), (a,a), ':', label=name)
    #    except:
    #        pass


def convergence_overview(dm, **parameters):
    """
    Plot convergence of current as function of D (Λ₀) for a fine
    grid of dc and ac voltage at fixed omega.
    """
    assert isinstance(parameters["omega"], Number)
    parameters.pop("d", None)
    vac_num = 26
    vdc_num = 26
    vdc_max = 82.686
    vac_max = 82.686
    exponent = 3
    if parameters["good_flags"] & DataManager.SOLVER_FLAGS["simplified_initial_conditions"]:
        exponent = 2
    elif parameters["good_flags"] & DataManager.SOLVER_FLAGS["improved_initial_conditions"]:
        exponent = 4
    d_num = 5
    log10d_min = 5
    log10d_max = 9
    d_arr = np.logspace(log10d_min, log10d_max, d_num)
    gdc = np.empty((d_num, vac_num, vdc_num), dtype=np.float64)
    idc = np.empty((d_num, vac_num, vdc_num), dtype=np.float64)
    iac = np.empty((d_num, vac_num, vdc_num), dtype=np.float64)
    phase = np.empty((d_num, vac_num, vdc_num), dtype=np.float64)
    for i, d in enumerate(d_arr):
        data = dm.list(d=d, **parameters)
        vdc, vac, gdc[i], idc[i], iac[i], phase[i] = filter_grid_data(data, vac_max=vac_max, vdc_max=vdc_max, vac_num=vac_num, vdc_num=vdc_num)

    gdc_fit_a = np.ndarray((vac_num, vdc_num), dtype=np.float64)
    gdc_fit_b = np.ndarray((vac_num, vdc_num), dtype=np.float64)
    idc_fit_a = np.ndarray((vac_num, vdc_num), dtype=np.float64)
    idc_fit_b = np.ndarray((vac_num, vdc_num), dtype=np.float64)
    iac_fit_a = np.ndarray((vac_num, vdc_num), dtype=np.float64)
    iac_fit_b = np.ndarray((vac_num, vdc_num), dtype=np.float64)
    j = np.vectorize(lambda d: solveTV0_scalar(d, rtol=1e-9, atol=1e-11)[2])
    logd_inv3 = j(d_arr)**exponent
    #logd_inv3 = np.log(d_arr)**-exponent
    fit_func = lambda x, y: x[0] + x[1] * logd_inv3 - y
    for i in range(vac_num):
        for j in range(vdc_num):
            (gdc_fit_a[i,j], gdc_fit_b[i,j]), trash = leastsq(fit_func, (gdc[-1,i,j], 0.), args=(gdc[:,i,j]))
            (idc_fit_a[i,j], idc_fit_b[i,j]), trash = leastsq(fit_func, (idc[-1,i,j], 0.), args=(idc[:,i,j]))
            (iac_fit_a[i,j], iac_fit_b[i,j]), trash = leastsq(fit_func, (iac[-1,i,j], 0.), args=(iac[:,i,j]))
    fig, (ax1, ax2, ax3) = plt.subplots(nrows=1, ncols=3, sharex=True)
    ax1.plot(logd_inv3, -idc_fit_b.reshape((1,-1))*logd_inv3.reshape((d_num,1)), color="gray", linewidth=0.5)
    ax1.plot(logd_inv3, idc_fit_a.reshape((1,-1)) - idc.reshape((d_num,-1)))
    ax2.plot(logd_inv3, -iac_fit_b.reshape((1,-1))*logd_inv3.reshape((d_num,1)), color="gray", linewidth=0.5)
    ax2.plot(logd_inv3, iac_fit_a.reshape((1,-1)) - iac.reshape((d_num,-1)))
    ax3.plot(logd_inv3, -gdc_fit_b.reshape((1,-1))*logd_inv3.reshape((d_num,1)), color="gray", linewidth=0.5)
    ax3.plot(logd_inv3, gdc_fit_a.reshape((1,-1)) - gdc.reshape((d_num,-1)))

    fig, ((ax1, ax2, ax3), (ax4, ax5, ax6), (ax7, ax8, ax9)) = plt.subplots(nrows=3, ncols=3, sharex=True, sharey=True)
    #fig, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(nrows=2, ncols=3, sharex=True, sharey=True)
    extent = (-0.5*vdc_max/(vdc_num-1), vdc_max*(1+0.5/(vdc_num-1)), -0.5*vac_max/(vac_num-1), vac_max*(1+0.5/(vac_num-1)))
    ax1.set_title(r"$I_\mathrm{avg}(\Lambda_0=\infty)$")
    ax2.set_title(r"$\frac{|I_\mathrm{avg}(\Lambda_0=10^9 T_K') - I_\mathrm{avg}(\Lambda_0=\infty)|}{I_\mathrm{avg}(\Lambda_0=10^9T_K')}$")
    ax3.set_title(r"fit quality")
    ax4.set_title(r"$I_\mathrm{osc}(\Lambda_0=\infty)$")
    ax5.set_title(r"$\frac{|I_\mathrm{osc}(\Lambda_0=10^9 T_K') - I_\mathrm{osc}(\Lambda_0=\infty)|}{I_\mathrm{osc}(\Lambda_0=10^9T_K')}$")
    ax6.set_title(r"fit quality")
    idc_diff = (abs(idc_fit_a) > 1e-6) * (((idc_fit_a.reshape((1,vac_num,vdc_num)) + idc_fit_b.reshape((1,vac_num,vdc_num))*logd_inv3.reshape((d_num,1,1)) - idc)**2).sum(axis=0)/d_num)**0.5 / (idc_fit_b * logd_inv3[-1])
    iac_diff = (abs(iac_fit_a) > 1e-6) * (((iac_fit_a.reshape((1,vac_num,vdc_num)) + iac_fit_b.reshape((1,vac_num,vdc_num))*logd_inv3.reshape((d_num,1,1)) - iac)**2).sum(axis=0)/d_num)**0.5 / (iac_fit_b * logd_inv3[-1])
    gdc_diff = (((gdc_fit_a.reshape((1,vac_num,vdc_num)) + gdc_fit_b.reshape((1,vac_num,vdc_num))*logd_inv3.reshape((d_num,1,1)) - gdc)**2).sum(axis=0)/d_num)**0.5 / (gdc_fit_b * logd_inv3[-1])
    img = ax1.imshow(idc_fit_a, extent=extent, origin="lower")
    fig.colorbar(img, ax=ax1)
    img = ax2.imshow((abs(idc_fit_a)>1e-6)*idc_fit_b*(-logd_inv3[4])/idc_fit_a, extent=extent, origin="lower")
    fig.colorbar(img, ax=ax2)
    img = ax3.imshow(-idc_diff, extent=extent, origin="lower")
    fig.colorbar(img, ax=ax3)
    img = ax4.imshow(iac_fit_a, extent=extent, origin="lower")
    fig.colorbar(img, ax=ax4)
    img = ax5.imshow((abs(iac_fit_a)>1e-6)*iac_fit_b*(-logd_inv3[4])/iac_fit_a, extent=extent, origin="lower")
    fig.colorbar(img, ax=ax5)
    img = ax6.imshow(-iac_diff, extent=extent, origin="lower")
    fig.colorbar(img, ax=ax6)
    img = ax7.imshow(gdc_fit_a, extent=extent, origin="lower")
    fig.colorbar(img, ax=ax7)
    img = ax8.imshow(gdc_fit_b*logd_inv3[4]/gdc_fit_a, extent=extent, origin="lower")
    fig.colorbar(img, ax=ax8)
    img = ax9.imshow(gdc_diff, extent=extent, origin="lower")
    fig.colorbar(img, ax=ax9)

    #cmap = plt.cm.seismic
    #gdc_diff = (merged.dc_conductance_1 - merged.dc_conductance_2)/merged.dc_conductance_2
    #max_gdc_diff = np.abs(gdc_diff).max()
    #norm = plt.Normalize(-max_gdc_diff, max_gdc_diff)
    #s = ax1.scatter(merged.vdc, merged.vac, c=gdc_diff, cmap=cmap, norm=norm)
    #fig.colorbar(s, ax=ax1)

    #idc_diff = (merged.dc_current_1 - merged.dc_current_2)/(merged.dc_current_2 + (np.abs(merged.dc_current_2) < 1e-6))
    #max_idc_diff = np.abs(idc_diff).max()
    #norm = plt.Normalize(-max_idc_diff, max_idc_diff)
    #s = ax2.scatter(merged.vdc, merged.vac, c=idc_diff, cmap=cmap, norm=norm)
    #fig.colorbar(s, ax=ax2)

    #iac_diff = (merged.ac_current_abs_1 - merged.ac_current_abs_2)/(merged.ac_current_abs_2 + (np.abs(merged.ac_current_abs_2) < 1e-6 ))
    #max_iac_diff = np.abs(iac_diff).max()
    #norm = plt.Normalize(-max_iac_diff, max_iac_diff)
    #s = ax3.scatter(merged.vdc, merged.vac, c=iac_diff, cmap=cmap, norm=norm)
    #fig.colorbar(s, ax=ax3)

    plt.show()


def compare_RGeq(dm, omega=16.5372, **parameters):
    for name in ("include_Ga", "solve_integral_exactly", "integral_method", "second_order_rg_equations"):
        parameters.pop(name, None)
    data = dm.list(omega=omega, **parameters)
    bad_flags = DataManager.SOLVER_FLAGS["simplified_initial_conditions"] \
            | DataManager.SOLVER_FLAGS["extrapolate_voltage"] \
            | DataManager.SOLVER_FLAGS["deleted"]
    precision_flags = DataManager.SOLVER_FLAGS["include_Ga"] | DataManager.SOLVER_FLAGS["solve_integral_exactly"]
    data = data.loc[data.solver_flags & bad_flags == 0]
    order2 = (data.solver_flags & precision_flags == 0) \
            & (data.solver_flags & DataManager.SOLVER_FLAGS["second_order_rg_equations"] != 0)
    order3 = (data.solver_flags & precision_flags == 0) \
            & (data.solver_flags & DataManager.SOLVER_FLAGS["second_order_rg_equations"] == 0)
    order3a = (data.solver_flags & precision_flags == DataManager.SOLVER_FLAGS["include_Ga"]) \
            & (data.solver_flags & DataManager.SOLVER_FLAGS["second_order_rg_equations"] == 0)
    order3plus = (data.solver_flags & precision_flags == precision_flags) \
            & (data.solver_flags & DataManager.SOLVER_FLAGS["second_order_rg_equations"] == 0) \
            & (data.integral_method == -1)

    data.vac = data.vac.round(9)
    data.vdc = data.vdc.round(9)
    merged2 = pd.merge(
            data[order2],
            data[order3plus],
            how="inner",
            on=["vdc","vac","d","omega","method","solver_tol_rel","solver_tol_abs","resonant_dc_shift","padding","energy_im","energy_re","xL","lazy_inverse_factor"],
            suffixes=("", "_reference"))
    merged3a = pd.merge(
            data[order3a],
            data[order3plus],
            how="inner",
            on=["vdc","vac","d","omega","method","solver_tol_rel","solver_tol_abs","resonant_dc_shift","padding","energy_im","energy_re","xL","lazy_inverse_factor"],
            suffixes=("", "_reference"))
    merged3 = pd.merge(
            data[order3],
            data[order3plus],
            how="inner",
            on=["vdc","vac","d","omega","method","solver_tol_rel","solver_tol_abs","resonant_dc_shift","padding","energy_im","energy_re","xL","lazy_inverse_factor"],
            suffixes=("", "_reference"))

    vdc_o2, vac_o2, gdc_o2, idc_o2, iac_o2, phase_o2 = filter_grid_data(
            data[order2],
            omega = omega,
            vac_min = 0,
            vac_max = 165.372,
            vac_num = 51,
            vdc_min = 0,
            vdc_max = 165.372,
            vdc_num = 51,
            )
    vdc_o3, vac_o3, gdc_o3, idc_o3, iac_o3, phase_o3 = filter_grid_data(
            data[order3],
            omega = omega,
            vac_min = 0,
            vac_max = 165.372,
            vac_num = 101,
            vdc_min = 0,
            vdc_max = 165.372,
            vdc_num = 101,
            )
    vdc_o3a, vac_o3a, gdc_o3a, idc_o3a, iac_o3a, phase_o3a = filter_grid_data(
            data[order3a],
            omega = omega,
            vac_min = 0,
            vac_max = 165.372,
            vac_num = 51,
            vdc_min = 0,
            vdc_max = 165.372,
            vdc_num = 51,
            )
    vdc_o3p, vac_o3p, gdc_o3p, idc_o3p, iac_o3p, phase_o3p = filter_grid_data(
            data[order3plus],
            omega = omega,
            vac_min = 0,
            vac_max = 165.372,
            vac_num = 51,
            vdc_min = 0,
            vdc_max = 165.372,
            vdc_num = 51,
            )

    fig, ((ax1, ax2, ax5), (ax3, ax4, ax6)) = plt.subplots(ncols=3, nrows=2, sharex=True, sharey=True)
    #levels = np.linspace(0, 0.4/np.pi, 41)
    levels = (1/np.pi) / np.arange(1, 13)[::-1]
    abs_diff_o2 = np.pi*(merged2.dc_conductance - merged2.dc_conductance_reference)
    abs_diff_o3 = np.pi*(merged3.dc_conductance - merged3.dc_conductance_reference)
    abs_diff_o3a = np.pi*(merged3a.dc_conductance - merged3a.dc_conductance_reference)
    max_diff = max(
            np.abs(abs_diff_o2).max(),
            np.abs(abs_diff_o3).max(),
            np.abs(abs_diff_o3a).max()
            )
    abs_norm = plt.Normalize(-max_diff, max_diff)
    s1 = ax1.scatter(merged2.vdc, merged2.vac, c=abs_diff_o2, norm=abs_norm, cmap=plt.cm.seismic)
    s2 = ax2.scatter(merged3.vdc, merged3.vac, c=abs_diff_o3, norm=abs_norm, cmap=plt.cm.seismic)
    s3 = ax5.scatter(merged3a.vdc, merged3a.vac, c=abs_diff_o3a, norm=abs_norm, cmap=plt.cm.seismic)
    fig.colorbar(s1, ax=[ax1, ax2, ax5])
    ax1.contour(vdc_o3p, vac_o3p, gdc_o3p, levels, colors="#00ff00", alpha=0.8, linewidths=0.5)
    ax1.contour(vdc_o2, vac_o2, gdc_o2, levels, colors="#202020", alpha=0.8, linewidths=0.5)
    ax2.contour(vdc_o3p, vac_o3p, gdc_o3p, levels, colors="#00ff00", alpha=0.8, linewidths=0.5)
    ax2.contour(vdc_o3, vac_o3, gdc_o3, levels, colors="#202020", alpha=0.8, linewidths=0.5)
    ax5.contour(vdc_o3p, vac_o3p, gdc_o3p, levels, colors="#00ff00", alpha=0.8, linewidths=0.5)
    ax5.contour(vdc_o3a, vac_o3a, gdc_o3a, levels, colors="#202020", alpha=0.8, linewidths=0.5)
    #plot_contours(ax1, data.vdc[order3plus], data.vac[order3plus], data.dc_conductance[order3plus], levels, ".", color="#004400", alpha=0.8)
    #ax1.tricontour(data.vdc[order3plus], data.vac[order3plus], data.dc_conductance[order3plus], levels, colors="#008800", alpha=0.8, antialiased=True)
    #vdc = np.linspace(0, 165.372, 101)
    #vac = np.linspace(0, 165.372, 101)
    #grid_gdc_o3p = griddata((data.vdc[order3plus], data.vac[order3plus]), data.dc_conductance[order3plus], (vdc, vac), method="cubic")
    #ax1.contour(vdc, vac, grid_gdc_o3p, levels, colors="#00ff00", alpha=0.8, linewidths=0.5)

    rel_diff_o2 = (merged2.dc_conductance - merged2.dc_conductance_reference) / merged2.dc_conductance_reference
    rel_diff_o3 = (merged3.dc_conductance - merged3.dc_conductance_reference) / merged3.dc_conductance_reference
    rel_diff_o3a = (merged3a.dc_conductance - merged3a.dc_conductance_reference) / merged3a.dc_conductance_reference
    max_rel_diff = max(
            np.abs(rel_diff_o2).max(),
            np.abs(rel_diff_o3a).max(),
            np.abs(rel_diff_o3).max()
            )
    rel_norm = plt.Normalize(-max_rel_diff, max_rel_diff)
    s4 = ax3.scatter(merged2.vdc, merged2.vac, c=rel_diff_o2, norm=rel_norm, cmap=plt.cm.seismic)
    s5 = ax4.scatter(merged3.vdc, merged3.vac, c=rel_diff_o3, norm=rel_norm, cmap=plt.cm.seismic)
    s6 = ax6.scatter(merged3a.vdc, merged3a.vac, c=rel_diff_o3a, norm=rel_norm, cmap=plt.cm.seismic)
    fig.colorbar(s4, ax=[ax3, ax4, ax6])
    ax3.contour(vdc_o3p, vac_o3p, gdc_o3p, levels, colors="#00ff00", alpha=0.8, linewidths=0.5)
    ax3.contour(vdc_o2, vac_o2, gdc_o2, levels, colors="#202020", alpha=0.8, linewidths=0.5)
    ax4.contour(vdc_o3p, vac_o3p, gdc_o3p, levels, colors="#00ff00", alpha=0.8, linewidths=0.5)
    ax4.contour(vdc_o3, vac_o3, gdc_o3, levels, colors="#202020", alpha=0.8, linewidths=0.5)
    ax6.contour(vdc_o3p, vac_o3p, gdc_o3p, levels, colors="#00ff00", alpha=0.8, linewidths=0.5)
    ax6.contour(vdc_o3a, vac_o3a, gdc_o3a, levels, colors="#202020", alpha=0.8, linewidths=0.5)


def compare_RGeq_interp(
        dm = None,
        filename = "figdata/omega5_interp.npz",
        method = "mu",
        ref_method = "mu",
        observable = "gdc",
        suffix = "",
        ref_filename = None,
        **trashparams):
    data = np.load(filename)
    prefactor = 1
    missing_levels = None
    if observable[:5] == "gamma":
        levels = np.linspace(5, 30, 51)
    elif observable[0] == "g":
        prefactor = np.pi
        levels = (1/np.pi) / np.linspace(1, 13, 25)[::-1]
    elif observable[0] == "i":
        levels = np.linspace(0, 20, 51)
    elif observable[:5] == "phase":
        levels = np.linspace(-np.pi, np.pi, 41)
    else:
        raise ValueError(f"unknown observable: {observable}")
    vdc = data["vdc"][0]
    vac = data["vac"][:,0]
    extent = (1.5*vdc[0] - 0.5*vdc[1], 1.5*vdc[-1] - 0.5*vdc[-2], 1.5*vac[0] - 0.5*vac[1], 1.5*vac[-1] - 0.5*vac[-2])
    imshow_kwargs = dict(cmap=plt.cm.seismic, extent=extent, origin="lower", aspect="auto", zorder=0)
    contour_kwargs = dict(colors="#000000", alpha=0.7, linewidths=0.5, zorder=2)
    ref_contour_kwargs = dict(colors="#00f000", linewidths=0.5, zorder=1)

    fig, axes = plt.subplots(nrows=2, ncols=3+(ref_method!=method), sharex=True, sharey=True)
    ((ax1, ax2, ax5), (ax3, ax4, ax6)) = axes[:,:3]
    for ax in axes[-1]:
        ax.set_xlabel("Vdc / Tkrg")
    for ax in axes[:,0]:
        ax.set_ylabel("Vac / Tkrg")
    ax1.set_xlim(vdc[0], vdc[-1])
    ax1.set_ylim(vac[0], vac[-1])

    if ref_filename is not None:
        refdata = np.load(ref_filename)
        ref_data = refdata[f"{observable}_{ref_method}_o3p{suffix}"]
    else:
        ref_data = data[f"{observable}_{ref_method}_o3p{suffix}"]

    for ax in axes.flat:
        ax.contour(vdc, vac, ref_data, levels, **ref_contour_kwargs)

    abs_diff_o2 = prefactor*(data[f"{observable}_{method}_o2{suffix}"] - ref_data)
    abs_diff_o3 = prefactor*(data[f"{observable}_{method}_o3{suffix}"] - ref_data)
    abs_diff_o3a = prefactor*(data[f"{observable}_{method}_o3a{suffix}"] - ref_data)
    max_diff = min(max(np.abs(abs_diff_o2).max(), np.abs(abs_diff_o3).max(), np.abs(abs_diff_o3a).max()), 0.1)
    abs_norm = plt.Normalize(-max_diff, max_diff)
    img1 = ax1.imshow(abs_diff_o2, norm=abs_norm, **imshow_kwargs)
    img2 = ax2.imshow(abs_diff_o3, norm=abs_norm, **imshow_kwargs)
    img3 = ax5.imshow(abs_diff_o3a, norm=abs_norm, **imshow_kwargs)
    fig.colorbar(img1, ax=axes[0])

    ax1.contour(vdc, vac, data[f"{observable}_{method}_o2{suffix}"], levels, **contour_kwargs)
    ax2.contour(vdc, vac, data[f"{observable}_{method}_o3{suffix}"], levels, **contour_kwargs)
    ax5.contour(vdc, vac, data[f"{observable}_{method}_o3a{suffix}"], levels, **contour_kwargs)

    rel_diff_o2 = (data[f"{observable}_{method}_o2{suffix}"] - ref_data) / ref_data
    rel_diff_o3 = (data[f"{observable}_{method}_o3{suffix}"] - ref_data) / ref_data
    rel_diff_o3a = (data[f"{observable}_{method}_o3a{suffix}"] - ref_data) / ref_data
    max_rel_diff = min(max(np.abs(rel_diff_o2).max(), np.abs(rel_diff_o3).max(), np.abs(rel_diff_o3a).max()), 0.1)
    rel_norm = plt.Normalize(-max_rel_diff, max_rel_diff)
    img4 = ax3.imshow(rel_diff_o2, norm=rel_norm, **imshow_kwargs)
    img5 = ax4.imshow(rel_diff_o3, norm=rel_norm, **imshow_kwargs)
    img6 = ax6.imshow(rel_diff_o3a, norm=rel_norm, **imshow_kwargs)
    fig.colorbar(img4, ax=axes[1])

    ax3.contour(vdc, vac, data[f"{observable}_{method}_o2{suffix}"], levels, **contour_kwargs)
    ax4.contour(vdc, vac, data[f"{observable}_{method}_o3{suffix}"], levels, **contour_kwargs)
    ax6.contour(vdc, vac, data[f"{observable}_{method}_o3a{suffix}"], levels, **contour_kwargs)

    if ref_method != method:
        abs_diff_o3p = prefactor*(data[f"{observable}_{method}_o3p{suffix}"] - ref_data)
        rel_diff_o3p = (data[f"{observable}_{method}_o3p{suffix}"] - ref_data) / ref_data
        ax7, ax8 = axes[:,2]
        ax7.imshow(abs_diff_o3p, norm=abs_norm, **imshow_kwargs)
        ax8.imshow(rel_diff_o3p, norm=abs_norm, **imshow_kwargs)
        ax7.contour(vdc, vac, data[f"{observable}_{method}_o3p{suffix}"], levels, **contour_kwargs)
        ax8.contour(vdc, vac, data[f"{observable}_{method}_o3p{suffix}"], levels, **contour_kwargs)

    #fig, ax = plt.subplots()
    #ax.set_xlabel("Vdc / Tkrg")
    #ax.set_ylabel("Vac / Tkrg")
    #ax.set_xlim(vdc[0], vdc[-1])
    #ax.set_ylim(vac[0], vac[-1])
    #imshow_kwargs["cmap"] = plt.cm.viridis
    #ax.imshow(prefactor * ref_data, norm=plt.Normalize(0.08, 0.34), **imshow_kwargs)
    #ax.contour(vdc, vac, ref_data, levels, **contour_kwargs)
    #if missing_levels is not None:
    #    contour_kwargs["colors"] = "#ff0000"
    #    contour_kwargs["alpha"] = 1
    #    ax.contour(vdc, vac, ref_data, missing_levels, **contour_kwargs)

def GNorm(vmin, vmax):
    assert vmax > vmin > 0
    scale = 1 - vmin/vmax
    return FuncNorm(((lambda x: (1 - vmin/x)/scale), (lambda x: vmin/(1 - scale*x))), vmin, vmax)


def RGeq_comparison_order2(
        dm = None,
        filename = "figdata/omega5_interp.npz",
        method = "mu",
        compare = "o3a",
        #compare = "o3a",
        reference = "o3p",
        reference_method = "mu",
        #ref_filename = "figdata/omega5_interp_vb7.npz",
        ref_filename = "figdata/omega5_interp.npz",
        observable = "gdc",
        prefactor = np.pi,
        **trashparams):
    """
    Compare two different results for the differential conductance
    (or another observable) side-by-side and by plotting their absolute
    and relative difference.
    Used to generate some draft figures for the thesis.
    """
    fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, sharex=True, sharey=True)
    data = np.load(filename)
    cmp = prefactor*data[f"{observable}_{method}_{compare}"]
    if ref_filename is None:
        ref = prefactor*data[f"{observable}_{reference_method}_{reference}"]
    else:
        ref_data = np.load(ref_filename)
        ref = prefactor*ref_data[f"{observable}_{reference_method}_{reference}"]
    #gdc_o3 = np.pi*data["gdc_mu_o3"]
    #norm = LogNorm(0.08, 0.4)
    norm = GNorm(0.08, 1)
    cmap = plt.cm.viridis
    omega = 16.5372
    vdc = data["vdc"][0] / omega
    vac = data["vac"][:,0] / omega
    extent1 = (1.5*vdc[0] - 0.5*vdc[1], 1.5*vdc[-1] - 0.5*vdc[-2], 1.5*vac[0] - 0.5*vac[1], 1.5*vac[-1] - 0.5*vac[-2])
    extent2 = (1.5*vdc[0] - 0.5*vdc[1], -1.5*vdc[-1] + 0.5*vdc[-2], 1.5*vac[0] - 0.5*vac[1], 1.5*vac[-1] - 0.5*vac[-2])
    img1 = ax1.imshow(ref, cmap=cmap, norm=norm, extent=extent1, origin="lower")
    img2 = ax1.imshow(cmp, cmap=cmap, norm=norm, extent=extent2, origin="lower")
    fig.colorbar(img1, ax=ax1, location="right")
    ccmap = plt.cm.seismic
    absdiff = cmp - ref
    absdiff_max = np.abs(absdiff).max()
    reldiff = absdiff/ref
    reldiff_max = np.abs(reldiff).max()
    img3 = ax2.imshow(reldiff, cmap=ccmap, norm=plt.Normalize(-reldiff_max, reldiff_max), extent=extent1, origin="lower")
    img4 = ax2.imshow(absdiff, cmap=ccmap, norm=plt.Normalize(-absdiff_max, absdiff_max), extent=extent2, origin="lower")
    fig.colorbar(img3, ax=ax2, location="right")
    fig.colorbar(img4, ax=ax2, location="left")
    ax1.set_xlim(extent2[1], extent1[1])
    ax1.set_ylim(extent1[2], extent1[3])
    ax2.set_xlabel(r"$V_\mathrm{avg}~(\Omega)$")
    ax1.set_ylabel(r"$V_\mathrm{osc}~(\Omega)$")
    ax2.set_ylabel(r"$V_\mathrm{osc}~(\Omega)$")
    plt.show()

def RGeq_comparison_contours(
        dm = None,
        filename = "figdata/omega5_interp.npz",
        method = "mu",
        compare1 = "o2",
        compare2 = "o3a",
        reference = "o3p",
        observable = "gdc",
        prefactor = np.pi,
        comp1_filename = None,
        comp2_filename = None,
        **trashparams):
    """
    Example usage:
    >>> RGeq_comparison_contours(comp1_filename="figdata/omega5_interp_vb7.npz", compare1="o3a", compare2="o3a", reference="o3a")
    """
    contour_kwargs = dict(colors="#000000", linewidths=0.8, zorder=2)
    ref_contour_kwargs = dict(colors="#ff0000", linewidths=0.8, zorder=1)
    data = np.load(filename)
    if comp1_filename is not None:
        comp1_data = np.load(comp1_filename)
        assert np.allclose(data["vdc"], comp1_data["vdc"])
        assert np.allclose(data["vac"], comp1_data["vac"])
        cmp1 = prefactor*comp1_data[f"{observable}_{method}_{compare1}"]
    else:
        cmp1 = prefactor*data[f"{observable}_{method}_{compare1}"]
    if comp2_filename is not None:
        comp2_data = np.load(comp2_filename)
        assert np.allclose(data["vdc"], comp2_data["vdc"])
        assert np.allclose(data["vac"], comp2_data["vac"])
        cmp2 = prefactor*comp2_data[f"{observable}_{method}_{compare2}"]
    else:
        cmp2 = prefactor*data[f"{observable}_{method}_{compare2}"]
    ref = prefactor*data[f"{observable}_{method}_{reference}"]
    omega = 16.5372
    vdc = data["vdc"][0] / omega
    vac = data["vac"][:,0] / omega
    levels = 1/np.linspace(1, 12, 12)[::-1]
    #levels = 0.05 + np.linspace(0.03**0.1, 0.9**.1, 20)**10
    #levels = np.logspace(-2,0,50)
    #levels = np.concatenate((
    #    np.linspace(0.01, 0.2, 20),
    #    np.linspace(0.22, 0.32, 6),
    #    np.linspace(0.4, 1, 4)))
    fig, ax = plt.subplots(nrows=1, ncols=1, sharex=True, sharey=True)
    ax.contour(vdc, vac, cmp1, levels, **contour_kwargs)
    ax.contour(-vdc, vac, cmp2, levels, **contour_kwargs)
    ax.contour(vdc, vac, ref, levels, **ref_contour_kwargs)
    ax.contour(-vdc, vac, ref, levels, **ref_contour_kwargs)
    ax.vlines((0,), 0, vac[-1], color="#808080", linewidth=0.5)
    ax.set_xlabel(r"$V_\mathrm{avg}~(\Omega)$")
    ax.set_ylabel(r"$V_\mathrm{osc}~(\Omega)$")
    plt.show()


def contours_to_coordinates(contour):
    for level, segments in zip(contour.levels, contour.allsegs):
        for seg in segments:
            yield level, seg
    #for level, collection in zip(contour.levels, contour.collections):
    #    for path in collection.get_paths():
    #        yield level, path.vertices

def contours_to_coordinates_unified(contour):
    for level, vertices in contours_to_coordinates(contour):
        array = np.empty((vertices.shape[0], 3), dtype=np.float64)
        array[:,:2] = vertices
        array[:,2] = level
        yield array

def save_contour_coordinates(contour, filename, x, y, z):
    """For usage in pgf plots. Overwrites filename."""
    with open(filename, "w") as file:
        print(f"{x} {y} {z}", file=file)
        for array in contours_to_coordinates_unified(contour):
            np.savetxt(file, array, "%.6f")
            print(file=file)


def RGeq_comparison_contours2(
        dm = None,
        filename = "figdata/omega5_interp_high_res.npz",
        observable = "gdc",
        prefactor = np.pi,
        **trashparams):
    omega = 16.5372
    kwargs = dict(linewidths=0.8)
    colors = dict(o2="#ff0000", o3="#6000ff", o3r="#ff00ff", o3a="#00f000", o3p="#000000", ref="#808080")
    zorder = dict(o2=1, o3=2, o3r=5, o3a=3, o3p=4)
    approximations = list(zorder.keys())
    data = np.load(filename)
    vdc = data["vdc"] / omega
    vac = data["vac"] / omega
    levels = (1/np.linspace(1, 12, 12)[::-1]) / prefactor
    fig, ax = plt.subplots(nrows=1, ncols=1, sharex=True, sharey=True)
    for approx in approximations:
        try:
            contour = ax.contour(vdc, vac, data[f"{observable}_mu_{approx}"], levels, zorder=zorder[approx], colors=colors[approx], **kwargs)
            save_contour_coordinates(contour, f"figdata/contour_{observable}_mu_{approx}.dat", "vdc", "vac", "gdc")
        except KeyError:
            settings.logger.warning(f"No data available for {observable}_mu_{approx}")
        try:
            contour = ax.contour(vdc, vac, data[f"{observable}_J_{approx}"], levels, zorder=zorder[approx], colors=colors[approx], **kwargs)
            save_contour_coordinates(contour, f"figdata/contour_{observable}_J_{approx}.dat", "vdc", "vac", "gdc")
        except KeyError:
            settings.logger.warning(f"No data available for {observable}_J_{approx}")
        try:
            contour = ax.contour(vdc, vac, data[f"{observable}_J_{approx}_p"], levels, zorder=zorder[approx], colors=colors[approx], **kwargs)
            save_contour_coordinates(contour, f"figdata/contour_{observable}_J_{approx}_p.dat", "vdc", "vac", "gdc")
        except KeyError:
            settings.logger.warning(f"No data available for {observable}_J_{approx}_p")

    #ax.contour(-vdc, vac, data[f"{observable}_mu_o3p"], levels, zorder=-1, colors=colors["ref"], **kwargs)
    #ax.vlines((0,), 0, vac[-1], color="#808080", linewidth=0.5)
    ax.set_xlabel(r"$V_\mathrm{avg}~(\Omega)$")
    ax.set_ylabel(r"$V_\mathrm{osc}~(\Omega)$")
    plt.show()


def plot_Ga_max(
        dm,
        omega = 16.5372,
        method = "mu",
        **parameters):
    """
    Show relevance of Ga: Plot maximum matrix element of Ga as function
    of Vdc and Vac for fixed Ω.
    """
    parameters["include_Ga"] = True
    vdc = []
    vac = []
    ga_max = []
    g2_max = []
    g200c = []
    ga00_max_c = []
    nmax_arr = []
    argmax = []
    for kondo in dm.load_all(omega=omega, method=method, **parameters):
        try:
            nmax = kondo.nmax
            ga = kondo.ga
            g2 = kondo.g2
            vdc.append(kondo.vdc)
            vac.append(kondo.vac)
            nmax_arr.append(nmax)
            if ga.ndim == 4:
                idx = np.abs(ga).argmax()
                ga_max.append(np.abs(ga.flat[idx]))
                g2_max.append(np.abs(g2).max())
                argmax.append(idx)
                ga00_max_c.append(np.abs(ga[0,0,nmax-4:nmax+5,nmax-4:nmax+5]).max())
                g200c.append(np.abs(g2[0,0,nmax,nmax]))
            elif ga.ndim == 5:
                vb = kondo.voltage_branches
                idx = np.abs(ga[:,:,vb]).argmax()
                ga_max.append(np.abs(ga[:,:,vb].flat[idx]))
                g2_max.append(np.abs(g2[:,:,vb]).max())
                argmax.append(idx)
                ga00_max_c.append(np.abs(ga[0,0,vb,nmax-4:nmax+5,nmax-4:nmax+5]).max())
                g200c.append(np.abs(g2[0,0,vb,nmax,nmax]))
            else:
                raise ValueError("Invalid shape: %s"%ga.shape)
        except:
            settings.logger.exception("Error while reading data:")
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(nrows=2, ncols=2, sharex=True, sharey=True)
    vdc = np.array(vdc)
    vac = np.array(vac)
    nmax = np.array(nmax_arr)
    argmax = np.array(argmax)
    ga_max = np.array(ga_max)
    g2_max = np.array(g2_max)
    g200c = np.array(g200c)
    s1 = ax1.scatter(vdc, vac, c=g2_max)
    s2 = ax3.scatter(vdc, vac, c=ga_max/g2_max)
    s3 = ax4.scatter(vdc, vac, c=g200c-g2_max, norm=plt.Normalize(-1e-6, 1e-6), cmap=plt.cm.seismic)
    vdc_arr = np.linspace(0, 165.372, 400)
    vac_arr = np.linspace(0, 165.372, 400)
    vdc_mesh, vac_mesh = np.meshgrid(vdc_arr, vac_arr)
    grid_ga_max = griddata((vdc, vac), ga_max, (vdc_mesh, vac_mesh), method="cubic")
    grid_g2_max = griddata((vdc, vac), g2_max, (vdc_mesh, vac_mesh), method="cubic")
    img = ax2.imshow(grid_ga_max, extent=(0, 165.372, 0, 165.372), cmap=plt.cm.viridis, origin="lower")
    fig.colorbar(s1, ax=ax1)
    fig.colorbar(img, ax=ax2)
    fig.colorbar(s2, ax=ax3)
    fig.colorbar(s3, ax=ax4)
    extent = tuple((1.5*a-0.5*b)/omega for (a,b) in zip((vdc_arr[0], vdc_arr[-1], vac_arr[0], vac_arr[-1]), (vdc_arr[1], vdc_arr[-2], vac_arr[1], vac_arr[-2])))
    fig, ax = plt.subplots()
    img = ax.imshow(grid_ga_max,
                     extent=extent,
                     cmap=plt.cm.viridis,
                     origin="lower")
    fig.colorbar(img, ax=ax)
    ax.set_xlabel(r"$V_\mathrm{avg}~(\Omega)$")
    ax.set_ylabel(r"$V_\mathrm{osc}~(\Omega)$")
    fig, ax = plt.subplots()
    img = ax.imshow(grid_ga_max/grid_g2_max,
                     extent=extent,
                     cmap=plt.cm.viridis,
                     origin="lower")
    fig.colorbar(img, ax=ax)
    ax.set_xlabel(r"$V_\mathrm{avg}~(\Omega)$")
    ax.set_ylabel(r"$V_\mathrm{osc}~(\Omega)$")
    fig, ax = plt.subplots()
    img = ax.imshow(grid_ga_max/grid_g2_max**2,
                     extent=extent,
                     cmap=plt.cm.viridis,
                     origin="lower")
    fig.colorbar(img, ax=ax)
    ax.set_xlabel(r"$V_\mathrm{avg}~(\Omega)$")
    ax.set_ylabel(r"$V_\mathrm{osc}~(\Omega)$")
    plt.show()


def plot_RG2_commutator(
        dm,
        omega = 16.5372,
        method = "mu",
        **parameters):
    """
    Relevance of commutator of resolvent (R or Π) and G².
    Plot largest matrix element of this commutator as function of Vdc
    and Vac for fixed Ω.
    """
    parameters["include_Ga"] = True
    parameters["solve_integral_exactly"] = True
    parameters["integral_method"] = -1
    vdc = []
    vac = []
    g2_max = []
    pi_max = []
    commutator00_max = []
    commutator01_max = []
    for kondo in dm.load_all(omega=omega, method=method, **parameters):
        try:
            vdc.append(kondo.vdc)
            vac.append(kondo.vac)
            nmax = kondo.nmax
            g2 = kondo.g2
            gamma = kondo.gamma
            z = kondo.z
            if gamma.ndim == 3:
                vb = kondo.voltage_branches
                gamma = gamma[vb]
                z = z[vb]
                g2 = g2[:,:,vb]
            elif gamma.ndim != 2:
                raise ValueError("Invalid shape: %s"%gamma.shape)
            g2_max.append(np.abs(g2).max())
            chi = 1j*gamma
            chi[np.diag_indices(2*nmax+1)] += omega * np.arange(-nmax, nmax+1)
            chi = z @ chi
            pi = np.linalg.inv(chi)
            pi_max.append(np.abs(pi).max())
            g2[0,0] = z @ g2[0,0]
            g2[0,1] = z @ g2[0,1]
            com00 = pi @ g2[0,0] - g2[0,0] @ pi
            com01 = pi @ g2[0,1] - g2[0,1] @ pi
            commutator00_max.append(np.abs(com00).max())
            commutator01_max.append(np.abs(com01).max())
        except:
            settings.logger.exception("Error while reading data:")
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(nrows=2, ncols=2, sharex=True, sharey=True)
    vdc = np.array(vdc)
    vac = np.array(vac)
    g2_max = np.array(g2_max)
    pi_max = np.array(pi_max)
    commutator00_max = np.array(commutator00_max)
    commutator01_max = np.array(commutator01_max)
    s1 = ax1.scatter(vdc, vac, c=commutator00_max)
    s2 = ax2.scatter(vdc, vac, c=commutator01_max)
    s3 = ax3.scatter(vdc, vac, c=commutator00_max/g2_max)
    s4 = ax4.scatter(vdc, vac, c=commutator01_max/g2_max)

    fig.colorbar(s1, ax=ax1)
    fig.colorbar(s2, ax=ax2)
    fig.colorbar(s3, ax=ax3)
    fig.colorbar(s4, ax=ax4)

    fig, ax = plt.subplots()
    vdc_mesh = np.linspace(0, 165.372, 201)
    vac_mesh = np.linspace(0, 165.372, 201)
    extent = (1.5*vdc_mesh[0] - 0.5*vdc_mesh[1], 1.5*vdc_mesh[-1] - 0.5*vdc_mesh[-2], 1.5*vac_mesh[0] - 0.5*vac_mesh[1], 1.5*vac_mesh[-1] - 0.5*vac_mesh[-2])
    extent = tuple(v/omega for v in extent)
    vdc_mesh, vac_mesh = np.meshgrid(vdc_mesh, vac_mesh)
    interpolated = griddata((vdc, vac), commutator01_max/g2_max, (vdc_mesh, vac_mesh), method="cubic")
    interp_max = interpolated.max()
    print("Maximum of color scale:", interp_max)
    norm = plt.Normalize(0, interp_max)
    img = ax.imshow(interpolated, extent=extent, norm=norm, origin="lower")
    imwrite(f"figdata/colorbar_viridis.png", np.array(0xffff*plt.cm.viridis(np.linspace(0, 1, 0x1000))[::-1,:3], dtype=np.uint16).reshape((0x1000,1,3)), format="PNG-FI")
    imwrite(f"figdata/commutator_RG2_relative.png", np.array(0xffff*plt.cm.viridis(interpolated[::-1]/interp_max)[...,:3], dtype=np.uint16), format="PNG-FI")
    s = ax.scatter(vdc/omega, vac/omega, c=commutator01_max/g2_max, norm=norm)
    fig.colorbar(img, ax=ax)
    ax.set_xlabel(r"$V_\mathrm{avg}~(\Omega)$")
    ax.set_ylabel(r"$V_\mathrm{osc}~(\Omega)$")
    plt.show()


def matrix_truncation(
        dm,
        **trashparams
        ):
    omega = 16.5371763
    vdc = 19.84461156
    vac = 29.76691734
    vb = 4
    params = dict(d=1e9, solver_tol_rel=1e-8, solver_tol_abs=1e-10, voltage_branches=vb, omega=omega, vdc=vdc, vac=vac)
    kondos = {}
    g200_matrices = {}
    g201_matrices = {}
    z_matrices = {}
    gamma_matrices = {}
    deltaGammaL_matrices = {}
    nmax_ref = 19
    for kondo in dm.load_all(**params):
        nmax = kondo.nmax
        idx = (kondo.method, kondo.nmax > 20, kondo.padding>0)
        if nmax > nmax_ref:
            z_matrices[idx] = kondo.z[vb, nmax-nmax_ref:nmax_ref-nmax, nmax-nmax_ref:nmax_ref-nmax]
            gamma_matrices[idx] = kondo.gamma[vb, nmax-nmax_ref:nmax_ref-nmax, nmax-nmax_ref:nmax_ref-nmax]
            deltaGammaL_matrices[idx] = kondo.deltaGammaL[nmax-nmax_ref:nmax_ref-nmax, nmax-nmax_ref:nmax_ref-nmax]
            g200_matrices[idx] = kondo.g2[0,0,vb, nmax-nmax_ref:nmax_ref-nmax, nmax-nmax_ref:nmax_ref-nmax]
            g201_matrices[idx] = kondo.g2[0,1,vb, nmax-nmax_ref:nmax_ref-nmax, nmax-nmax_ref:nmax_ref-nmax]
        elif nmax == nmax_ref:
            z_matrices[idx] = kondo.z[vb]
            gamma_matrices[idx] = kondo.gamma[vb]
            deltaGammaL_matrices[idx] = kondo.deltaGammaL
            g200_matrices[idx] = kondo.g2[0,0,vb]
            g201_matrices[idx] = kondo.g2[0,1,vb]
        else:
            raise ValueError(f"nmax={nmax} should not be smaller than nmax_ref={nmax_ref}")
        kondos[idx] = kondo

    fig, axes = plt.subplots(nrows=2, ncols=4, sharex=True, sharey=True)
    norm = LogNorm(1e-9, 1)
    cmap = plt.cm.viridis
    #z_ref = (z_matrices[("mu", True, False)] + z_matrices[("J", True, True)])/2
    z_ref_j = z_matrices[("J", True, True)]
    z_ref_mu = z_matrices[("mu", True, False)]
    axes[0,0].set_title(r"$\hat{\bar\mu}_{LR}$")
    axes[0,1].set_title(r"$\hat{\bar\mu}_{LR}$, extrap.")
    axes[0,2].set_title(r"$\hat{J}^{(0)}_{LR}$")
    axes[0,3].set_title(r"$\hat{J}^{(0)}_{LR}$, extrap.")
    img = axes[0,0].imshow(np.abs(z_matrices[("mu", False, False)]), norm=norm, cmap=cmap)
    axes[0,1].imshow(np.abs(z_matrices[("mu", False, True)]), norm=norm, cmap=cmap)
    axes[0,2].imshow(np.abs(z_matrices[("J", False, False)]), norm=norm, cmap=cmap)
    axes[0,3].imshow(np.abs(z_matrices[("J", False, True)]), norm=norm, cmap=cmap)
    axes[1,0].imshow(np.abs(z_matrices[("mu", False, False)]-z_ref_mu), norm=norm, cmap=cmap)
    axes[1,1].imshow(np.abs(z_matrices[("mu", False, True)]-z_ref_mu), norm=norm, cmap=cmap)
    axes[1,2].imshow(np.abs(z_matrices[("J", False, False)]-z_ref_j), norm=norm, cmap=cmap)
    axes[1,3].imshow(np.abs(z_matrices[("J", False, True)]-z_ref_j), norm=norm, cmap=cmap)
    #axes[2,0].imshow(np.abs(z_matrices[("mu", True, False)] - z_matrices[("J", True, True)]), norm=norm, cmap=cmap)
    fig.colorbar(img, ax=axes)
    for ax in axes.flat:
        ax.xaxis.set_visible(False)
        ax.yaxis.set_visible(False)

    fig, axes = plt.subplots(nrows=2, ncols=4, sharex=True, sharey=True)
    g201_ref_j = g201_matrices[("J", True, True)]
    g201_ref_mu = g201_matrices[("mu", True, False)]
    axes[0,0].set_title(r"$\hat{\bar\mu}_{LR}$")
    axes[0,1].set_title(r"$\hat{\bar\mu}_{LR}$, extrap.")
    axes[0,2].set_title(r"$\hat{J}^{(0)}_{LR}$")
    axes[0,3].set_title(r"$\hat{J}^{(0)}_{LR}$, extrap.")
    img = axes[0,0].imshow(np.abs(g201_matrices[("mu", False, False)]), norm=norm, cmap=cmap)
    axes[0,1].imshow(np.abs(g201_matrices[("mu", False, True)]), norm=norm, cmap=cmap)
    axes[0,2].imshow(np.abs(g201_matrices[("J", False, False)]), norm=norm, cmap=cmap)
    axes[0,3].imshow(np.abs(g201_matrices[("J", False, True)]), norm=norm, cmap=cmap)
    axes[1,0].imshow(np.abs(g201_matrices[("mu", False, False)]-g201_ref_mu), norm=norm, cmap=cmap)
    axes[1,1].imshow(np.abs(g201_matrices[("mu", False, True)]-g201_ref_mu), norm=norm, cmap=cmap)
    axes[1,2].imshow(np.abs(g201_matrices[("J", False, False)]-g201_ref_j), norm=norm, cmap=cmap)
    axes[1,3].imshow(np.abs(g201_matrices[("J", False, True)]-g201_ref_j), norm=norm, cmap=cmap)
    fig.colorbar(img, ax=axes)
    for ax in axes.flat:
        ax.xaxis.set_visible(False)
        ax.yaxis.set_visible(False)
    plt.show()


def asymmetric_coupling(dm, show_gpat=True, **parameters):
    assert isinstance(parameters["omega"], Number)
    assert isinstance(parameters["vac"], Number) or isinstance(parameters["fourier_coef"][0], Number)
    parameters.pop("xL", None)
    parameters.pop("vdc", None)
    parameters.pop("nmax", None)
    data = dm.list(**parameters).sort_values(["vdc", "xL"])
    fig, ax = plt.subplots()
    xL = np.unique(data.xL)[::-1]
    if 0.5 in data.xL:
        ax.plot(data[data.xL==0.5].vdc, np.pi*data[data.xL==0.5].dc_conductance, label=f"xL=0.5", linewidth=2, zorder=1, color="black")
        xL = xL[1:]
    for x in xL:
        ax.plot(data[data.xL==x].vdc, np.pi*data[data.xL==x].dc_conductance/(4*x*(1-x)), label=f"xL={x:.3}", zorder=x)
    ax.legend(loc=0)
    if show_gpat:
        if isinstance(parameters["vac"], Number):
            #vac = parameters.pop("vac")
            #parameters.pop("fourier_coef")
            #gpat = gen_photon_assisted_tunneling(dm, vdc=data.vdc, fourier_coef=(vac/2,0), **parameters, xL=0.001)
            gpat = photon_assisted_tunneling(dm, vdc=data.vdc, **parameters, xL=0.001)
        else:
            gpat = gen_photon_assisted_tunneling(dm, vdc=data.vdc, **parameters, xL=0.001)
        ax.plot(data.vdc, np.pi*gpat/0.003996, '-g')
    plt.show()


def compare_limits(
        dm,
        omega = 16.5372,
        omega_vdc = 16.5372,
        omega_vac = 16.5372,
        vac = 16.5372,
        vac_vdc = 165.372,
        vdc = 82.686,
        **parameters,
        ):
    if omega is None:
        omega = 16.5372
    if vac is None:
        vac = 16.5372
    if vdc is None:
        vdc = 82.686
    parameters.pop("method", None)
    parameters.pop("voltage_branches", None)
    fig, ((ax_omega, ax_vac), (ax_vdc, ax_vac_vdc)) = plt.subplots(2, 2, sharey="row")
    ax_omega.set_ylabel("G (2e²/h)")
    ax_vdc.set_ylabel("G (2e²/h)")
    ax_omega.set_xlabel("Ω (Tk)")
    ax_vac.set_xlabel("Vosc (Tk)")
    ax_vac_vdc.set_xlabel("Vosc (Tk)")
    ax_vdc.set_xlabel("Vavg (Tk)")
    ax_omega.set_title(f"Vavg=0, Vosc={vac/TK_VOLTAGE:.4g}Tk")
    ax_vac.set_title(f"Vavg=0, Ω={omega_vac/TK_VOLTAGE:.4g}Tk")
    ax_vdc.set_title(f"Vosc={vac_vdc/TK_VOLTAGE:.4g}, Ω={omega_vdc/TK_VOLTAGE:.4g}Tk")
    ax_vac_vdc.set_title(f"Vavg={vdc/TK_VOLTAGE:.4g}, Ω={omega_vac/TK_VOLTAGE:.4g}Tk")

    data_omega = dm.list(vac=vac, vdc=0.0, method="J", voltage_branches=0, **parameters).sort_values("omega")
    data_vac = dm.list(omega=omega, vdc=0.0, method="J", voltage_branches=0, **parameters).sort_values("vac")
    data_vdc = dm.list(vac=vac_vdc, omega=omega_vdc, method="mu", voltage_branches=4, **parameters).sort_values("vdc")
    data_vac_vdc = dm.list(vdc=vdc, omega=omega_vac, **parameters).sort_values("vac")
    #print(data_omega.shape, data_vac.shape, data_vdc.shape, data_vac_vdc.shape)
    interp = interp_vac0(dm, voltage_branches=4, method="mu", **parameters)

    def adiabatic(vac, vdc):
        if np.all(vdc == 0):
            result, error = quad_vec((lambda x: interp(vac*np.sin(x))), 0, np.pi/2, epsrel=1e-7, epsabs=1e-14)
            result *= 2
            error *= 2
            assert error < 1e-6
            return result
        result, error = quad_vec((lambda x: interp(vdc+vac*np.sin(x))), -np.pi/2, np.pi/2, epsrel=1e-7, epsabs=1e-14)
        assert error < 1e-6
        return result

    def kaminski_high_vac(vac):
        """
        Comparison to Kaminski et al PRB 62.8154 (2000)
        Low frequency (adiabatic) limit
        Eq. (77) in Kaminski et al 2000
        """
        g = 3*np.pi**2/(16*np.log(vac/TK_VOLTAGE)**2)
        if isinstance(g, Number):
            return g
        g[vac <= TK_VOLTAGE] = np.nan
        g[g > 1.2] = np.nan
        return g

    def kaminski_high_omega(vac, omega):
        """
        Comparison to Kaminski et al PRB 62.8154 (2000)
        High frequency limit
        Eqs. (72) - (74) in Kaminski et al 2000
        """
        # Eq. (72) in Kaminski et al 2000
        decoherence_rate = (vac/TK_VOLTAGE)**2 / (np.pi*omega/TK_VOLTAGE*np.log(omega/TK_VOLTAGE)**2)
        # Eq. (74) in Kaminski et al 2000
        g = 1 - decoherence_rate
        # Eq. (73) in Kaminski et al 2000
        g[decoherence_rate>1] = 3*np.pi**2/(16*np.log(decoherence_rate[decoherence_rate>1])**2)
        return g

    def kaminski_low_energy(vac):
        """
        Comparison to Kaminski et al PRB 62.8154 (2000)
        low energies, near equilibrium
        Eq. (76) in Kaminski et al 2000
        """
        return 1 - 3/16 * (vac/TK_VOLTAGE)**2

    frtrg_style = dict(color="C3", zorder=5, linewidth=2)
    adiabatic_style = dict(color="C0", zorder=4)
    kaminski_style = dict(color="C2", zorder=3)
    pat_style = dict(color="C1", zorder=2)

    ax_omega.plot(data_omega.omega/TK_VOLTAGE, np.pi*data_omega.dc_conductance, **frtrg_style)
    ax_omega.hlines(adiabatic(vac, 0), 0, vac/TK_VOLTAGE, **adiabatic_style)
    ax_omega.hlines(kaminski_high_vac(vac), 0, vac/TK_VOLTAGE, **kaminski_style)
    ax_omega.plot(data_omega.omega[data_omega.omega>=vac]/TK_VOLTAGE,
            kaminski_high_omega(vac, data_omega.omega[data_omega.omega>=vac]),
            **kaminski_style)
    ax_omega.plot(data_omega.omega/TK_VOLTAGE,
            np.pi*photon_assisted_tunneling(dm, data_omega.omega, 0, vac),
            **pat_style)

    ax_vac.plot(data_vac.vac/TK_VOLTAGE, np.pi*data_vac.dc_conductance, **frtrg_style)
    ax_vac.plot(data_vac.vac/TK_VOLTAGE, adiabatic(data_vac.vac, 0), **adiabatic_style)
    ax_vac.plot(data_vac.vac[data_vac.vac>omega_vac]/TK_VOLTAGE,
                kaminski_high_vac(data_vac.vac[data_vac.vac>omega_vac]),
                **kaminski_style)
    ax_vac.plot(data_vac.vac[data_vac.vac<omega_vac]/TK_VOLTAGE,
                kaminski_high_omega(data_vac.vac[data_vac.vac<omega_vac], omega_vac),
                **kaminski_style)
    ax_vac.plot(data_vac.vac/TK_VOLTAGE,
            np.pi*photon_assisted_tunneling(dm, omega_vac, 0, data_vac.vac),
            **pat_style)

    ax_vdc.plot(data_vdc.vdc/TK_VOLTAGE, np.pi*data_vdc.dc_conductance, **frtrg_style)
    ax_vdc.plot(data_vdc.vdc/TK_VOLTAGE, adiabatic(vac_vdc, data_vdc.vdc), **adiabatic_style)
    ax_vdc.plot(data_vdc.vdc/TK_VOLTAGE, np.pi*photon_assisted_tunneling(dm, omega_vdc, data_vdc.vdc, vac_vdc), **pat_style)

    ax_vac_vdc.plot(data_vac_vdc.vac/TK_VOLTAGE, np.pi*data_vac_vdc.dc_conductance, **frtrg_style)
    ax_vac_vdc.plot(data_vac_vdc.vac/TK_VOLTAGE, adiabatic(data_vac_vdc.vac, vdc), **adiabatic_style)
    ax_vac_vdc.plot(data_vac_vdc.vac/TK_VOLTAGE, np.pi*photon_assisted_tunneling(dm, omega_vac, vdc, data_vac_vdc.vac), **pat_style)
    plt.show()


def overview_3d_contours(dm, show_contours=False, show_surfaces=True, save_images=True, offset=0, **trash):
    import mpl_toolkits.mplot3d
    data_vdc0 = np.load("figdata/vdc0_interp.npz")
    data_omega5 = np.load("figdata/omega5_interp.npz")
    interp = interp_vac0(dm, method="mu", d=1e9, include_Ga=True, voltage_branches=4, solver_tol_rel=1e-8, solver_tol_abs=1e-10, integral_method=-15)

    fill_levels = 1.1/np.linspace(1, 6.12, 256)[::-1] - 0.1
    colors = plt.cm.viridis(np.linspace(0, 1, 256))
    levels = 1/np.arange(1, 50, 0.5)[::-1]
    real_cmap = lambda x: viridis(1 - (1.1/(x+0.1)-1)/5.12)
    omega_max = 16.5372
    vdc = data_omega5["vdc"][0]
    #vdc_resolution = data_omega5["vdc"].shape[1]
    omega = data_vdc0["omega"][0]
    #omega_resolution = data_vdc0["omega"].shape[1]
    vac = data_omega5["vac"][:,0]
    #assert np.allclose(vac, omega_max*data_vdc0["vac_omega"][:,0])

    data = dm.list(omega=16.5372, good_flags=0x1000, bad_flags=0x2c8c, voltage_branches=4, method="mu", d=1e9, solver_tol_rel=1e-8, solver_tol_abs=1e-10, padding=0, xL=0.5)
    #data = data.loc[(data.vdc < 165.38) & (data.vac < 165.38)]
    x_arr, y_arr = np.meshgrid(np.linspace(-165.372, 165.372, 601), np.linspace(0, 330.744, 601))
    gdc_omega5_img_arr = np.pi*griddata(
            (data.vdc-data.vac, data.vdc+data.vac),
            data.dc_conductance,
            (x_arr, y_arr),
            method="cubic")
    array = np.array(0xffff*real_cmap(gdc_omega5_img_arr[::-1]), dtype=np.uint16)
    imwrite(f"figdata/overview_z_rotated.png", array, format="PNG-FI")

    fig = plt.figure()
    ax = fig.add_subplot(projection="3d")
    #ax.view_init(elev=-90, azim=90)
    ax.view_init(elev=45, azim=225)

    cmap_ax = fig.add_axes((0.95, 0.1, 0.025, 0.8))
    min_m = fill_levels[0]
    cmap_ax.contourf(np.array([0,1]), np.array([min_m,1]), np.array([[min_m,min_m],[1,1]]), levels=fill_levels, colors=colors)
    cmap_ax.xaxis.set_visible(False)
    cmap_ax.set_xlim(0, 1)
    cmap_ax.set_ylim(min_m, 1)

    vdc0_y = data_vdc0["vac_omega"]
    vdc0_x = np.zeros_like(vdc0_y)
    vdc0_z = data_vdc0["omega"]
    vdc0_m = np.pi*data_vdc0["gdc_J_o3a_p"]

    if show_surfaces:
        ax.contourf(vdc0_m, vdc0_y, vdc0_z/TK_VOLTAGE, zdir="x", levels=fill_levels, offset=0, zorder=-10, colors=colors)
    if show_contours:
        contour = ax.contour(vdc0_m, vdc0_y, vdc0_z/TK_VOLTAGE, zdir="x", levels=levels, offset=-offset, zorder=10, colors="black")
        save_contour_coordinates(contour, "figdata/overview_contour_x.dat", "vdc", "omega", "gdc")
    if save_images:
        array = np.array(0xffff*real_cmap(vdc0_m.T[::-1,::-1]), dtype=np.uint16)
        imwrite(f"figdata/overview_x.png", array, format="PNG-FI")
        array_new = np.empty((array.shape[1], array.shape[0]+array.shape[1], 4), dtype=np.uint16)
        array_new[:,array.shape[0]:].fill(0xffff)
        array_new[:,array.shape[0]:,3].fill(0)
        array_new[:,:array.shape[0]] = array.transpose((1,0,2))
        sheared_array = array_new.flat[:array_new.size - 4*array_new.shape[0]].reshape(array.shape[1], array.shape[0]+array.shape[1]-1, 4).transpose((1,0,2))
        imwrite(f"figdata/overview_x_sheared.png", sheared_array, format="PNG-FI")
        #array_new = np.empty((array.shape[0], array.shape[0]+array.shape[1], 4), dtype=np.uint16)
        #array_new[:,array.shape[1]:].fill(0xffff)
        #array_new[:,:array.shape[1]] = array
        #sheared_array = array_new.flat[:array_new.size - 4*array_new.shape[0]].reshape(array.shape[0], array.shape[0]+array.shape[1]-1, 4)
        #imwrite(f"figdata/overview_x_sheared.png", sheared_array, format="PNG-FI")

    omega5 = data_omega5["omega"]
    omega5_x = data_omega5["vdc"]/omega5
    omega5_y = data_omega5["vac"]/omega5
    omega5_m = np.pi*data_omega5["gdc_mu_o3a"]
    if show_surfaces:
        ax.contourf(omega5_x, omega5_y, omega5_m, zdir="z", levels=fill_levels, offset=omega5/TK_VOLTAGE, colors=colors)
    if show_contours:
        contour = ax.contour(omega5_x, omega5_y, omega5_m, zdir="z", levels=levels, offset=omega5/TK_VOLTAGE+offset, colors="black")
        save_contour_coordinates(contour, "figdata/overview_contour_z.dat", "vac", "vdc", "gdc")
    if save_images:
        imwrite(f"figdata/overview_z.png", np.array(0xffff*real_cmap(omega5_m[::-1]), dtype=np.uint16), format="PNG-FI")

    vac0_x = vdc/omega_max
    vac0_z = omega
    vac0_vdc = vac0_x.reshape((-1,1)) * vac0_z.reshape((1,-1))
    vac0_m = np.pi*interp(vac0_vdc)
    if show_surfaces:
        ax.contourf(vac0_x, vac0_m, vac0_z/TK_VOLTAGE, zdir="y", levels=fill_levels, offset=0, zorder=-10, colors=colors)
    if show_contours:
        contour = ax.contour(vac0_x, vac0_m, vac0_z/TK_VOLTAGE, zdir="y", levels=levels, offset=-offset, zorder=10, colors="black")
        save_contour_coordinates(contour, "figdata/overview_contour_y.dat", "omega", "vac", "gdc")
    if save_images:
        array = np.array(0xffff*real_cmap(vac0_m.T[::-1]), dtype=np.uint16)
        imwrite(f"figdata/overview_y.png", array, format="PNG-FI")
        array_new = np.empty((array.shape[1], array.shape[0]+array.shape[1], 4), dtype=np.uint16)
        array_new[:,array.shape[0]:].fill(0xffff)
        array_new[:,array.shape[0]:,3].fill(0)
        array_new[:,:array.shape[0]] = array[::-1].transpose((1,0,2))
        sheared_array = array_new.flat[:array_new.size - 4*array_new.shape[0]].reshape(array.shape[1], array.shape[0]+array.shape[1]-1, 4).transpose((1,0,2))[::-1]
        imwrite(f"figdata/overview_y_sheared.png", sheared_array, format="PNG-FI")

    #ax.plot_surface(vdc0_x, vdc0_y, vdc0_z, rstride=1, cstride=1, facecolors=plt.cm.viridis(vdc0_m), shade=False, zorder=-10)
    #ax.set_axis_off()
    #fig.set_size_inches(14/2.56, 12/2.56)
    #fig.savefig("/tmp/figure.png", dpi=512, transparent=True)
    #ax.set_axis_on()
    plt.show()


def RGeq_comparison_new(
        dm = None,
        filename = "figdata/omega5_interp.npz",
        method = "mu",
        compare = "o3a_ispline",
        reference = "o3a",
        reference_method = "mu",
        #ref_filename = "figdata/omega5_interp_vb7.npz",
        ref_filename = "figdata/omega5_interp.npz",
        observable = "gdc",
        prefactor = np.pi,
        **trashparams):
    """
    Compare two different results for the differential conductance
    (or another observable) side-by-side and by plotting their absolute
    and relative difference.
    Used to generate some draft figures for the thesis.
    """
    fig, ((ax_dc, ax_trash), (ax_img, ax_ac)) = plt.subplots(nrows=2, ncols=2, sharex="col", sharey="row")
    data = np.load(filename)
    cmp = prefactor*data[f"{observable}_{method}_{compare}"]
    if ref_filename is None:
        ref = prefactor*data[f"{observable}_{reference_method}_{reference}"]
    else:
        ref_data = np.load(ref_filename)
        ref = prefactor*ref_data[f"{observable}_{reference_method}_{reference}"]
    #gdc_o3 = np.pi*data["gdc_mu_o3"]
    #norm = LogNorm(0.08, 0.4)

    omega = 16.5372
    vdc = data["vdc"][0] / omega
    vac = data["vac"][:,0] / omega

    #dc_indices = np.array([0, 8, 20, 30, 90, 165])
    #ac_indices = np.array([30, 50, 80, 125, 170])
    dc_indices = np.array([0, 9, 21, 30, 90, 165])
    ac_indices = np.array([32, 52, 80, 124, 172])
    separation = 0.12

    vmax = max(vdc[dc_indices[-1]], vac[ac_indices[-1]])

    #skip = 40
    #plot_number_dc = data["vdc"].shape[0] // skip + 1
    #plot_number_ac = data["vac"].shape[1] // skip + 1
    #for i in range(plot_number_dc):
    #    color = plt.cm.viridis(data["vac"][i*skip,0]/(omega*vac_max))
    #    ax_dc.plot(data["vdc"][i*skip]/omega, cmp[i*skip], color=color)
    #    ax_dc.plot(data["vdc"][i*skip]/omega, ref[i*skip], color=color, linestyle=":")
    #for i in range(plot_number_ac):
    #    color = plt.cm.viridis(data["vdc"][0,i*skip]/(omega*vdc_max))
    #    ax_ac.plot(cmp[:,i*skip], data["vac"][:,i*skip]/omega, color=color)
    #    ax_ac.plot(ref[:,i*skip], data["vac"][:,i*skip]/omega, color=color, linestyle=":")

    #ax_dc.plot(data["vdc"][::skip].T/omega, cmp[::skip].T, color="blue")
    #ax_dc.plot(data["vdc"][::skip].T/omega, ref[::skip].T, ":", color="red")
    #ax_ac.plot(cmp[:,::skip], data["vac"][:,::skip]/omega, color="blue")
    #ax_ac.plot(ref[:,::skip], data["vac"][:,::skip]/omega, color="red")

    extent = (1.5*vdc[0] - 0.5*vdc[1], 1.5*vdc[-1] - 0.5*vdc[-2], 1.5*vac[0] - 0.5*vac[1], 1.5*vac[-1] - 0.5*vac[-2])
    ccmap = plt.cm.seismic
    absdiff = cmp - ref
    absdiff_max = np.abs(absdiff).max()
    reldiff = absdiff/ref
    reldiff_max = np.abs(reldiff).max()
    img = ax_img.imshow(reldiff, cmap=ccmap, norm=plt.Normalize(-reldiff_max, reldiff_max), extent=extent, origin="lower")
    #img4 = ax2.imshow(absdiff, cmap=ccmap, norm=plt.Normalize(-absdiff_max, absdiff_max), extent=extent2, origin="lower")
    fig.colorbar(img, ax=ax_trash, location="right")

    #ax_img.hlines(vac[ac_indices], 0, 1, transform=ax_img.get_yaxis_transform(), color="black", linewidth=0.5)
    #ax_img.vlines(vdc[dc_indices], 0, 1, transform=ax_img.get_xaxis_transform(), color="black", linewidth=0.5)
    for n, i in enumerate(ac_indices[::-1]):
        color = plt.cm.viridis(vac[i]/vmax)
        ax_dc.plot(data["vdc"][i]/omega, n*separation + cmp[i], color=color)
        ax_dc.plot(data["vdc"][i]/omega, n*separation + ref[i], color=color, linestyle=":")
        ax_img.hlines(vac[i], 0, 1, color=color, transform=ax_img.get_yaxis_transform(), linewidth=0.5)
    for n, i in enumerate(dc_indices[::-1]):
        color = plt.cm.viridis(vdc[i]/vmax)
        ax_ac.plot(n*separation + cmp[:,i], data["vac"][:,i]/omega, color=color)
        ax_ac.plot(n*separation + ref[:,i], data["vac"][:,i]/omega, color=color, linestyle=":")
        ax_img.vlines(vdc[i], 0, 1, color=color, transform=ax_img.get_xaxis_transform(), linewidth=0.5)

    ax_img.set_xlabel(r"$V_\mathrm{avg}~(\Omega)$")
    ax_img.set_ylabel(r"$V_\mathrm{osc}~(\Omega)$")
    ax_ac.set_xlabel(r"$G~(2e^2/h)$")
    ax_dc.set_ylabel(r"$G~(2e^2/h)$")
    plt.show()


def kondo_temperature(dm, **parameters):
    parameters["omega"] = 0
    parameters["vac"] = 0
    data = dm.list(**parameters).sort_values("vdc")
    plt.plot(data.vdc, np.pi*data.dc_conductance)
    vdc_max_fit = 0.15
    selection = data.vdc < vdc_max_fit
    fit_func = lambda x, a, b: b*(1 - a*x**2)
    (cv, prefactor), covar = curve_fit(fit_func, data.vdc[selection], data.dc_conductance[selection], (0.2, 1/np.pi))
    plt.plot(data.vdc[selection], np.pi*fit_func(data.vdc[selection], cv, prefactor))
    plt.plot(data.vdc, np.pi*prefactor/(1 + cv*data.vdc**2))
    #print(cv, prefactor*np.pi, covar[0,0]**0.5, covar[1,1]**0.5, covar)
    print(f"G/G0 = 1 - ({cv:.5g} ± {covar[0,0]**0.5:.3g}) (Vdc/Tkrg)^2  for small Vdc")

    selection = data.vdc < 4
    fit_func = lambda x, a, b, c: c + b/(1 + a*x**2)
    (cv, prefactor, shift), covar = curve_fit(fit_func, data.vdc[selection], data.dc_conductance[selection], (0.2, 1/np.pi, 0.1))
    plt.plot(data.vdc[selection], np.pi*fit_func(data.vdc[selection], cv, prefactor, shift))
    print(f"G/G0 = {np.pi*shift:.5g} + {np.pi*prefactor:.5g}/(1 + {cv:.5g} (Vdc/Tkrg)^2)")


def compare_resonant(dm, **parameters):
    if parameters.get("omega", None) is None:
        parameters["omega"] = 16.5372
    parameters.pop("voltage_branches", None)
    parameters.pop("resonant_dc_shift", None)
    data = dm.list(**parameters)
    data = data.loc[(((data.vdc + 1e-4)%data.omega) < 2e-4) & (data.vdc > 0)]
    data.omega = np.round(data.omega, 8)
    data.vdc = np.round(data.vdc, 8)
    data.vac = np.round(data.vac, 8)
    data_resonant = data.loc[(data.method == "J") & (data.padding >= (data.nmax>0)) & (data.resonant_dc_shift > 0) & (data.voltage_branches == 0)]
    data_ref = data.loc[(data.method == "mu") & (data.padding == 0) & (data.resonant_dc_shift == 0) & (data.voltage_branches == 4)]
    merged = pd.merge(
            data_resonant,
            data_ref,
            how="inner",
            on=["vdc","vac","d","omega","energy_im","energy_re","solver_tol_rel","solver_tol_abs","xL","lazy_inverse_factor"],
            suffixes=("_res", "_ref"))
    print(data_resonant.shape, data_ref.shape, merged.shape)
    diff_rel = (merged.dc_conductance_res - merged.dc_conductance_ref)/merged.dc_conductance_res
    diff_rel_max = np.nanmax(np.abs(diff_rel[merged.vac > 0]))
    norm = plt.Normalize(-diff_rel_max, diff_rel_max)
    s = plt.scatter(merged.vdc, merged.vac, c=diff_rel, cmap=plt.cm.seismic, norm=norm)
    plt.colorbar(s)
    plt.show()


def compare_current_initial_conditions(dm, **parameters):
    data = dm.list(**parameters)
    global_bad_flags = DataManager.SOLVER_FLAGS["simplified_initial_conditions"] \
            | DataManager.SOLVER_FLAGS["extrapolate_voltage"] \
            | DataManager.SOLVER_FLAGS["deleted"]
    data = data.loc[data.solver_flags & global_bad_flags == 0]
    data = data.sort_values(["vdc", "vac"])
    data.vdc = np.round(data.vdc, 8)
    data.vac = np.round(data.vac, 8)
    cmp_good_flags = DataManager.SOLVER_FLAGS["include_Ga"]
    cmp_bad_flags = DataManager.SOLVER_FLAGS["improved_initial_conditions"] | DataManager.SOLVER_FLAGS["simplified_initial_conditions"]
    ref_good_flags = DataManager.SOLVER_FLAGS["improved_initial_conditions"] | DataManager.SOLVER_FLAGS["include_Ga"]
    ref_bad_flags = DataManager.SOLVER_FLAGS["simplified_initial_conditions"]
    cmp_flags = cmp_good_flags | cmp_bad_flags
    ref_flags = ref_good_flags | ref_bad_flags

    ref_data = data.loc[data.solver_flags & ref_flags == ref_good_flags]
    cmp_data = data.loc[data.solver_flags & cmp_flags == cmp_good_flags]
    merged = pd.merge(
            data,
            ref_data,
            how="inner",
            on=["vdc","vac","d","omega","energy_im","energy_re","solver_tol_rel","solver_tol_abs","xL","lazy_inverse_factor","resonant_dc_shift"],
            suffixes=("", "_ref"))

    s = plt.scatter(merged.vdc, merged.vac, c=(merged.dc_current_ref-merged.dc_current)/merged.dc_current_ref, cmap=plt.cm.seismic)
    plt.colorbar(s)
    plt.show()



if __name__ == '__main__':
    main()
