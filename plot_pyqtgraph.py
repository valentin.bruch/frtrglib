#!/usr/bin/env python

# Copyright 2022 Valentin Bruch <valentin.bruch@rwth-aachen.de>
# License: MIT
"""
Kondo FRTRG, generate interactive plots using PyQtGraph
"""

import numpy as np
import argparse
from pyqtgraph.Qt import QtGui
from pyqtgraph.Qt.QtWidgets import QApplication
from pyqtgraph import makeQImage
import pyqtgraph.opengl as gl
from matplotlib import colormaps as cm
import pandas as pd
from scipy.optimize import curve_fit
import settings
from data_management import DataManager
from final_plots import photon_assisted_tunneling
from pyqtgraph_utils import *

#from OpenGL import GL
#GL.glEnable(GL.GL_DEPTH_TEST)
#GL.glShadeModel(GL.GL_SMOOTH)
#GL.glHint(GL.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST)

def full_overview(dm,
        grid = True,
        size = 0.25,
        xyscale = "linear",
        zscale = "log",
        scale = None,
        gl_preset = "translucent",
        plot_value = "G",
        cmap = "viridis",
        show_gpat = False,
        units = "tkrg",
        **parameters):
    """
    Show all data for conductance.
    """
    if size is None:
        size = 0.25
    if show_gpat:
        raise ValueError
    assert plot_value in ("G", "gamma", "Iac", "Idc")
    data = dm.list(**parameters)
    data.sort_values("vac", inplace=True)
    app = QApplication([])
    w = gl.GLViewWidget()
    w.show()
    w.setWindowTitle("Kondo model: Overview")
    if grid:
        gx = gl.GLGridItem()
        gy = gl.GLGridItem()
        gz = gl.GLGridItem()
        gx.translate(10, 10, 1e-6)
        gy.translate(10, 10, 1e-6)
        gz.translate(10, 10, 1e-6)
        gx.rotate(-90, 0, 1, 0)
        gy.rotate(90, 1, 0, 0)
        w.addItem(gx)
        w.addItem(gy)
        w.addItem(gz)
    ax = gl.GLAxisItem(size=QtGui.QVector3D(50,50,50))
    w.addItem(ax)
    cmap = cm.get_cmap(cmap)
    # Vac = 0
    vac0_data = data.loc[data.vac == 0]
    vac0_data.sort_values("vdc", inplace=True)
    if plot_value == "G":
        values = np.pi*np.broadcast_to(vac0_data.dc_conductance.to_numpy(), (2, vac0_data.shape[0])).T
    elif plot_value == "Idc":
        values = np.broadcast_to(vac0_data.dc_current.to_numpy()/5, (2, vac0_data.shape[0])).T
    elif plot_value == "Iac":
        values = np.broadcast_to(vac0_data.ac_current_abs.to_numpy()/5, (2, vac0_data.shape[0])).T
    elif plot_value == "gamma":
        values = np.broadcast_to(vac0_data.gamma.to_numpy()/20, (2, vac0_data.shape[0])).T
    z = np.zeros((vac0_data.shape[0], 2))
    z[:,1] = 25

    # overview
    match units:
        case "omega":
            pos = np.array([data.vdc/data.omega, data.vac/data.omega, data.omega]).T
        case "tkrg":
            su = gl.GLSurfacePlotItem(x=vac0_data.vdc.to_numpy(), y=np.array([0,0]), z=z, colors=cmap(values))
            w.addItem(su)
            pos = np.array([data.vdc, data.vac, data.omega]).T
        case _:
            raise ValueError(f"Invalid value for units: {units}. Possible values are omega and tkrg.")

    if xyscale == "log":
        pos[:,0] = np.log10(pos[:,0])
        pos[:,1] = np.log10(pos[:,1])
    if zscale == "log":
        pos[:,2] = np.log10(pos[:,2])
    if plot_value == "G":
        values = np.pi*data.dc_conductance
    elif plot_value == "Idc":
        values = data.dc_current / 5
    elif plot_value == "Iac":
        values = data.ac_current_abs / 5
    elif plot_value == "gamma":
        values = data.gamma / 20
    sp = gl.GLScatterPlotItem(pos=pos, size=size, color=cmap(values), pxMode=False)
    sp.setGLOptions(gl_preset)
    w.addItem(sp)
    app.exec()


def fixed_parameter(dm,
        vac = None,
        vdc = None,
        omega = None,
        scale = 80*np.pi,
        size = None,
        grid = False,
        xyscale = "linear",
        zscale = "linear",
        gl_preset = "translucent",
        plot_value = "dc_conductance",
        mirror_vdc = True,
        cmap = "viridis",
        show_gpat = False,
        **parameters):
    """
    Show overview of all data where one physical parameter is fixed
    """
    if plot_value == "dc_current":
        mirror_vdc = False
    if omega is not None:
        parameter = "omega"
    elif vac is not None:
        parameter = "vac"
    elif vdc is not None:
        parameter = "vdc"
    data = dm.list(vac=vac, vdc=vdc, omega=omega, **parameters)
    if mirror_vdc and parameter != "vdc" and xyscale != "log":
        data_mirror = data.copy()
        data_mirror.vdc *= -1
        data = pd.concat((data, data_mirror))
        del data_mirror
    app = QApplication([])
    w = gl.GLViewWidget()
    w.show()
    w.setWindowTitle(f"Kondo model: fixed {parameter}")
    if grid:
        gx = gl.GLGridItem()
        gy = gl.GLGridItem()
        gz = gl.GLGridItem()
        gx.translate(10, 10, 1e-6)
        gy.translate(10, 10, 1e-6)
        gz.translate(10, 10, 1e-6)
        gx.rotate(-90, 0, 1, 0)
        gy.rotate(90, 1, 0, 0)
        w.addItem(gx)
        w.addItem(gy)
        w.addItem(gz)
    ax = gl.GLAxisItem(size=QtGui.QVector3D(100,100,100))
    w.addItem(ax)
    cmap = cm.get_cmap(cmap)
    # overview
    pos = np.array([
        *(getattr(data, name) \
                for name in ("vdc", "vac", "omega") \
                if name != parameter),
        data[plot_value]
        ]).T
    xL = parameters.get("xL", 0.5)
    scale *= 1/(4*xL*(1-xL))
    if xyscale == "log":
        pos[:,0] = np.log10(pos[:,0])
        pos[:,1] = np.log10(pos[:,1])
    if zscale == "log":
        pos[:,2] = np.log10(pos[:,2])
    if size is None:
        size = scale**0.5/20
    pos[:,2] *= scale
    color_data = data[plot_value].copy()
    color_data -= color_data.min()
    color_data /= color_data.max()
    sp = gl.GLScatterPlotItem(pos=pos, size=size, color=cmap(color_data), pxMode=False)
    sp.setGLOptions(gl_preset)
    w.addItem(sp)
    if show_gpat:
        if plot_value != "dc_conductance":
            raise NotImplementedError
        truncation_order = parameters.get("truncation_order", 3)
        include_Ga = parameters.get("include_Ga", True)
        integral_method = parameters.get("integral_method", -15)
        gpat = photon_assisted_tunneling(dm, data.omega, data.vdc, data.vac, xL=xL, include_Ga=include_Ga, truncation_order=truncation_order, integral_method=integral_method)
        pos2 = np.empty_like(pos)
        pos2[:,:2] = pos[:,:2]
        pos2[:,2] = scale*(np.log10(gpat) if zscale=="log" else gpat)
        color_data = gpat.copy()
        color_data -= color_data.min()
        color_data /= color_data.max()
        sp = gl.GLScatterPlotItem(pos=pos2, size=size, color=cmap(color_data), pxMode=False)
        sp.setGLOptions(gl_preset)
        w.addItem(sp)
    app.exec()


def stacked_frequencies(dm, omegas=(7.1271, 16.5372), shift=2, scale=1, size=0.1, xyscale=None, zscale=None, grid=None, cmap="viridis", gl_preset=None, **parameters):
    parameters.pop("omega", None)
    app = QApplication([])
    w = gl.GLViewWidget()
    w.show()
    w.setWindowTitle("Kondo model: stacked frequencies")
    plots = []
    cmap = cm.get_cmap(cmap)
    for n, omega in enumerate(omegas):
        data = dm.list(omega=omega, **parameters)
        pos = np.array([
                data.vdc/omega,
                data.vac/omega,
                np.log(data.dc_conductance*np.pi)*scale + n*shift
            ]).T
        sp = gl.GLScatterPlotItem(pos=pos, size=size, color=cmap(data.dc_conductance*np.pi), pxMode=False)
        w.addItem(sp)
        plots.append(sp)
    app.exec()


def vdc0_scaled(dm,
        scale = 80,
        size = None,
        grid = False,
        xyscale = "linear",
        zscale = "linear",
        gl_preset = "translucent",
        mirror_vdc = None,
        cmap = "viridis",
        **parameters):
    """
    Show overview of all data where one physical parameter is fixed
    """
    for name in ("vdc", "vac", "omega"):
        parameters.pop(name)
    cmap = cm.get_cmap(cmap)
    data = dm.list(vdc=0, **parameters)
    app = QApplication([])
    w = gl.GLViewWidget()
    w.show()
    w.setWindowTitle(f"Kondo model: Vdc=0")
    ax = gl.GLAxisItem(size=QtGui.QVector3D(100,100,100))
    w.addItem(ax)
    # overview
    pos = np.array([4*data.vac/data.omega, data.omega, scale*np.pi*data.dc_conductance]).T
    if xyscale == "log":
        pos[:,0] = np.log10(pos[:,0])
        pos[:,1] = np.log10(pos[:,1])
    if zscale == "log":
        pos[:,2] = scale*np.log10(pos[:,2]/scale)
    if size is None:
        size = scale**0.5/20
    sp = gl.GLScatterPlotItem(
            pos=pos,
            size=size,
            color=cmap(np.pi*data.dc_conductance),
            pxMode=False)
    sp.setGLOptions(gl_preset)
    w.addItem(sp)
    app.exec()


def plot_interpolated(
        *args,
        mirror_vdc = True,
        mirror_vac = False,
        cmap = "viridis",
        order = "o3a",
        method = "mu",
        include_Ga = False,
        integral_method = -15,
        **parameters):
    for trash in ("size",):
        parameters.pop(trash)
    cmap = cm.get_cmap(cmap)
    #data = np.genfromtxt("figdata/vdc_vac_omega16.5372_interp.dat", names=True, delimiter=",")
    #data = np.load("figdata/vdc_vac_omega16.5372_interp.npz")
    data = np.load("figdata/omega5_interp.npz")
    app = QApplication([])
    w = gl.GLViewWidget()
    add3DAxes(w, size=(70,70,60), fontColor=(0,0,0,255), xlabel="Vosc", ylabel="Vavg", zlabel="log(G)", r=4, h=9, shift=(0,-135,-95))
    #pos = np.array([*np.meshgrid(data["vdc"], data["vac"]), 50*data["gdc_g"]]).reshape((3,-1)).T
    #sp = gl.GLScatterPlotItem(pos=pos, size=0.2, color=cm.viridis(data["gdc_g"].flatten()), pxMode=False)
    #sp.setGLOptions("additive")
    #w.addItem(sp)
    #pos_diff = np.array([*np.meshgrid(data["vdc"], data["vac"]), 1000*(data["gdc_i"] - data["gdc_g"])]).T
    #sp = gl.GLScatterPlotItem(pos=pos_diff, size=0.2, color=cm.viridis(pos_diff[:,2]/10), pxMode=False)
    #sp.setGLOptions("additive")
    #w.addItem(sp)
    vdc = data["vdc"][0]
    vac = data["vac"][:,0]
    gdc = np.pi*data[f"gdc_{method}_{order}"]
    if mirror_vdc:
        vdc = np.concatenate((-vdc[:0:-1], vdc))
        gdc = np.concatenate((gdc[:,:0:-1], gdc), axis=1)
    if mirror_vac:
        vac = np.concatenate((-vac[:0:-1], vac))
        gdc = np.concatenate((gdc[:0:-1], gdc), axis=0)
    cmap_light = lambda x: cmap((x**.5 - 0.282)/0.718)
    cmap_dark = lambda x: 0.1*np.array([[1.,1.,1.,1.]]) + 0.9*cmap((x**.5 - 0.282)/0.718)
    sp = gl.GLSurfacePlotItem(
            x=vac,
            y=vdc,
            z=50*np.log(gdc),
            colors=cmap_light(gdc),
            drawEdges=False,
            smooth=True)
    sp.translate(0, 0, -1e-3)
    #sp.translate(-1e-2, -1e-2, -1e-2)
    w.addItem(sp)
    for i in range(0, vac.size, 10):
        line = gl.GLLinePlotItem(
                pos=np.array([vac[i]*np.ones_like(vdc), vdc, 50*np.log(gdc[i])]).T,
                color=cmap_dark(gdc[i]),
                width=10,
                antialias=True,
                mode="line_strip")
        line.setGLOptions("opaque")
        line.setDepthValue(10)
        w.addItem(line)
    for i in range(0, vdc.size, 10):
        line = gl.GLLinePlotItem(
                pos=np.array([vac, vdc[i]*np.ones_like(vac), 50*np.log(gdc[:,i])]).T,
                color=cmap_dark(gdc[:,i]),
                width=10,
                antialias=True,
                mode="line_strip")
        line.setGLOptions("opaque")
        line.setDepthValue(10)
        w.addItem(line)
    w.show()
    w.opts["bgcolor"] = (1.,1.,1.,1.)
    #w.opts["fov"] = 100

    w.setGeometry(0, 0, 2000, 1360)

    for frame, progress in enumerate(np.linspace(0, 2*np.pi, 800, endpoint=False)):
        settings.logger.info(f"progress: {progress*50/np.pi:.1f}%")
        #w.opts["center"] = QtGui.QVector3D(0,24+18*np.cos(progress+0.3),-180-20*np.sin(progress-0.1))
        #w.opts["distance"] = 475 - 15*np.cos(progress+0.2)
        #w.opts["azimuth"] = -22*np.cos(progress+0.3) - 21
        #w.opts["elevation"] = 12*np.sin(progress-0.1) + 30
        w.opts["center"] = QtGui.QVector3D(0,24+12*np.cos(progress+0.3),-162-14*np.sin(progress-0.1))
        w.opts["distance"] = 460 - 10*np.cos(progress+0.2)
        w.opts["azimuth"] = -16*np.cos(progress+0.3) - 21
        w.opts["elevation"] = 8*np.sin(progress-0.1) + 30

        #arr = w.renderToArray((2000,1500)).transpose((1,0,2))
        #makeQImage(arr).save(f"/tmp/frames/img_{frame:04d}.png")
        w.grabFramebuffer().save(f"/tmp/frames/img_{frame:04d}.png")
        # ffmpeg -framerate 50 -i /tmp/frames/img_%04d.png -c:v libsvtav1 -b:v 4M -movflags +faststart output.webm

    app.exec()


def convergence_grid_data(dm,
        omega = 16.5372,
        vac_min = 0,
        vac_max = 165.372,
        vac_num = 11,
        vdc_min = 0,
        vdc_max = 165.372,
        vdc_num = 11,
        v_tol = 1e-6,
        **trashoptions
        ):
    """
    Find data points on grid in Vac and Vdc for fixed Ω.
    Fit to convergence with D. Returne merged data in extended table containing
    both original and fitted values.

    Return values:
        fitted_data:   DataFrame containing only fitted results
        extended_data: extended DataFrame containing fitted results and
                       original data
    Both returned DataFrames use (vdc, vac) as multi-index.
    """
    vac_step = (vac_max - vac_min) / (vac_num - 1)
    vdc_step = (vdc_max - vdc_min) / (vdc_num - 1)
    data = dm.list(omega=omega)
    data = data.loc[
            (data.vac >= vac_min - v_tol)
            & (data.vdc >= vdc_min - v_tol)
            & (data.vac <= vac_max + v_tol)
            & (data.vdc <= vdc_max + v_tol)
            & np.isfinite(data.dc_conductance)]
    data = data.loc[
            (np.abs(((data.vac - vac_min + v_tol) % vac_step) - v_tol)  < v_tol)
            & (np.abs(((data.vdc - vdc_min + v_tol) % vdc_step) - v_tol)  < v_tol)
            ].sort_values(["vac", "vdc", "d"])
    fit_data = data.loc[
            (data.solver_tol_rel==1e-10)
            & (data.solver_tol_abs==1e-12)
            & (data.method=="mu")
            & (data.solver_flags & 0x48c == 0)]
    group = fit_data.groupby(by=["vdc","vac"])
    fit_func = lambda logd_inv, a, b, c: a + b * logd_inv**c
    def fit(df):
        g = df.dc_conductance.mean()
        g_err = df.dc_conductance.std()
        try:
            (idc, idc_pref, idc_exp), covar = curve_fit(
                    fit_func,
                    1/np.log(df.d),
                    df.dc_current,
                    (df.dc_current.mean(), 0., 3.))
            idc_err, idc_pref_err, idc_exp_err = covar.diagonal()**0.5
        except (RuntimeError, TypeError, ValueError):
            idc = idc_pref = idc_exp = idc_err = idc_pref_err = idc_exp_err = np.nan
        try:
            (iac, iac_pref, iac_exp), covar = curve_fit(
                    fit_func,
                    1/np.log(df.d),
                    df.ac_current_abs,
                    (df.ac_current_abs.mean(), 0., 3.))
            iac_err, iac_pref_err, iac_exp_err = covar.diagonal()**0.5
        except (RuntimeError, TypeError, ValueError):
            iac = iac_pref = iac_exp = iac_err = iac_pref_err = iac_exp_err = np.nan
        return pd.Series(dict(g=g, g_err=g_err, idc=idc, idc_pref=idc_pref, idc_exp=idc_exp, idc_err=idc_err, idc_pref_err=idc_pref_err, idc_exp_err=idc_exp_err, iac=iac, iac_pref=iac_pref, iac_exp=iac_exp, iac_err=iac_err, iac_pref_err=iac_pref_err, iac_exp_err=iac_exp_err))
    fitted_data = group.apply(fit)
    extended_data = data.set_index(["vdc", "vac"]).merge(
            fitted_data,
            how = "left",
            on = ["vdc","vac"])
    extended_data.drop(["hash", "version_major", "version_minor", "git_commit_count", "git_commit_id", "timestamp", "energy_re", "energy_im", "dirname", "basename"], axis=1, inplace=True)
    return fitted_data, extended_data


def plot_convergence(dm,
        size = 1.,
        gl_preset = "additive",
        g_scale = 100,
        i_scale = 10,
        cmap = "vidiris",
        **trashoptions
        ):
    """
    3d plot of G, Idc, and Iac vs. Vdc and Vac.
    Plotted are fitted values based on convergence with D and the data points
    leading to this convergence. The deviation from the converged value is
    multiplied by factors 10 (Idc and Iac) or 200 (G).
    """
    if size is None:
        size = 1.
    fitted_data, data = convergence_grid_data(dm)
    app = QApplication([])
    cmap = cm.get_cmap(cmap)
    data_convergence = data.loc[
            (data.method=="mu")
            & (data.solver_tol_rel==1e-10)
            & (data.solver_tol_abs==1e-12)]
    default_bad_flags = DataManager.SOLVER_FLAGS["second_order_rg_equations"] \
            | DataManager.SOLVER_FLAGS["extrapolate_voltage"] \
            | DataManager.SOLVER_FLAGS["simplified_initial_conditions"] \
            | DataManager.SOLVER_FLAGS["include_Ga"] \
            | DataManager.SOLVER_FLAGS["solve_integral_exactly"]
    data_J = data.loc[(data.method == "J") | (data.solver_flags & default_bad_flags == 0)]
    index_frame_J = data_J.index.to_frame()
    index_frame_convergence = data_convergence.index.to_frame()
    index_frame_fit = fitted_data.index.to_frame()

    w_g = gl.GLViewWidget()
    w_g.show()
    w_g.setWindowTitle(f"Kondo model: G convergence, Ω=16.5372")
    ax = gl.GLAxisItem(size=QtGui.QVector3D(100,100,50))
    w_g.addItem(ax)
    pos = np.array([index_frame_fit.vdc, index_frame_fit.vac, 400*np.pi*fitted_data.g]).T
    sp = gl.GLScatterPlotItem(
            pos = pos,
            size = 2*size,
            color = cmap(np.pi*fitted_data.g),
            pxMode = False)
    sp.setGLOptions(gl_preset)
    w_g.addItem(sp)
    pos = np.array([
            index_frame_convergence.vdc,
            index_frame_convergence.vac,
            400*np.pi*data_convergence.g + g_scale*400*np.pi*(data_convergence.dc_conductance - data_convergence.g),
            ]).T
    sp = gl.GLScatterPlotItem(pos=pos, size=size, color=cmap((np.log10(data_convergence.d)-8)/2), pxMode=False)
    sp.setGLOptions(gl_preset)
    w_g.addItem(sp)
    pos = np.array([
            index_frame_J.vdc,
            index_frame_J.vac,
            400*np.pi*data_J.g + g_scale*400*np.pi*(data_J.dc_conductance - data_J.g),
            ]).T
    sp = gl.GLScatterPlotItem(pos=pos, size=size, color=(1.,0.,0.,1.), pxMode=False)
    sp.setGLOptions(gl_preset)
    w_g.addItem(sp)

    w_idc = gl.GLViewWidget()
    w_idc.show()
    w_idc.setWindowTitle(f"Kondo model: Idc convergence, Ω=16.5372")
    ax = gl.GLAxisItem(size=QtGui.QVector3D(100,100,50))
    w_idc.addItem(ax)
    pos = np.array([index_frame_fit.vdc, index_frame_fit.vac, 20*fitted_data.idc]).T
    sp = gl.GLScatterPlotItem(
            pos = pos,
            size = 2*size,
            color = cmap(fitted_data.idc/fitted_data.idc.max()),
            pxMode = False)
    sp.setGLOptions(gl_preset)
    w_idc.addItem(sp)
    pos = np.array([
            index_frame_convergence.vdc,
            index_frame_convergence.vac,
            20*data_convergence.idc + i_scale*20*(data_convergence.dc_current - data_convergence.idc),
            ]).T
    sp = gl.GLScatterPlotItem(
            pos = pos,
            size = size,
            color = cmap((np.log10(data_convergence.d) - 8)/2),
            pxMode = False)
    sp.setGLOptions(gl_preset)
    w_idc.addItem(sp)
    pos = np.array([
            index_frame_J.vdc,
            index_frame_J.vac,
            20*data_J.idc + i_scale*20*(data_J.dc_current - data_J.idc),
            ]).T
    sp = gl.GLScatterPlotItem(pos=pos, size=size, color=(1.,0.,0.,1.), pxMode=False)
    sp.setGLOptions(gl_preset)
    w_idc.addItem(sp)

    w_iac = gl.GLViewWidget()
    w_iac.show()
    w_iac.setWindowTitle(f"Kondo model: Idc convergence, Ω=16.5372")
    ax = gl.GLAxisItem(size=QtGui.QVector3D(100,100,50))
    w_iac.addItem(ax)
    pos = np.array([index_frame_fit.vdc, index_frame_fit.vac, 40*fitted_data.iac]).T
    sp = gl.GLScatterPlotItem(
            pos = pos,
            size = 2*size,
            color = cmap(fitted_data.iac/fitted_data.iac.max()),
            pxMode = False)
    sp.setGLOptions(gl_preset)
    w_iac.addItem(sp)
    pos = np.array([
            index_frame_convergence.vdc,
            index_frame_convergence.vac,
            40*data_convergence.iac + i_scale*40*(data_convergence.ac_current_abs - data_convergence.iac),
            ]).T
    sp = gl.GLScatterPlotItem(
            pos = pos,
            size = size,
            color = cmap((np.log10(data_convergence.d) - 8)/2),
            pxMode = False)
    sp.setGLOptions(gl_preset)
    w_iac.addItem(sp)
    pos = np.array([
            index_frame_J.vdc,
            index_frame_J.vac,
            40*data_J.iac + i_scale*40*(data_J.ac_current_abs - data_J.iac),
            ]).T
    sp = gl.GLScatterPlotItem(pos=pos, size=size, color=(1.,0.,0.,1.), pxMode=False)
    sp.setGLOptions(gl_preset)
    w_iac.addItem(sp)

    app.exec()


def compare_orders(dm,
        omega = 16.5372,
        d = 1e9,
        solver_tol_rel = 1e-8,
        solver_tol_abs = 1e-10,
        method = "mu",
        padding = 0,
        xL = 0.5,
        cmap = "viridis",
        **trashoptions
        ):
    cmap = cm.get_cmap(cmap)
    data = dm.list(omega=omega, d=d, solver_tol_rel=solver_tol_rel, solver_tol_abs=solver_tol_abs, method=method, padding=padding, xL=xL, include_Ga=False, solve_integral_exactly=False)
    bad_flags = DataManager.SOLVER_FLAGS["simplified_initial_conditions"] \
            | DataManager.SOLVER_FLAGS["extrapolate_voltage"] \
            | DataManager.SOLVER_FLAGS["deleted"]
    data = data.loc[data.solver_flags & bad_flags == 0]
    order2 = data.solver_flags & DataManager.SOLVER_FLAGS["second_order_rg_equations"] != 0
    order3 = data.solver_flags & DataManager.SOLVER_FLAGS["second_order_rg_equations"] == 0
    data.vac = data.vac.round(6)
    data.vdc = data.vdc.round(6)
    merged = pd.merge(
            data[order2],
            data[order3],
            how="inner",
            on=["vdc","vac","d","omega","method","solver_tol_rel","solver_tol_abs","resonant_dc_shift","padding","energy_im","energy_re","xL","lazy_inverse_factor"],
            suffixes=("_2", "_3"))

    app = QApplication([])
    w_g = gl.GLViewWidget()
    w_g.show()
    rel_diff = (merged.dc_conductance_2 - merged.dc_conductance_3) / merged.dc_conductance_3
    #colors = cm.viridis((np.pi*merged.dc_conductance_3)**0.5)
    #pos = np.array([merged.vdc, merged.vac, 150*np.pi*merged.dc_conductance_3]).T
    #sp = gl.GLScatterPlotItem(pos=pos, size=1.5, color=(0,0.4,0,1), pxMode=False)
    #w_g.addItem(sp)
    colors = cm.seismic(5.5*rel_diff + 0.5)
    pos = np.array([merged.vdc, merged.vac, 150*np.pi*merged.dc_conductance_2]).T
    #pos = np.array([merged.vdc, merged.vac, 150*np.pi*merged.dc_conductance_3]).T
    sp = gl.GLScatterPlotItem(pos=pos, size=1, color=colors, pxMode=False)
    w_g.addItem(sp)
    app.exec()


def compare_extra_precise(dm,
        omega = 16.5372,
        d = 1e9,
        solver_tol_rel = 1e-8,
        solver_tol_abs = 1e-10,
        method = "mu",
        xL = 0.5,
        cmap = "viridis",
        size = 1.,
        comparison_order = 3,
        **trashoptions
        ):
    if size is None:
        size = 1.
    cmap = cm.get_cmap(cmap)
    data = dm.list(omega=omega, d=d, solver_tol_rel=solver_tol_rel, solver_tol_abs=solver_tol_abs, padding=0, xL=xL, method=method)
    bad_flags = DataManager.SOLVER_FLAGS["simplified_initial_conditions"] \
            | DataManager.SOLVER_FLAGS["extrapolate_voltage"] \
            | DataManager.SOLVER_FLAGS["deleted"]
    if comparison_order == 3:
        bad_flags |= DataManager.SOLVER_FLAGS["second_order_rg_equations"]
    precision_flags = DataManager.SOLVER_FLAGS["include_Ga"] | DataManager.SOLVER_FLAGS["solve_integral_exactly"]
    data = data.loc[data.solver_flags & bad_flags == 0]
    standard = data.solver_flags & precision_flags == 0
    if comparison_order == 2:
        standard &= data.solver_flags & DataManager.SOLVER_FLAGS["second_order_rg_equations"] != 0
    precise = (data.solver_flags & precision_flags == precision_flags) & (data.integral_method == -1)
    if comparison_order == 2:
        precise &= data.solver_flags & DataManager.SOLVER_FLAGS["second_order_rg_equations"] == 0
    data.vac = data.vac.round(9)
    data.vdc = data.vdc.round(9)
    merged = pd.merge(
            data[standard],
            data[precise],
            how="inner",
            on=["vdc","vac","d","omega","method","solver_tol_rel","solver_tol_abs","resonant_dc_shift","padding","energy_im","energy_re","xL","lazy_inverse_factor"],
            suffixes=("_s", "_p"))

    app = QApplication([])
    w_g = gl.GLViewWidget()
    w_g.show()
    rel_diff = (merged.dc_conductance_s - merged.dc_conductance_p) / merged.dc_conductance_p
    #colors = cm.viridis((np.pi*merged.dc_conductance_p)**0.5)
    #pos = np.array([merged.vdc, merged.vac, 150*np.pi*merged.dc_conductance_p]).T
    #sp = gl.GLScatterPlotItem(pos=pos, size=1.5, color=(0,0.4,0,1), pxMode=False)
    #w_g.addItem(sp)
    colors = cm.seismic(8*rel_diff + 0.5)
    #pos = np.array([merged.vdc, merged.vac, 150*np.pi*merged.dc_conductance_s]).T
    pos = np.array([merged.vdc, merged.vac, 150*np.pi*merged.dc_conductance_p]).T
    sp = gl.GLScatterPlotItem(pos=pos, size=size, color=colors, pxMode=False)
    w_g.addItem(sp)
    app.exec()


def compare_RGeq(dm,
        omega = 16.5372,
        d = 1e9,
        solver_tol_rel = 1e-8,
        solver_tol_abs = 1e-10,
        method = "mu",
        xL = 0.5,
        size = 2.,
        scale = 150*np.pi,
        **trashoptions
        ):
    if size is None:
        size = 1.
    data = dm.list(omega=omega, d=d, solver_tol_rel=solver_tol_rel, solver_tol_abs=solver_tol_abs, padding=0, xL=xL, method=method)
    bad_flags = DataManager.SOLVER_FLAGS["simplified_initial_conditions"] \
            | DataManager.SOLVER_FLAGS["extrapolate_voltage"] \
            | DataManager.SOLVER_FLAGS["deleted"]
    precision_flags = DataManager.SOLVER_FLAGS["include_Ga"] | DataManager.SOLVER_FLAGS["solve_integral_exactly"]
    data = data.loc[data.solver_flags & bad_flags == 0]
    order2 = (data.solver_flags & precision_flags == 0) \
            & (data.solver_flags & DataManager.SOLVER_FLAGS["second_order_rg_equations"] != 0)
    order3 = (data.solver_flags & precision_flags == 0) \
            & (data.solver_flags & DataManager.SOLVER_FLAGS["second_order_rg_equations"] == 0)
    order3plus = (data.solver_flags & precision_flags == precision_flags) \
            & (data.solver_flags & DataManager.SOLVER_FLAGS["second_order_rg_equations"] == 0) \
            & (data.integral_method == -1)

    pos = np.array([data.vdc, data.vac, scale*data.dc_conductance]).T
    app = QApplication([])
    w_g = gl.GLViewWidget()
    w_g.show()
    sp = gl.GLScatterPlotItem(pos=pos[order2], size=size, color=(1,0,0,1), pxMode=False)
    w_g.addItem(sp)
    sp = gl.GLScatterPlotItem(pos=pos[order3], size=size, color=(0,0,1,1), pxMode=False)
    w_g.addItem(sp)
    sp = gl.GLScatterPlotItem(pos=pos[order3plus], size=size, color=(0,1,0,1), pxMode=False)
    w_g.addItem(sp)
    app.exec()


PRESETS = dict(
        default = dict(d=1e9, truncation_order=3, include_Ga=False, solve_integral_exactly=False),
        compare_precision = dict(function=compare_extra_precise, omega=16.5372),
        compare_precision_order2 = dict(function=compare_extra_precise, omega=16.5372, comparison_order=2),
        vdc0 = dict(vdc=0, d=1e9, scale=30, truncation_order=3),
        vdc0_scaled = dict(function=vdc0_scaled, d=1e9, scale=30, truncation_order=3),
        # comparison to experiment by Kogan et al. 2004
        kogan04 = dict(omega=7.1271, d=1e9, truncation_order=3),
        # comparison to experiment by Bruhat et al. 2018
        bruhat18a = dict(omega=5.8206184, d=1e9, truncation_order=3),
        bruhat18b = dict(omega=9.2159791, d=1e9, truncation_order=3),
        # Ω = 5 Tk (Tk defined by G(V=Tk)=e²/h)
        omega5 = dict(omega=16.5372, d=1e9, truncation_order=3),
        omega5log = dict(omega=16.5372, xyscale="log", scale=1, d=1e9, truncation_order=3),
        interp = dict(function=plot_interpolated),
        convergence = dict(function=plot_convergence, truncation_order=3),
        orders = dict(function=compare_orders, omega=16.5372),
        gamma_overview = dict(function=full_overview, d=1e9, truncation_order=3, plot_value="gamma"),
        gamma = dict(d=1e9, truncation_order=3, plot_value="gamma", scale=10, size=1),
        idc = dict(d=1e9, truncation_order=3, plot_value="dc_current", scale=10, size=1),
        iac = dict(d=1e9, truncation_order=3, plot_value="ac_current_abs", scale=20, size=1),
        phase = dict(d=1e9, truncation_order=3, plot_value="ac_current_phase", scale=100, size=1),
        compare_RGeq = dict(function=compare_RGeq),
        stacked = dict(function=stacked_frequencies),
        )

def main(dm, preset=None, **parameters):
    if preset:
        try:
            parameters.update(PRESETS[preset])
        except KeyError:
            settings.logger.warning("Unknown preset: " + preset)
    if "function" in parameters:
        function = parameters.pop("function")
        if function != fixed_parameter:
            parameters.pop("show_gpat", None)
        function(dm, **parameters)
        return
    for name in ("vdc", "vac", "omega"):
        if parameters.get(name, None) is not None:
            fixed_parameter(dm, **parameters)
            return
    full_overview(dm, **parameters)

def parse():
    """
    Parse command line arguments and call main()
    """
    from logging import _levelToName
    parser = argparse.ArgumentParser(description="Generate 3d overview plots using pyqtgraph")
    parser.add_argument("preset", type=str, choices=PRESETS.keys(), nargs="?",
            help="name for preset parameters")
    parser.add_argument("--db_filename", metavar="file", type=str,
            help = "SQLite database file for saving metadata")
    parser.add_argument("--log_level", metavar="str", type=str,
            default = _levelToName.get(settings.logger.level, "INFO"),
            choices = ("INFO", "DEBUG", "WARNING", "ERROR"),
            help = "logging level")
    parser.add_argument("--omega", type=float,
            help="Frequency, units of Tk")
    parser.add_argument("--method", type=str, choices=("J", "mu"),
            help="method: J or mu")
    parser.add_argument("--nmax", metavar="int", type=int,
            help="Floquet matrix size")
    parser.add_argument("--padding", metavar="int", type=int,
            help="Floquet matrix ppadding")
    parser.add_argument("--voltage_branches", metavar="int", type=int,
            help="Voltage branches")
    parser.add_argument("--resonant_dc_shift", metavar="int", type=int,
            help="resonant DC shift")
    parser.add_argument("--vdc", metavar="float", type=float,
            help="Vdc, units of Tkrg")
    fourier_coef_group = parser.add_mutually_exclusive_group()
    fourier_coef_group.add_argument("--vac", metavar="float", type=float,
            help="Vac, units of Tkrg")
    fourier_coef_group.add_argument("--fourier_coef", metavar="tuple", type=float, nargs="*",
            help="Voltage Fourier arguments, units of omega")
    parser.add_argument("--d", metavar="float", type=float,
            help="D (UV cutoff), units of Tkrg")
    parser.add_argument("--xL", metavar="float", type=float, default=0.5,
            help="Asymmetry, 0 < xL < 1")
    parser.add_argument("--truncation_order", metavar='int', type=int, choices=(2,3),
            help = "Truncation order of RG equations.")
    parser.add_argument("--compact", metavar="int", type=int,
            help="compact FRTRG implementation (0,1, or 2)")
    parser.add_argument("--solver_tol_rel", metavar="float", type=float,
            help="Solver relative tolerance")
    parser.add_argument("--solver_tol_abs", metavar="float", type=float,
            help="Solver absolute tolerance")
    parser.add_argument("--xyscale", type=str, choices=("log", "lin"), default="lin",
            help="Scale for x and y axes")
    parser.add_argument("--zscale", type=str, choices=("log", "lin"), default="lin",
            help="Scale for z axis")
    parser.add_argument("--scale", metavar="float", type=float, default=80*np.pi,
            help="Scaling factor data axis")
    parser.add_argument("--cmap", metavar="colormap", type=str, default="viridis",
            help="name of matplotlib colormap")
    parser.add_argument("--grid", metavar="bool", type=int, default=0,
            help="Show grid")
    parser.add_argument("--size", metavar="float", type=float, default=None,
            help="size of points in plot")
    parser.add_argument("--gl_preset", type=str, default="additive",
            choices=("translucent", "additive", "opaque"),
            help="OpenGL setup for drawing.")
    parser.add_argument("--integral_method", metavar="int", type=int,
            help="method for solving frequency integral (-1 for exact solution)")
    parser.add_argument("--include_Ga", metavar="bool", type=int,
            help="include Ga in RG equations")
    parser.add_argument("--show_gpat", metavar="int", type=int,
            help="Show G from photon assisted tunneling (0 or 1)")
    parser.add_argument("--min_version_major", metavar="int", type=int, default=14,
            help="Minimal major version")
    parser.add_argument("--min_version_minor", metavar="int", type=int, default=-1,
            help="Minimal minor version")
    parser.add_argument("--good_flags", metavar="int", type=int, default=0,
            help="Required solver flags")
    parser.add_argument("--bad_flags", metavar="int", type=int, default=0,
            help="Forbidden solver flags")
    args = parser.parse_args()

    options = args.__dict__
    options["min_version"] = (options.pop("min_version_major"), options.pop("min_version_minor"), -1, -1)
    db_filename = options.pop("db_filename", None)
    if db_filename is not None:
        settings.defaults.DB_CONNECTION_STRING = "sqlite:///" + os.path.abspath(db_filename)
    settings.defaults.logger.setLevel(options.pop("log_level"))
    settings.defaults.update_globals()

    dm = DataManager()
    main(dm, **options)


if __name__ == "__main__":
    parse()
