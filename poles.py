#!/usr/bin/env python3

# Copyright 2022 Valentin Bruch <valentin.bruch@rwth-aachen.de>
# License: MIT
"""
Kondo FRTRG, analyze poles and branch cuts
"""
from kondo import Kondo
import numpy as np
import matplotlib.pyplot as plt
from settings import logger
from matplotlib.colors import LogNorm

def plot_bc(
        epsilon=1e-4,
        ir_cutoff=0.1,
        depth=20,
        solver_parameters={},
        nmax=10,
        voltage_branches=4,
        omega=16.5372,
        vdc=1,
        resolution=100,
        **parameters):
    """
    Plot branch cuts for directly calculated data.
    """
    t = np.linspace(ir_cutoff, -depth, resolution)
    logger.info("Starting initial RG flow")
    kondo = Kondo(
            nmax=nmax,
            voltage_branches=voltage_branches,
            omega=omega,
            vdc=vdc,
            **parameters,
            compact=0)
    solver = kondo.run(**solver_parameters, ir_cutoff=ir_cutoff)

    # left of center
    logger.info("Moving to left branch")
    solver_left = kondo.solveOdeRe(
            0., -epsilon,
            **solver_parameters,
            init_values=solver.y[:,-1])
    assert np.allclose(kondo.energy, 1j*ir_cutoff - epsilon)
    logger.info("Solving along left branch")
    solver_left_down = kondo.solveOdeIm(
            ir_cutoff, -depth,
            **solver_parameters,
            init_values=solver_left.y[:,-1],
            t_eval=t)

    # right of center
    kondo.global_properties.energy = 1j*ir_cutoff
    logger.info("Moving to right branch")
    solver_right = kondo.solveOdeRe(
            0., epsilon,
            **solver_parameters,
            init_values=solver.y[:,-1])
    assert np.allclose(kondo.energy, 1j*ir_cutoff + epsilon)
    logger.info("Solving along right branch")
    solver_right_down = kondo.solveOdeIm(
            ir_cutoff, -depth,
            **solver_parameters,
            init_values=solver_right.y[:,-1],
            t_eval=t)

    gamma_size = kondo.gamma.values.size
    y_diff_abs = np.abs(solver_right_down.y - solver_left_down.y)
    gamma_diff_abs = y_diff_abs[:gamma_size].reshape((*kondo.gamma.values.shape, resolution))

    logger.info("Preparing plot")
    fig, ax = plt.subplots()
    x_data = []
    y_data = []
    z_data = []
    for vb in range(-voltage_branches, voltage_branches+1):
        for n in range(-nmax, nmax+1):
            x_data.append((vb*vdc + omega*n)*np.ones_like(t))
            y_data.append(t)
            z_data.append(gamma_diff_abs[vb+voltage_branches, n+nmax, n+nmax])
    x_data = np.concatenate(x_data, axis=None)
    y_data = np.concatenate(y_data, axis=None)
    z_data = np.concatenate(z_data, axis=None)
    plot = ax.scatter(
            x_data,
            y_data,
            c=z_data,
            norm=LogNorm(epsilon, 3),
            cmap=plt.cm.viridis)
    fig.colorbar(plot, ax=ax)
    plt.show()


if __name__ == "__main__":
    plot_bc(
            epsilon=1e-6,
            solver_parameters=dict(rtol=1e-10, atol=1e-12),
            d=1e9,
            omega=16.5372,
            vdc=5,
            vac=7,
            nmax=12,
            voltage_branches=5,
            method="mu")
